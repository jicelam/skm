﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace SKM.Data.Test.Services.Data {
	public class EntityDataService_DeleteTests {
		/* ----------------------------------------- Favorites ----------------------------------------- */



		/* ----------------------------------------- Forums ----------------------------------------- */
		[Fact]
		public async Task DeleteForum_ValidForumId_Success() {
			using (var fixture = new DataFixture()) {
				var targetForum = fixture.SeedService.Forums[3];
				var targetUser = fixture.SeedService.Users[1];
				var startingForumCount = fixture.SeedService.Forums.Count;

				var forumBefore = await fixture.DataService.GetForum(targetForum.Id);
				var threadResultBefore = await fixture.DataService.GetThreads(targetForum.Id);
				var postResultBefore = await fixture.DataService.GetPosts(threadResultBefore.Items.First().Id);
				var noticeResultBefore = await fixture.DataService.GetNotices(targetForum.Id);
				var favoriteResultBefore = await fixture.DataService.GetFavorites(targetUser.Id);

				Assert.NotNull(forumBefore);
				Assert.NotEmpty(threadResultBefore.Items);
				Assert.NotEmpty(postResultBefore.Items);
				Assert.NotEmpty(noticeResultBefore.Items);
				Assert.NotEmpty(favoriteResultBefore.Items);

				await fixture.DataService.DeleteForum(targetForum.Id);

				var forumAfter = await fixture.DataService.GetForum(targetForum.Id);
				var threadAfter = await fixture.DataService.GetThread(threadResultBefore.Items.First().Id);
				var postAfter = await fixture.DataService.GetPost(postResultBefore.Items.First().Id);
				var noticeAfter = await fixture.DataService.GetNotice(noticeResultBefore.Items.First().Id);
				var favoriteAfter = await fixture.DataService.GetFavorite(favoriteResultBefore.Items.First().Id);

				Assert.Null(forumAfter);
				Assert.Null(threadAfter);
				Assert.Null(postAfter);
				Assert.Null(noticeAfter);
				Assert.Null(favoriteAfter);
				Assert.Equal(startingForumCount - 1, fixture.SeedService.Forums.Count);
			}
		}

		[Fact]
		public async Task DeleteForum_InvalidForumId_Failure() {
			using (var fixture = new DataFixture()) {
				var startingForumCount = fixture.SeedService.Forums.Count;
				var startingThreadCount = fixture.SeedService.Threads.Count;
				var startingPostCount = fixture.SeedService.Posts.Count;
				var startingNoticeCount = fixture.SeedService.Notices.Count;
				var startingFavoriteCount = fixture.SeedService.FavoritedItems.Count;

				await Assert.ThrowsAsync<ArgumentException>(async () => {
					await fixture.DataService.DeleteForum(new Guid());
				});

				Assert.Equal(startingForumCount, fixture.SeedService.Forums.Count);
				Assert.Equal(startingThreadCount, fixture.SeedService.Threads.Count);
				Assert.Equal(startingPostCount, fixture.SeedService.Posts.Count);
				Assert.Equal(startingNoticeCount, fixture.SeedService.Notices.Count);
				Assert.Equal(startingFavoriteCount, fixture.SeedService.FavoritedItems.Count);
			}
		}

		/* ----------------------------------------- Notices ----------------------------------------- */

		[Fact]
		public async Task DeleteNotice_ValidNoticeId_Success() {
			using (var fixture = new DataFixture()) {
				var targetNotice = fixture.SeedService.Notices[4];
				var startingNoticeCount = fixture.SeedService.Notices.Count;

				var noticeBefore = await fixture.DataService.GetNotice(targetNotice.Id);

				Assert.NotNull(noticeBefore);

				await fixture.DataService.DeleteNotice(targetNotice.Id);

				var noticeAfter = await fixture.DataService.GetNotice(targetNotice.Id);

				Assert.Null(noticeAfter);
				Assert.Equal(startingNoticeCount - 1, fixture.SeedService.Notices.Count);
			}
		}

		[Fact]
		public async Task DeleteNotice_InvalidNoticeId_Failure() {
			using (var fixture = new DataFixture()) {
				var startingNoticeCount = fixture.SeedService.Notices.Count;

				await Assert.ThrowsAsync<ArgumentException>(async () => {
					await fixture.DataService.DeleteNotice(new Guid());
				});

				Assert.Equal(startingNoticeCount, fixture.SeedService.Notices.Count);
			}
		}

		/* ----------------------------------------- Posts ----------------------------------------- */

		[Fact]
		public async Task DeletePost_ValidPostId_Success() {
			using (var fixture = new DataFixture()) {
				var targetPost = fixture.SeedService.Posts[11];
				var startingPostCount = fixture.SeedService.Posts.Count;

				var postBefore = await fixture.DataService.GetPost(targetPost.Id);

				Assert.NotNull(postBefore);

				await fixture.DataService.DeletePost(targetPost.Id);

				var postAfter = await fixture.DataService.GetPost(targetPost.Id);

				Assert.Null(postAfter);
				Assert.Equal(startingPostCount - 1, fixture.SeedService.Posts.Count);
			}
		}

		[Fact]
		public async Task DeletePost_InvalidPostId_Failure() {
			using (var fixture = new DataFixture()) {
				var startingPostCount = fixture.SeedService.Posts.Count;

				await Assert.ThrowsAsync<ArgumentException>(async () => {
					await fixture.DataService.DeletePost(new Guid());
				});

				Assert.Equal(startingPostCount, fixture.SeedService.Posts.Count);
			}
		}

		/* ----------------------------------------- Reprimands ----------------------------------------- */



		/* ----------------------------------------- Threads ----------------------------------------- */

		[Fact]
		public async Task DeleteThread_ValidThreadId_Success() {
			using (var fixture = new DataFixture()) {
				var targetThread = fixture.SeedService.Threads[9];
				var startingThreadCount = fixture.SeedService.Threads.Count;

				var threadBefore = await fixture.DataService.GetThread(targetThread.Id);
				var postResultBefore = await fixture.DataService.GetPosts(targetThread.Id);

				Assert.NotNull(threadBefore);
				Assert.NotEmpty(postResultBefore.Items);

				await fixture.DataService.DeleteThread(targetThread.Id);

				var threadAfter = await fixture.DataService.GetThread(targetThread.Id);
				var postAfter = await fixture.DataService.GetPost(postResultBefore.Items.First().Id);

				Assert.Null(threadAfter);
				Assert.Null(postAfter);
				Assert.Equal(startingThreadCount - 1, fixture.SeedService.Threads.Count);
			}
		}

		[Fact]
		public async Task DeleteThread_InvalidThreadId_Failure() {
			using (var fixture = new DataFixture()) {
				var startingThreadCount = fixture.SeedService.Threads.Count;
				var startingPostCount = fixture.SeedService.Posts.Count;

				await Assert.ThrowsAsync<ArgumentException>(async () => {
					await fixture.DataService.DeleteThread(new Guid());
				});

				Assert.Equal(startingThreadCount, fixture.SeedService.Threads.Count);
				Assert.Equal(startingPostCount, fixture.SeedService.Posts.Count);
			}
		}

		/* ----------------------------------------- Users ----------------------------------------- */
	}
}
