﻿using SKM.Models.Data.Create;
using System;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace SKM.Data.Test.Services.Data {
	public class EntityDataService_CreateTests {
		/* ----------------------------------------- Favorites ----------------------------------------- */



		/* ----------------------------------------- Forums ----------------------------------------- */

		[Theory]
		[InlineData("Test Forum", "This is a test forum", "//test.storage.skm")]
		[InlineData("Test Forum", null, "//test.storage.skm")]
		[InlineData("Test Forum", "This is a test forum", null)]
		[InlineData("Test Forum", null, null)]
		public async Task CreateForum_ValidInput_Success(string name, string description, string iconUrl) {
			using (var fixture = new DataFixture()) {
				var currentUser = fixture.SeedService.CurrentUser;
				var startingForumCount = fixture.SeedService.Forums.Count;

				var newForum = await fixture.DataService.CreateForum(new ForumCreateData(
					name: name,
					createdById: currentUser.Id,
					description: description,
					iconUrl: iconUrl
				));

				Assert.NotNull(newForum);
				Assert.False(newForum.Id == Guid.Empty, "Forum id should not be empty");

				var forum = await fixture.DataService.GetForum(newForum.Id);

				Assert.NotNull(forum);
				Assert.Equal(newForum.Id, forum.Id);
				Assert.Equal(newForum.CreatedOn, forum.CreatedOn);
				Assert.Equal(newForum.Description, forum.Description);
				Assert.Equal(newForum.Name, forum.Name);

				var result = await fixture.DataService.GetForums();

				Assert.Equal(startingForumCount + 1, result.Items.Count());
			}
		}

		/* ----------------------------------------- Notices ----------------------------------------- */

		[Theory]
		[InlineData("Test content", 2, "//test.storage.skm", "Test Notice")]
		[InlineData("Test content", null, "//test.storage.skm", "Test Notice")]
		[InlineData("Test content", 2, null, "Test Notice")]
		public async Task CreateNotice_ValidInputs_Success(
			string content,
			int? forumIndex,
			string iconUrl,
			string name
		) {
			using (var fixture = new DataFixture()) {
				var currentUser = fixture.SeedService.CurrentUser;
				var forumId = forumIndex == null ? null as Guid? : fixture.SeedService.Forums[(int) forumIndex].Id;
				var startingNoticeCount = fixture.SeedService.Notices.Where(n => n.ForumId == forumId).Count();

				var newNotice = await fixture.DataService.CreateNotice(new NoticeCreateData(
					content: content,
					createdById: currentUser.Id,
					forumId: forumId,
					iconUrl: iconUrl,
					name: name
				));

				Assert.NotNull(newNotice);
				Assert.False(newNotice.Id == Guid.Empty, "Id should not be empty");
				Assert.False(string.IsNullOrWhiteSpace(newNotice.Content), "Content should not be empty");
				Assert.False(string.IsNullOrWhiteSpace(newNotice.Name), "Name should not be empty");

				var result = await fixture.DataService.GetNotices(forumId);
				var notice = result.Items.Where(n => n.Id == newNotice.Id)
					.FirstOrDefault();

				Assert.NotNull(notice);
				Assert.Equal(newNotice.Id, notice.Id);
				Assert.Equal(newNotice.Content, notice.Content);
				Assert.Equal(newNotice.CreatedBy.Id, notice.CreatedBy.Id);
				Assert.Equal(newNotice.CreatedOn, notice.CreatedOn);
				Assert.Equal(newNotice.ForumId, notice.ForumId);
				Assert.Equal(newNotice.Name, notice.Name);
				Assert.Equal(startingNoticeCount + 1, result.Items.Count());
			}
		}

		/* ----------------------------------------- Posts ----------------------------------------- */

		[Theory]
		[InlineData(4, "Test content")]
		public async Task CreatePost_ValidInputs_Success(int? threadIndex, string content) {
			using (var fixture = new DataFixture()) {
				var currentUser = fixture.SeedService.CurrentUser;
				var threadId = threadIndex == null ? null as Guid? : fixture.SeedService.Threads[(int) threadIndex].Id;
				var startingPostCount = fixture.SeedService.Posts.Where(p => p.ThreadId == threadId).Count();

				var newPost = await fixture.DataService.CreatePost(new PostCreateData(
					threadId: threadId ?? new Guid(),
					content: content,
					createdById: currentUser.Id
				));

				Assert.NotNull(newPost);
				Assert.False(string.IsNullOrWhiteSpace(newPost.Content), "Content should not be empty");
				Assert.False(newPost.ThreadId == Guid.Empty, "Thread id should not be empty");

				var post = await fixture.DataService.GetPost(newPost.Id);

				Assert.NotNull(post);

				var result = await fixture.DataService.GetPosts(threadId ?? new Guid());

				Assert.Equal(newPost.Id, post.Id);
				Assert.Equal(newPost.Content, post.Content);
				Assert.Equal(newPost.CreatedBy.Id, post.CreatedBy.Id);
				Assert.Equal(newPost.CreatedOn, post.CreatedOn);
				Assert.Equal(newPost.ThreadId, post.ThreadId);
				Assert.Equal(startingPostCount + 1, result.Items.Count());
			}
		}


		/* ----------------------------------------- Reprimands ----------------------------------------- */



		/* ----------------------------------------- Threads ----------------------------------------- */

		[Theory]
		[InlineData("Test content", 1, "//test.storage.skm", "Test Thread")]
		[InlineData("Test content", 1, null, "Test Thread")]
		public async Task CreateThread_ValidInputs_Success(string content, int? forumIndex, string iconUrl, string name) {
			using (var fixture = new DataFixture()) {
				var currentUser = fixture.SeedService.CurrentUser;
				var forumId = forumIndex == null ? null as Guid? : fixture.SeedService.Forums[(int) forumIndex].Id;
				var startingThreadCount = fixture.SeedService.Threads.Where(p => p.ForumId == forumId).Count();

				var newThread = await fixture.DataService.CreateThread(new ThreadCreateData(
					forumId: forumId ?? new Guid(),
					content: content,
					createdById: currentUser.Id,
					iconUrl: iconUrl,
					name: name
				));

				Assert.NotNull(newThread);
				Assert.False(newThread.ForumId == Guid.Empty, "Thread id should not be empty");

				var thread = await fixture.DataService.GetThread(newThread.Id);

				Assert.NotNull(thread);

				var postResult = await fixture.DataService.GetPosts(newThread.Id);

				var post = postResult.Items.First();
				var postCount = postResult.Items.Count();
				Assert.Equal(content, post.Content);
				Assert.False(string.IsNullOrWhiteSpace(post.Content), "Content should not be empty");
				Assert.Equal(1, postCount);

				var threadResult = await fixture.DataService.GetThreads((Guid) forumId);

				Assert.Equal(newThread.Id, thread.Id);
				Assert.Equal(newThread.CreatedBy.Id, thread.CreatedBy.Id);
				Assert.Equal(newThread.CreatedOn, thread.CreatedOn);
				Assert.Equal(newThread.LastPostBy.Id, thread.LastPostBy.Id);
				Assert.Equal(newThread.LastPostOn, thread.LastPostOn);
				Assert.Equal(newThread.PostCount, thread.PostCount);
				Assert.Equal(startingThreadCount + 1, threadResult.Items.Count());
			}
		}

		/* ----------------------------------------- Users ----------------------------------------- */
	}
}
