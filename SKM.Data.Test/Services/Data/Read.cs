﻿using SKM.Models.Data;
using SKM.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace SKM.Data.Test.Services.Data {
	public class EntityDataService_ReadTests : IClassFixture<DataFixture> {
		private readonly DataFixture _fixture;

		public EntityDataService_ReadTests(DataFixture fixture) {
			_fixture = fixture;
		}

		/* ----------------------------------------- Favorites ----------------------------------------- */

		[Fact]
		public async Task ReadFavorites_NoQuery_Success() {
			var targetUser = _fixture.SeedService.Users[1];

			await TestCollectionRetrieval(
				async () => (await _fixture.DataService.GetFavorites(targetUser.Id)).Items.OrderBy(f => f.Id),
				() => _fixture.SeedService.FavoritedItems.Where(f => f.UserId == targetUser.Id).OrderBy(f => f.Id),
				(f1, f2) => f1.Id != f2.Id
			);
		}

		[Theory]
		[InlineData(0, 1)]
		[InlineData(2, 2)]
		public async Task ReadFavorites_Query_Success(int skipAmount, int takeAmount) {
			var targetUser = _fixture.SeedService.Users[1];

			await TestCollectionQueryRetrieval(
				skipAmount,
				takeAmount,
				async () => await _fixture.DataService.GetFavorites(targetUser.Id),
				async () => await _fixture.DataService.GetFavorites(targetUser.Id, new QueryOptions<FavoritedItemData> {
					Skip = skipAmount,
					Take = takeAmount
				})
			);
		}

		/* ----------------------------------------- Forums ----------------------------------------- */

		[Fact]
		public async Task ReadForum_Success() {
			var targetForum = _fixture.SeedService.Forums[1];

			var forum = await _fixture.DataService.GetForum(targetForum.Id);

			Assert.Equal(targetForum.Id, forum.Id);
			Assert.Equal(targetForum.Name, forum.Name);
			Assert.Equal(targetForum.CreatedOn, forum.CreatedOn);
		}

		[Fact]
		public async Task ReadForum_NonExistantItem_ReturnNull() {
			var forum = await _fixture.DataService.GetForum(new Guid());

			Assert.Null(forum);
		}

		[Fact]
		public async Task ReadForums_NoQuery_Success() {
			await TestCollectionRetrieval(
				async () => (await _fixture.DataService.GetForums()).Items.OrderBy(f => f.Id),
				() => _fixture.SeedService.Forums.OrderBy(f => f.Id),
				(f1, f2) => f1.Id != f2.Id
			);
		}

		[Theory]
		[InlineData(1, 1)]
		[InlineData(2, 2)]
		[InlineData(3, 4)]
		[InlineData(6, 1)]
		[InlineData(2, 8)]
		public async Task ReadForums_Query_Success(int skipAmount, int takeAmount) {
			await TestCollectionQueryRetrieval(
				skipAmount,
				takeAmount,
				async () => await _fixture.DataService.GetForums(),
				async () => await _fixture.DataService.GetForums(new QueryOptions<ForumData> {
					Skip = skipAmount,
					Take = takeAmount
				})
			);
		}

		/* ----------------------------------------- Notices ----------------------------------------- */

		[Fact]
		public async Task ReadNotices_NoForumNoQuery_Success() {
			await TestCollectionRetrieval(
				async () => (await _fixture.DataService.GetNotices()).Items.OrderBy(n => n.Id),
				() => _fixture.SeedService.Notices.Where(n => n.ForumId == null).OrderBy(n => n.Id),
				(n1, n2) => n1.Id != n2.Id
			);
		}

		[Theory]
		[InlineData(1, 1)]
		[InlineData(2, 2)]
		[InlineData(3, 4)]
		public async Task ReadNotices_NoForumWithQuery_Success(int skipAmount, int takeAmount) {
			await TestCollectionQueryRetrieval(
				skipAmount,
				takeAmount,
				async () => await _fixture.DataService.GetNotices(),
				async () => await _fixture.DataService.GetNotices(new QueryOptions<NoticeData> {
					Skip = skipAmount,
					Take = takeAmount
				})
			);
		}

		[Fact]
		public async Task ReadNotices_WithForumNoQuery_Success() {
			var targetForum = _fixture.SeedService.Forums[1];

			await TestCollectionRetrieval(
				async () => (await _fixture.DataService.GetNotices(targetForum.Id)).Items.OrderBy(n => n.Id),
				() => _fixture.SeedService.Notices.Where(n => n.ForumId == targetForum.Id).OrderBy(n => n.Id),
				(n1, n2) => n1.Id != n2.Id
			);
		}

		[Theory]
		[InlineData(1, 1)]
		[InlineData(2, 2)]
		[InlineData(3, 4)]
		public async Task ReadNotices_WithForumWithQuery_Success(int skipAmount, int takeAmount) {
			var targetForum = _fixture.SeedService.Forums[1];

			await TestCollectionQueryRetrieval(
				skipAmount,
				takeAmount,
				async () => await _fixture.DataService.GetNotices(targetForum.Id),
				async () => await _fixture.DataService.GetNotices(targetForum.Id, new QueryOptions<NoticeData> {
					Skip = skipAmount,
					Take = takeAmount
				})
			);
		}

		/* ----------------------------------------- Posts ----------------------------------------- */
		[Fact]
		public async Task ReadPost_Success() {
			var targetPost = _fixture.SeedService.Posts[21];

			var post = await _fixture.DataService.GetPost(targetPost.Id);

			Assert.Equal(targetPost.Id, post.Id);
			Assert.Equal(targetPost.Content, post.Content);
			Assert.Equal(targetPost.CreatedOn, post.CreatedOn);
			Assert.Equal(targetPost.ThreadId, post.ThreadId);
		}

		[Fact]
		public async Task ReadPost_NonExistantItem_ReturnNull() {
			var post = await _fixture.DataService.GetPost(new Guid());

			Assert.Null(post);
		}


		[Fact]
		public async Task ReadPosts_NoQuery_Success() {
			var targetThread = _fixture.SeedService.Threads[2];

			await TestCollectionRetrieval(
				async () => (await _fixture.DataService.GetPosts(targetThread.Id)).Items.OrderBy(n => n.Id),
				() => _fixture.SeedService.Posts.Where(p => p.ThreadId == targetThread.Id).OrderBy(n => n.Id),
				(n1, n2) => n1.Id != n2.Id
			);
		}

		[Theory]
		[InlineData(1, 1)]
		[InlineData(2, 2)]
		[InlineData(3, 4)]
		public async Task ReadPosts_Query_Success(int skipAmount, int takeAmount) {
			var targetThread = _fixture.SeedService.Threads[4];

			await TestCollectionQueryRetrieval(
				skipAmount,
				takeAmount,
				async () => await _fixture.DataService.GetPosts(targetThread.Id),
				async () => await _fixture.DataService.GetPosts(targetThread.Id, new QueryOptions<PostData> {
					Skip = skipAmount,
					Take = takeAmount
				})
			);
		}

		/* ----------------------------------------- Reprimands ----------------------------------------- */

		[Fact]
		public async Task ReadReprimands_NoQuery_Success() {
			var targetUser = _fixture.SeedService.Users[5];

			await TestCollectionRetrieval(
				async () => (await _fixture.DataService.GetReprimands(targetUser.Id)).Items.OrderBy(n => n.Id),
				() => _fixture.SeedService.Reprimands.Where(p => p.AssignedTo.Id == targetUser.Id).OrderBy(n => n.Id),
				(n1, n2) => n1.Id != n2.Id
			);
		}

		[Fact]
		public async Task ReadReprimands_NoQuery_SuccessNoResults() {
			var targetUser = _fixture.SeedService.Users[1];

			var reprimands = await _fixture.DataService.GetReprimands(targetUser.Id);

			Assert.Empty(reprimands.Items);
		}

		[Theory]
		[InlineData(0, 1)]
		[InlineData(2, 2)]
		public async Task ReadReprimands_Query_Success(int skipAmount, int takeAmount) {
			var targetUser = _fixture.SeedService.Users[5];

			await TestCollectionQueryRetrieval(
				skipAmount,
				takeAmount,
				async () => await _fixture.DataService.GetReprimands(targetUser.Id),
				async () => await _fixture.DataService.GetReprimands(targetUser.Id, new QueryOptions<ReprimandData> {
					Skip = skipAmount,
					Take = takeAmount
				})
			);
		}

		/* ----------------------------------------- Threads ----------------------------------------- */

		[Fact]
		public async Task ReadThread_Success() {
			var targetThread = _fixture.SeedService.Threads[9];

			var thread = await _fixture.DataService.GetThread(targetThread.Id);

			Assert.Equal(targetThread.Id, thread.Id);
			Assert.Equal(targetThread.CreatedOn, thread.CreatedOn);
			Assert.Equal(targetThread.ForumId, thread.ForumId);
			Assert.Equal(targetThread.Name, thread.Name);
		}

		[Fact]
		public async Task ReadThread_NonExistantItem_ReturnNull() {
			var thread = await _fixture.DataService.GetThread(new Guid());

			Assert.Null(thread);
		}


		[Fact]
		public async Task ReadThreads_NoQuery_Success() {
			var targetForum = _fixture.SeedService.Forums[4];

			await TestCollectionRetrieval(
				async () => (await _fixture.DataService.GetThreads(targetForum.Id)).Items.OrderBy(n => n.Id),
				() => _fixture.SeedService.Threads.Where(t => t.ForumId == targetForum.Id).OrderBy(n => n.Id),
				(n1, n2) => n1.Id != n2.Id
			);
		}

		[Theory]
		[InlineData(1, 1)]
		[InlineData(6, 10)]
		[InlineData(80, 20)]
		public async Task ReadThreads_Query_Success(int skipAmount, int takeAmount) {
			var targetForum = _fixture.SeedService.Forums[2];

			await TestCollectionQueryRetrieval(
				skipAmount,
				takeAmount,
				async () => await _fixture.DataService.GetThreads(targetForum.Id),
				async () => await _fixture.DataService.GetThreads(targetForum.Id, new QueryOptions<ThreadData> {
					Skip = skipAmount,
					Take = takeAmount
				})
			);
		}

		/* ----------------------------------------- Users ----------------------------------------- */

		[Fact]
		public async Task ReadUser_Success() {
			var targetUser = _fixture.SeedService.Users[3];

			var thread = await _fixture.DataService.GetUser(targetUser.Id);

			Assert.Equal(targetUser.Id, thread.Id);
			Assert.Equal(targetUser.Caption, thread.Caption);
			Assert.Equal(targetUser.JoinedOn, thread.JoinedOn);
			Assert.Equal(targetUser.Name, thread.Name);
		}

		[Fact]
		public async Task ReadUser_NonExistantItem_ReturnNull() {
			var user = await _fixture.DataService.GetUser(new Guid());

			Assert.Null(user);
		}


		[Fact]
		public async Task ReadUsers_NoQuery_Success() {
			await TestCollectionRetrieval(
				async () => (await _fixture.DataService.GetUsers()).Items.OrderBy(u => u.Id),
				() => _fixture.SeedService.Users.OrderBy(u => u.Id),
				(n1, n2) => n1.Id != n2.Id
			);
		}

		[Theory]
		[InlineData(1, 1)]
		[InlineData(6, 10)]
		[InlineData(10, 15)]
		public async Task ReadUsers_Query_Success(int skipAmount, int takeAmount) {
			await TestCollectionQueryRetrieval(
				skipAmount,
				takeAmount,
				async () => await _fixture.DataService.GetUsers(),
				async () => await _fixture.DataService.GetUsers(new QueryOptions<UserData> {
					Skip = skipAmount,
					Take = takeAmount
				})
			);
		}

		/* ----------------------------------------- Util ----------------------------------------- */

		private async Task TestCollectionRetrieval<ReturnType>(
			Func<Task<IEnumerable<ReturnType>>> getResult,
			Func<IEnumerable<ReturnType>> getTarget,
			Func<ReturnType, ReturnType, bool> comparator
		) {
			var resultItems = await getResult();
			var targetItems = getTarget();

			Assert.Equal(targetItems.Count(), resultItems.Count());

			var mismatchFound = resultItems
				.Zip(targetItems, (f1, f2) => new { Result = f1, Target = f2 })
				.Any(r => comparator(r.Result, r.Target));
			Assert.True(!mismatchFound);
		}

		private async Task TestCollectionQueryRetrieval<ReturnType>(
			int skipAmount,
			int takeAmount,
			Func<Task<QueryResult<ReturnType>>> getAllItems,
			Func<Task<QueryResult<ReturnType>>> getTruncatedItems
 		) {
			var allItems = await getAllItems();
			var truncatedItems = await getTruncatedItems();

			Assert.NotEqual(truncatedItems.Items.FirstOrDefault(), allItems.Items.FirstOrDefault());

			var allItemCount = allItems.Items.Count();
			var truncatedItemCount = truncatedItems.Items.Count();
			Assert.Equal(
				skipAmount + takeAmount <= allItemCount
					? takeAmount
					: Math.Max(0, takeAmount + (allItemCount - (skipAmount + takeAmount))),
				truncatedItemCount
			);
		}
	}
}
