﻿using SKM.Models.Data.Update;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace SKM.Data.Test.Services.Data {
	public class EntityDataService_UpdateTests {
		/* ----------------------------------------- Favorites ----------------------------------------- */



		/* ----------------------------------------- Forums ----------------------------------------- */

		[Theory]
		[InlineData("Changed description", "//new.test.domain.skm", "Changed forum name")]
		public async Task UpdateForum_ValidInput_Success(string description, string iconUrl, string name) {
			using (var fixture = new DataFixture()) {
				var forumBefore = fixture.SeedService.Forums[2];

				var updatedForum = await fixture.DataService.UpdateForum(new ForumUpdateData(
					id: forumBefore.Id,
					description: description,
					iconUrl: iconUrl,
					name: name
				));

				var forumAfter = fixture.SeedService.Forums
					.Where(f => f.Id == forumBefore.Id)
					.FirstOrDefault();

				Assert.Equal(forumBefore.Id, updatedForum.Id);
				Assert.Equal(forumBefore.Id, forumAfter.Id);
				Assert.Null(forumBefore.UpdatedOn);
				Assert.NotNull(forumAfter.UpdatedOn);

				Assert.Equal(updatedForum.Description, forumAfter.Description);
				Assert.Equal(updatedForum.IconUrl, forumAfter.IconUrl);
				Assert.Equal(updatedForum.Name, forumAfter.Name);
				Assert.NotEqual(forumBefore.Description, forumAfter.Description);
				Assert.NotEqual(forumBefore.IconUrl, forumAfter.IconUrl);
				Assert.NotEqual(forumBefore.Name, forumAfter.Name);
			}
		}

		/* ----------------------------------------- Notices ----------------------------------------- */

		[Theory]
		[InlineData("Changed content", "Changed name")]
		public async Task UpdateNotice_ValidInput_Success(string content, string name) {
			using (var fixture = new DataFixture()) {
				var noticeBefore = fixture.SeedService.Notices[4];

				var updatedNotice = await fixture.DataService.UpdateNotice(new NoticeUpdateData(
					id: noticeBefore.Id,
					content: content,
					name: name
				));

				var noticeAfter = fixture.SeedService.Notices
					.Where(n => n.Id == noticeBefore.Id)
					.FirstOrDefault();

				Assert.Equal(noticeBefore.Id, updatedNotice.Id);
				Assert.Equal(noticeBefore.Id, noticeAfter.Id);
				Assert.Null(noticeBefore.UpdatedOn);
				Assert.NotNull(noticeAfter.UpdatedOn);

				Assert.Equal(updatedNotice.Content, noticeAfter.Content);
				Assert.Equal(updatedNotice.Name, noticeAfter.Name);
				Assert.NotEqual(noticeBefore.Content, noticeAfter.Content);
				Assert.NotEqual(noticeBefore.Name, noticeAfter.Name);
			}
		}

		/* ----------------------------------------- Posts ----------------------------------------- */

		[Theory]
		[InlineData("Changed content")]
		public async Task UpdatePost_ValidInput_Success(string content) {
			using (var fixture = new DataFixture()) {
				var postBefore = fixture.SeedService.Posts[11];

				var updatedPost = await fixture.DataService.UpdatePost(new PostUpdateData(
					id: postBefore.Id,
					content: content
				));

				var postAfter = fixture.SeedService.Posts
					.Where(p => p.Id == postBefore.Id)
					.FirstOrDefault();

				Assert.Equal(postBefore.Id, updatedPost.Id);
				Assert.Equal(postBefore.Id, postAfter.Id);
				Assert.Null(postBefore.UpdatedOn);
				Assert.NotNull(postAfter.UpdatedOn);

				Assert.Equal(updatedPost.Content, postAfter.Content);
				Assert.NotEqual(postBefore.Content, postAfter.Content);
			}
		}

		/* ----------------------------------------- Reprimands ----------------------------------------- */



		/* ----------------------------------------- Threads ----------------------------------------- */

		[Theory]
		[InlineData("//new.test.domain.skm", "Changed thread name")]
		public async Task UpdateThread_ValidInput_Success(string iconUrl, string name) {
			using (var fixture = new DataFixture()) {
				var threadBefore = fixture.SeedService.Threads[5];

				var updatedThread = await fixture.DataService.UpdateThread(new ThreadUpdateData(
					id: threadBefore.Id,
					iconUrl: iconUrl,
					name: name
				));

				var threadAfter = fixture.SeedService.Threads
					.Where(t => t.Id == threadBefore.Id)
					.FirstOrDefault();

				Assert.Equal(threadBefore.Id, updatedThread.Id);
				Assert.Equal(threadBefore.Id, threadAfter.Id);
				Assert.Null(threadBefore.UpdatedOn);
				Assert.NotNull(threadAfter.UpdatedOn);

				Assert.Equal(updatedThread.IconUrl, threadAfter.IconUrl);
				Assert.Equal(updatedThread.Name, threadAfter.Name);
				Assert.NotEqual(threadBefore.IconUrl, threadAfter.IconUrl);
				Assert.NotEqual(threadBefore.Name, threadAfter.Name);
			}
		}

		/* ----------------------------------------- Users ----------------------------------------- */
	}
}
