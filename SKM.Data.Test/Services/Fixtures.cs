﻿using Microsoft.EntityFrameworkCore;
using SKM.Data.Services;
using System;

namespace SKM.Data.Test.Services {
	public class DataFixture : IDisposable {
		private static int _instanceCount = 0;

		public EntityDataService DataService { get; }
		public DataSeedService SeedService { get; }

		public DataFixture() {
			var databaseName = $"skm-db-service-test-{++_instanceCount}";

			var options = new DbContextOptionsBuilder<SKMContext>()
				.UseInMemoryDatabase(databaseName: databaseName)
				.Options;

			DataService = new EntityDataService(new SKMContext(options));
			SeedService = new DataSeedService(new SKMContext(options), null);
			SeedService.Testing = true;
			SeedService.Seed().Wait();
		}

		public void Dispose() {
			DataService.Dispose();
			SeedService.Dispose();
		}
	}
}
