# SKM.Api

SKM is a test web app meant to demonstrate knowledge of/play around with various full-stack web technologies. The app is a simple forum system that allows for basic forums, threads and posts. The front-end is written as an SPA which talks to a back-end ASP.NET Core server. The front-end source code lives in the SKM.Client project (not included as part of the overall solution currently), while the back-end lives in all the others.

## Prerequisites

SKM is written in .NET Core 2.2, and hence requires the appropriate SDK in order to build. The project also assumes a SQL Server persistance layer, so you will also need to install and run SQL server in some manner in order for the project to function correctly. Most of development thus far has happened with the developer version of SQL Server Management Studio, so that is what's recommended.

## Building and Running the Project

Since SKM is written with .NET Core, it can theoretically be developed from any environment that supports a command line. The bulk of development has happened through Visual Studio 2019 however, so the project is best guaranteed to run from there. Building the project should simply entail opening the solution and building it. The project is currently on set up with two build configurations: Development and Production, whose respective purposes should be pretty clear. Once the project is up and running, you should be able to make queries against it through either http://localhost:63334 or https://localhost:44382. Many endpoints need to speak to a data source in order to function properly however. For first-time setups, the project contains a development-only seed endpoint useful for generating test data. You can call this by making a POST request to the following url: https://localhost:44382/api/seed.

## Technologies

The SKM back-end utilizes ASP.NET Core for handling web requests, Entity Framework Core for database model definitions and manipulation, and xUnit for testing.

## Solution Structure

The SKM back-end is split up into different projects in attempt to keep a clean separation of concerns across the app. The main projects are as follows:

- __SKM__: Common interface, class and function definitions to be used across the various projects
- __SKM.Api__: The main, public-facing ASP.NET Core project
- __SKM.Data__: The data access layer
- __SKM.Data.Test__: Unit tests for the data access layer

Entity Framework model definitions live in SKM.Data, and are projected through Automapper into a set of DTO classes defined in SKM onto the api layer whenever a request is made.

## Testing

The solution currently only contains a number of unit tests written with xUnit. These can be run from Visual Studio's test explorer.