﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using SKM.Api.Util;
using SKM.Data;
using SKM.Data.Models;
using SKM.Data.Services;
using SKM.Services;
using System;

namespace SKM.Api {
	public class Startup {
		private const string _corsPolicyName = "Default";

		public Startup(IConfiguration configuration) {
			Configuration = configuration;
		}

		public IConfiguration Configuration { get; }

		public void ConfigureServices(IServiceCollection services) {
			services
				.Configure<CookiePolicyOptions>(options => {
					options.CheckConsentNeeded = context => true;
					options.MinimumSameSitePolicy = SameSiteMode.None;
				})
				.Configure<IdentityOptions>(options => {
					options.Lockout.AllowedForNewUsers = true;
					options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(10);
					options.Lockout.MaxFailedAccessAttempts = 5;

					options.User.AllowedUserNameCharacters = $"{options.User.AllowedUserNameCharacters} ";
					options.User.RequireUniqueEmail = true;
				})
				.ConfigureApplicationCookie(options => {
					var cookieExpirationPeriod = TimeSpan.FromMinutes(60);

					options.Cookie.HttpOnly = true;
					options.Cookie.Name = "skm_cookie";
					options.Cookie.Expiration = cookieExpirationPeriod;
					options.ExpireTimeSpan = cookieExpirationPeriod;
					options.SlidingExpiration = true;
				});

			services
				.AddDbContext<SKMContext>(options => {
					options.UseSqlServer(Configuration.GetConnectionString("SKMDB"));
				})
#if DEBUG
				.AddTransient<DataSeedService, DataSeedService>()
#endif
				.AddTransient<IDataService, EntityDataService>();

			services
				.AddDefaultIdentity<ApplicationUser>()
				.AddEntityFrameworkStores<SKMContext>()
				.AddDefaultTokenProviders();

			services
				.AddCors(options => {
					options.AddPolicy(_corsPolicyName, builder => {
						builder.AllowCredentials();
						builder.WithOrigins("http://localhost:3150");
					});
				})
				.AddMvc()
				.AddJsonOptions(options => {
					options.SerializerSettings.Converters.Add(new GuidJsonConverter());
				})
				.SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

			services
				.AddSpaStaticFiles(config => {
					config.RootPath = "wwwroot";
				});
		}

		public void Configure(IApplicationBuilder application, IHostingEnvironment env) {
			if (env.IsDevelopment()) {
				application.UseDeveloperExceptionPage();
			} else {
				application.UseHsts();
			}

			application
				.UseHttpsRedirection()
				.UseStaticFiles()
				.UseSpaStaticFiles();

			application
				.UseCors(_corsPolicyName)
				.UseCookiePolicy()
				.UseAuthentication();

			application
				.UseMvc(routes => {
					routes.MapRoute(
						name: "default",
						template: "{controller}/{action=Index}"
					);
				})
				.UseSpa(spa => {
					spa.Options.SourcePath = "../SKM.Client";
				});
		}
	}
}
