﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using SKM.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKM.Api.Util {
	[ModelBinder(BinderType = typeof(QueryOptionsBinder))]
	public class RequestQueryOptions {
		public bool Reverse { get; set; } = false;
		public int? Skip { get; set; }
		public int? Take { get; set; }

		public QueryOptions<ResultType> ToDataServiceQueryOptions<ResultType>(Func<IQueryable<ResultType>, IQueryable<ResultType>> extension = null) {
			return new QueryOptions<ResultType> {
				Extension = extension,
				Skip = Skip,
				Take = Take
			};
		}

		public static class Params {
			public const string NextToken = "nextToken";
			public const string Reverse = "reverse";
			public const string Skip = "skip";
			public const string Take = "take";
		}

		public class NextToken {
			private readonly Dictionary<string, string> _dictionary;

			public NextToken(string encodedNextToken) {
				try {
					var decodedToken = Encoding.UTF8.GetString(Convert.FromBase64String(encodedNextToken));

					_dictionary = new Dictionary<string, string>();
					foreach (var paramClause in decodedToken.Split('&')) {
						var paramValuePair = paramClause.Split('=');

						_dictionary[paramValuePair[0]] = paramValuePair[1];
					}
				} catch (Exception) {
					throw new ArgumentException("Invalid nextToken parameter");
				}
			}

			public string this[string paramName] {
				get {
					string value;
					_dictionary.TryGetValue(paramName, out value);

					return value;
				}
			}

			public RequestQueryOptions GetQueryOptions() {
				try {
					return new RequestQueryOptions {
						Reverse = bool.Parse(_dictionary[Params.Reverse]),
						Skip = int.Parse(_dictionary[Params.Skip]),
						Take = int.Parse(_dictionary[Params.Take])
					};
				} catch (Exception) {
					throw new ArgumentException("Invalid nextToken parameter");
				}
			}
		}
	}

	public class RequestQueryResult<ResultType> : QueryResult<ResultType> {
		public bool Reversed { get; set; }

		public RequestQueryResult(
			IEnumerable<ResultType> items,
			int skipAmount,
			int totalCount,
			bool? reversed
		) : base(items, skipAmount, totalCount) {
			Reversed = reversed ?? false;
		}

		public RequestQueryResult(
			QueryResult<ResultType> other,
			bool? reversed
		) : base(other.Items, other.SkipAmount, other.TotalCount) {
			Reversed = reversed ?? false;
		}

		public string Encode() {
			var itemCount = Items.Count();
			var skipAmount = SkipAmount + itemCount;

			return skipAmount < TotalCount
				? Convert.ToBase64String(Encoding.UTF8.GetBytes(
					$"{RequestQueryOptions.Params.Reverse}={Reversed}&{RequestQueryOptions.Params.Skip}={skipAmount}&{RequestQueryOptions.Params.Take}={itemCount}"
				))
				: null;
		}
	}

	public class QueryOptionsBinder : IModelBinder {
		public Task BindModelAsync(ModelBindingContext bindingContext) {
			if (bindingContext == null)
				throw new ArgumentNullException();

			RequestQueryOptions optionsParam = null;
			var nextTokenParam = bindingContext.ValueProvider.GetValue(RequestQueryOptions.Params.NextToken).FirstValue;
			if (nextTokenParam != null)
				optionsParam = new RequestQueryOptions.NextToken(nextTokenParam).GetQueryOptions();

			if (optionsParam == null) {
				optionsParam = new RequestQueryOptions();
				var reverseParam = bindingContext.ValueProvider.GetValue(RequestQueryOptions.Params.Reverse).FirstValue;
				if (reverseParam != null) {
					optionsParam.Reverse = bool.Parse(reverseParam);
				}

				var skipParam = bindingContext.ValueProvider.GetValue(RequestQueryOptions.Params.Skip).FirstValue;
				if (skipParam != null) {
					optionsParam.Skip = int.Parse(skipParam);

					if (optionsParam.Skip < 0)
						throw new ArgumentException("The skip parameter cannot be negative");
				}

				var takeParam = bindingContext.ValueProvider.GetValue(RequestQueryOptions.Params.Take).FirstValue;
				if (takeParam != null) {
					optionsParam.Take = int.Parse(takeParam);

					if (optionsParam.Take < 0)
						throw new ArgumentException("The take parameter cannot be negative");
				}
			}

			bindingContext.Result = ModelBindingResult.Success(optionsParam);

			return Task.CompletedTask;
		}
	}
}
