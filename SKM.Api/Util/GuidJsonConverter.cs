﻿using Newtonsoft.Json;
using System;

namespace SKM.Api.Util {
	internal class GuidJsonConverter : JsonConverter<Guid> {
		public override Guid ReadJson(JsonReader reader, Type objectType, Guid existingValue, bool hasExistingValue, JsonSerializer serializer) {
			return new Guid((string) reader.Value);
		}

		public override void WriteJson(JsonWriter writer, Guid value, JsonSerializer serializer) {
			writer.WriteValue(value.ToString("N"));
		}
	}
}
