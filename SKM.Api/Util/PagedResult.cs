﻿using System.Collections.Generic;

namespace SKM.Api.Util {
	public struct PagedResult<ResultType> {
		public IEnumerable<ResultType> Items { get; private set; }
		public string NextToken { get; private set; }
		public bool Reversed { get; private set; }
		public int TotalCount { get; private set; }

		public PagedResult(
			IEnumerable<ResultType> results,
			string nextToken,
			bool reversed,
			int totalCount
		) {
			Items = results;
			NextToken = nextToken;
			Reversed = reversed;
			TotalCount = totalCount;
		}
	}
}
