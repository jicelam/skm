﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SKM.Models.Data;
using SKM.Models.Data.Update;
using SKM.Services;
using System;
using System.Threading.Tasks;

namespace SKM.Api.Controllers {
	[ApiController]
	[Route("api/posts")]
	public class PostController : ApiController {
		private readonly IDataService _dataService;

		public PostController(IDataService dataService) {
			_dataService = dataService;
		}

		[HttpGet("{postId}")]
		public async Task<ActionResult<PostData>> GetPost(Guid postId) {
			var result = await _dataService.GetPost(postId);
			if (result == null)
				return NotFound();

			return Ok(result);
		}

		[Authorize]
		[HttpPut("{postId}/edit")]
		public async Task<ActionResult<PostData>> UpdatePost(Guid postId, [FromForm] PostUpdateFormData data) {
			var result = await _dataService.UpdatePost(new PostUpdateData(
				id: postId,
				data.Content
			));

			return Ok(result);
		}

		[Authorize]
		[HttpDelete("{postId}")]
		public async Task<ActionResult> DeleteForum(Guid postId) {
			await _dataService.DeletePost(postId);

			return Ok();
		}
	}

	public class PostUpdateFormData {
		public string Content { get; set; }
	}
}
