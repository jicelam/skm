﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SKM.Api.Util;
using SKM.Data.Models;
using SKM.Models.Data;
using SKM.Models.Data.Create;
using SKM.Models.Data.Update;
using SKM.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SKM.Api.Controllers {
	[ApiController]
	[Route("api/threads")]
	public class ThreadController : ApiController {
		private readonly IDataService _dataService;

		private readonly UserManager<ApplicationUser> _userManager;

		public ThreadController(
			IDataService dataService,
			UserManager<ApplicationUser> userManager
		) {
			_dataService = dataService;

			_userManager = userManager;
		}

		[HttpGet("{threadId}")]
		public async Task<ActionResult<ThreadData>> GetThread(Guid threadId) {
			var result = await _dataService.GetThread(threadId);
			if (result == null)
				return NotFound();

			return Ok(result);
		}

		[HttpGet("{threadId}/posts")]
		public async Task<ActionResult<PagedResult<PostData>>> GetPosts(Guid threadId, RequestQueryOptions options) {
			var results = await _dataService.GetPosts(threadId, options.ToDataServiceQueryOptions<PostData>(
				threadQuery => !options.Reverse
					? threadQuery.OrderBy(t => t.CreatedOn)
					: threadQuery.OrderByDescending(t => t.CreatedOn)
			));

			return PagedResult(new RequestQueryResult<PostData>(results, options.Reverse));
		}

		[Authorize]
		[HttpPost("{threadId}/posts/new")]
		public async Task<ActionResult<PostData>> CreatePost(Guid threadId, [FromForm] PostCreateFormData formData) {
			var currentUser = await _userManager.GetUserAsync(User);

			var result = await _dataService.CreatePost(new PostCreateData(
				threadId: threadId,
				content: formData.Content,
				createdById: currentUser.Uuid
			));

			return Ok(result);
		}

		[Authorize]
		[HttpPut("{threadId}/edit")]
		public async Task<ActionResult<ThreadData>> UpdateThread(Guid threadId, [FromForm] ThreadUpdateFormData formData) {
			var result = await _dataService.UpdateThread(new ThreadUpdateData(
				id: threadId,
				iconUrl: formData.IconUrl,
				name: formData.Name
			));

			return Ok(result);
		}

		[Authorize]
		[HttpDelete("{threadId}")]
		public async Task<ActionResult> DeleteForum(Guid threadId) {
			await _dataService.DeleteThread(threadId);

			return Ok();
		}
	}

	public class PostCreateFormData {
		public string Content { get; set; }
	}

	public class ThreadUpdateFormData {
		public string IconUrl { get; set; }
		public string Name { get; set; }
	}
}
