﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SKM.Api.Util;
using SKM.Data.Models;
using SKM.Models.Data;
using SKM.Models.Data.Create;
using SKM.Models.Data.Update;
using SKM.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SKM.Api.Controllers {
	[ApiController]
	[Route("api/forums")]
	public class ForumController : ApiController {
		private readonly IDataService _dataService;

		private readonly UserManager<ApplicationUser> _userManager;

		public ForumController(
			IDataService dataService,
			UserManager<ApplicationUser> userManager
		) {
			_dataService = dataService;

			_userManager = userManager;
		}

		[HttpGet]
		public async Task<ActionResult<PagedResult<ForumData>>> Get(RequestQueryOptions options) {
			var results = await _dataService.GetForums(options.ToDataServiceQueryOptions<ForumData>(
				forumQuery => !options.Reverse
					? forumQuery.OrderBy(f => f.Name)
					: forumQuery.OrderByDescending(f => f.Name)
			));

			return PagedResult(new RequestQueryResult<ForumData>(results, options.Reverse));
		}

		[HttpGet("{forumId}")]
		public async Task<ActionResult<ForumData>> GetForum(Guid forumId) {
			var result = await _dataService.GetForum(forumId);
			if (result == null)
				return NotFound();

			return Ok(result);
		}

		[HttpGet("{forumId}/notices")]
		public async Task<ActionResult<PagedResult<NoticeData>>> GetNotices(Guid forumId, RequestQueryOptions options) {
			var results = await _dataService.GetNotices(forumId, options.ToDataServiceQueryOptions<NoticeData>(
				noticeQuery => !options.Reverse
					? noticeQuery.OrderByDescending(n => n.CreatedOn)
					: noticeQuery.OrderBy(n => n.CreatedOn)
			));

			return PagedResult(new RequestQueryResult<NoticeData>(results, options.Reverse));
		}

		[HttpGet("{forumId}/threads")]
		public async Task<ActionResult<PagedResult<ThreadData>>> GetThreads(Guid forumId, RequestQueryOptions options) {
			var results = await _dataService.GetThreads(forumId, options.ToDataServiceQueryOptions<ThreadData>(
				threadQuery => !options.Reverse
					? threadQuery.OrderByDescending(t => t.LastPostOn)
					: threadQuery.OrderBy(t => t.LastPostOn)
			));

			return PagedResult(new RequestQueryResult<ThreadData>(results, options.Reverse));
		}

		[Authorize]
		[HttpPost("new")]
		public async Task<ActionResult<ForumData>> CreateForum([FromForm] ForumCreateFormData data) {
			var currentUser = await _userManager.GetUserAsync(User);

			var result = await _dataService.CreateForum(new ForumCreateData(
				name: data.Name,
				createdById: currentUser.Uuid,
				description: data.Description,
				iconUrl: data.IconUrl
			));

			return Ok(result);
		}

		[Authorize]
		[HttpPost("{forumId}/threads/new")]
		public async Task<ActionResult<ThreadData>> CreateThread(Guid forumId, [FromForm] ThreadCreateFormData formData) {
			var currentUser = await _userManager.GetUserAsync(User);

			var result = await _dataService.CreateThread(new ThreadCreateData(
				forumId: forumId,
				content: formData.Content,
				createdById: currentUser.Uuid,
				iconUrl: formData.IconUrl,
				name: formData.Name
			));

			return Ok(result);
		}

		[Authorize]
		[HttpPut("{forumId}/edit")]
		public async Task<ActionResult<ForumData>> UpdateForum(Guid forumId, [FromForm] ForumUpdateFormData data) {
			var result = await _dataService.UpdateForum(new ForumUpdateData(
				id: forumId,
				description: data.Description,
				iconUrl: data.IconUrl,
				name: data.Name
			));

			return Ok(result);
		}

		[Authorize]
		[HttpDelete("{forumId}")]
		public async Task<ActionResult> DeleteForum(Guid forumId) {
			await _dataService.DeleteForum(forumId);

			return Ok();
		}
	}

	public class ForumCreateFormData {
		public string Description { get; set; }
		public string IconUrl { get; set; }
		public string Name { get; set; }
	}

	public class ThreadCreateFormData {
		public string Content { get; set; }
		public string IconUrl { get; set; }
		public string Name { get; set; }
	}

	public class ForumUpdateFormData {
		public string Description { get; set; }
		public string IconUrl { get; set; }
		public string Name { get; set; }
	}
}
