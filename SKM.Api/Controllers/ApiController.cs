﻿using Microsoft.AspNetCore.Mvc;
using SKM.Api.Util;

namespace SKM.Api.Controllers {
	public class ApiController : ControllerBase {
		protected ActionResult<PagedResult<ResultType>> PagedResult<ResultType>(RequestQueryResult<ResultType> result) {
			return Ok(new PagedResult<ResultType>(
				result.Items,
				result.Encode(),
				result.Reversed,
				result.TotalCount
			));
		}
	}
}
