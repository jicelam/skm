﻿using Microsoft.AspNetCore.Mvc;
using SKM.Api.Util;
using SKM.Models.Data;
using SKM.Services;
using System.Linq;
using System.Threading.Tasks;

namespace SKM.Api.Controllers {
	[ApiController]
	[Route("api")]
	public class HomeController : ApiController {
		private readonly IDataService _dataService;

		public HomeController(IDataService dataService) {
			_dataService = dataService;
		}

		[HttpGet("notices")]
		public async Task<ActionResult<PagedResult<NoticeData>>> GetNotices(RequestQueryOptions options) {
			var results = await _dataService.GetNotices(options.ToDataServiceQueryOptions<NoticeData>(
				noticeQuery => !options.Reverse
					? noticeQuery.OrderByDescending(n => n.CreatedOn)
					: noticeQuery.OrderBy(n => n.CreatedOn)
			));

			return PagedResult(new RequestQueryResult<NoticeData>(results, options.Reverse));
		}
	}
}
