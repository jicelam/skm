﻿using Microsoft.AspNetCore.Mvc;
using SKM.Data;
using System.Threading.Tasks;

namespace SKM.Api.Controllers {
#if DEBUG
	[ApiController]
	[Route("api")]
	public class SeedController : ApiController {
		private readonly DataSeedService _seedService;

		public SeedController(DataSeedService seedService) {
			_seedService = seedService;
		}

		[HttpPost("seed")]
		public async Task<ActionResult> Seed() {
			await _seedService.Seed();

			return Ok("Seed complete");
		}
	}
#endif
}
