﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SKM.Models.Data.Create;
using SKM.Services;
using System.Threading.Tasks;

namespace SKM.Api.Controllers {
	[ApiController]
	[Route("api/mods")]
	public class ModController : ApiController {
		private readonly IDataService _dataService;

		public ModController(IDataService dataService) {
			_dataService = dataService;
		}

		[Authorize]
		[HttpPost("reprimand")]
		public async Task<ActionResult> ReprimandUser([FromForm] ReprimandCreateData data) {
			await _dataService.ReprimandUser(data);

			return Ok();
		}
	}
}
