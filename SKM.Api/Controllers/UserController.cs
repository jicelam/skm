﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SKM.Api.Util;
using SKM.Data;
using SKM.Data.Models;
using SKM.Models.Data;
using SKM.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SKM.Api.Controllers {
	[ApiController]
	[Route("api/users")]
	public class UserController : ApiController {
		private readonly IDataService _dataService;

		private readonly SignInManager<ApplicationUser> _signInManager;
		private readonly UserManager<ApplicationUser> _userManager;

		public UserController(
			IDataService dataService,
			SignInManager<ApplicationUser> signInManager,
			UserManager<ApplicationUser> userManager
		) {
			_dataService = dataService;

			_signInManager = signInManager;
			_userManager = userManager;
		}

		[HttpGet("me")]
		public async Task<ActionResult<UserData>> Get() {
			var currentUser = await _userManager.GetUserAsync(User);
			if (currentUser == null)
				return NotFound();

			return Ok(Mapper.Instance.Map<UserData>(currentUser));
		}

		[HttpGet("{userId}")]
		public async Task<ActionResult<UserData>> Get(Guid userId) {
			var result = await _dataService.GetUser(userId);
			if (result == null)
				return NotFound();

			return Ok(result);
		}

		[HttpGet("{userId}/favorites")]
		public async Task<ActionResult<PagedResult<FavoritedItemData>>> GetFavorites(Guid userId, RequestQueryOptions options) {
			var results = await _dataService.GetFavorites(userId, options.ToDataServiceQueryOptions<FavoritedItemData>(
				favoriteQuery => !options.Reverse
					? favoriteQuery.OrderBy(fi => fi.Type)
					: favoriteQuery.OrderByDescending(fi => fi.Type)
			));

			return PagedResult(new RequestQueryResult<FavoritedItemData>(results, options.Reverse));
		}

		[HttpGet("{userId}/reprimands")]
		public async Task<ActionResult<PagedResult<ReprimandData>>> GetReprimands(Guid userId, RequestQueryOptions options) {
			var results = await _dataService.GetReprimands(userId, options.ToDataServiceQueryOptions<ReprimandData>(
				reprimandQuery => !options.Reverse
					? reprimandQuery.OrderBy(r => r.Type).ThenByDescending(r => r.AssignedOn)
					: reprimandQuery.OrderByDescending(r => r.Type).ThenBy(r => r.AssignedOn)
			));

			return PagedResult(new RequestQueryResult<ReprimandData>(results, options.Reverse));
		}

		[HttpPost("register")]
		public async Task<ActionResult> Register([FromForm] RegistrationData data) {
			var user = new ApplicationUser {
				AvatarUrl = data.AvatarUrl,
				Caption = data.Caption,
				Email = data.Email,
				Location = data.Location,
				Signature = data.Signature,
				UserName = data.Name
			};
			var registrationResult = await _userManager.CreateAsync(user, data.Password);
			if (!registrationResult.Succeeded)
				throw new ApplicationException("User registration failed");

			await _signInManager.SignInAsync(user, false);

			return Ok();
		}

		[HttpPost("login")]
		public async Task<ActionResult> LogIn([FromForm] LoginData data) {
			var result = await _signInManager.PasswordSignInAsync(data.Name, data.Password, false, false);
			if (!result.Succeeded)
				throw new ApplicationException("Login failed");

			return Ok();
		}

		[HttpPost("logout")]
		public async Task<ActionResult> LogOut() {
			await _signInManager.SignOutAsync();

			return Ok();
		}
	}

	public class RegistrationData {
		public string AvatarUrl { get; set; }
		public string Caption { get; set; }
		public string Email { get; set; }
		public string Location { get; set; }
		public string Name { get; set; }
		public string Password { get; set; }
		public string Signature { get; set; }
	}

	public class LoginData {
		public string Name { get; set; }
		public string Password { get; set; }
	}
}
