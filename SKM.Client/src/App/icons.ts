import { library } from "@fortawesome/fontawesome-svg-core";
import {
	faAngleDown,
	faCog,
	faEdit,
	faEnvelope,
	faExclamationCircle,
	faSignInAlt,
	faSpinner,
	faUserCircle
} from "@fortawesome/free-solid-svg-icons";

library.add(
	faAngleDown,
	faCog,
	faEdit,
	faEnvelope,
	faExclamationCircle,
	faSignInAlt,
	faSpinner,
	faUserCircle
);