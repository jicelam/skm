import * as React from "react";
import Loadable, { LoadingComponentProps } from "react-loadable";

import { Loading } from "Components";
import { ErrorView } from "Views/Error";

export { ErrorView } from "Views/Error";

const ViewLoader = ({ error }: LoadingComponentProps) => (
	error
		? <ErrorView message={error.message} />
		: <Loading />
);

export const LoadableHomeView = Loadable({
	loader: async () => {
		const module = await import(
			/* webpackChunkName: "home-view" */
			"Views/Home"
		);

		return module.HomeView;
	},
	loading: ViewLoader
});

export const LoadableForumView = Loadable({
	loader: async () => {
		const module = await import(
			/* webpackChunkName: "forum-view" */
			"Views/Forum"
		);

		return module.ForumView;
	},
	loading: ViewLoader
});

export const LoadableLoginView = Loadable({
	loader: async () => {
		const module = await import(
			/* webpackChunkName: "login-view" */
			"Views/Login"
		);

		return module.LoginView;
	},
	loading: ViewLoader
});

export const LoadableLogoutView = Loadable({
	loader: async () => {
		const module = await import(
			/* webpackChunkName: "logout-view" */
			"Views/Logout"
		);

		return module.LogoutView;
	},
	loading: ViewLoader
});

export const LoadableNewThreadView = Loadable({
	loader: async () => {
		const module = await import(
			/* webpackChunkName: "forum-view" */
			"Views/Thread/New"
		);

		return module.NewThreadView;
	},
	loading: ViewLoader
});

export const LoadableThreadView = Loadable({
	loader: async () => {
		const module = await import(
			/* webpackChunkName: "thread-view" */
			"Views/Thread"
		);

		return module.ThreadView;
	},
	loading: ViewLoader
});

export const LoadableNewPostView = Loadable({
	loader: async () => {
		const module = await import(
			/* webpackChunkName: "thread-view" */
			"Views/Post/New"
		);

		return module.NewPostView;
	},
	loading: ViewLoader
});

export const LoadableUserView = Loadable({
	loader: async () => {
		const module = await import(
			/* webpackChunkName: "user-view" */
			"Views/User"
		);

		return module.UserView;
	},
	loading: ViewLoader
});