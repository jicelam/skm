import * as React from "react";
import { Route, Switch } from "react-router-dom";

import {
	ErrorView,
	LoadableForumView,
	LoadableHomeView,
	LoadableLoginView,
	LoadableLogoutView,
	LoadableNewPostView,
	LoadableNewThreadView,
	LoadableThreadView,
	LoadableUserView
} from "./views";

export const Routes = () => (
	<Switch>
		<Route path="/" component={LoadableHomeView} exact />
		<Route path="/forums/:id" component={LoadableForumView} exact />
		<Route path="/forums/:forumId/threads/new" component={LoadableNewThreadView} exact />
		<Route path="/login" component={LoadableLoginView} exact />
		<Route path="/logout" component={LoadableLogoutView} exact />
		<Route path="/threads/:threadId" component={LoadableThreadView} exact />
		<Route path="/threads/:threadId/posts/new" component={LoadableNewPostView} exact />
		<Route path="/users/:id" component={LoadableUserView} exact />
		<Route render={props => <ErrorView {...props} message="Not Found" />} />
	</Switch>
);