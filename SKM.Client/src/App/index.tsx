import { inject, observer, Provider } from "mobx-react";
import * as React from "react";
import * as ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";

import { Loading, Modal } from "Components";
import { InjectedComponent } from "Views/base";
import { HeaderView } from "Views/Header";
import { LogoutViewModel } from "Views/Logout/model";

import "./icons";
import { AppModel, AppModelConfig, AppViewModel } from "./model";
import { Routes } from "./routes";
import * as styles from "./styles.css";

export { AppModel, AppViewModel, PageViewModel } from "./model";

export interface AppContext {
	model: AppModel;
}

export interface AppProps {
	model?: AppModelConfig;

	onLoad?(): Promise<void>;
}

export class App extends React.Component<AppProps> {
	private static _model: AppModel;

	constructor(props: AppProps, context?: {}) {
		super(props, context);

		const { model } = this.props;
		App._model = new AppModel(model);
	}

	render(): JSX.Element {
		return (
			<BrowserRouter>
				<Provider model={App._model}>
					<AppView />
				</Provider>
			</BrowserRouter>
		);
	}

	async componentDidMount(): Promise<void> {
		const { onLoad } = this.props;

		const appElement = ReactDOM.findDOMNode(this) as HTMLElement;
		const rootElement = appElement.parentElement as HTMLElement;

		Modal.rootElementId = rootElement.id;

		onLoad && await onLoad();

		await App._model.load();

		this.forceUpdate();
	}
}

interface AppViewProps {}

interface AppViewInjected {
	model: AppViewModel;
}

@inject((context: AppContext) => ({
	model: context.model.view
}) as AppViewInjected)
@observer
class AppView extends InjectedComponent<AppViewProps, AppViewInjected> {
	render(): JSX.Element {
		const { model } = this.injected;

		return (
			<div className={styles.app}>
				{this.renderHeader()}
				<div className={styles.appView}>
					{model.loading
						? <Loading />
						: <Routes />
					}
				</div>
			</div>
		);
	}

	private renderHeader(): JSX.Element | null {
		const { model } = this.injected;

		return (
			!model.current || model.current instanceof LogoutViewModel
				? null
				: <HeaderView />
		);
	}
}