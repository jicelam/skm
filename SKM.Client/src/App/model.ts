import { action, observable } from "mobx";

import { User } from "Data";
import serviceLocator, {
	DataService,
	SecurityService,
	ServiceName
} from "Services";

export interface AppModelConfig {
	data: AppDataModelConfig;
	view: AppViewModelConfig;
}

export class AppModel implements AppModelConfig {
	data: AppDataModel;
	view: AppViewModel;

	private _loading = true;
	get loading(): boolean { return this._loading; }

	constructor(config: AppModelConfig = {} as AppModelConfig) {
		this.data = new AppDataModel(config.data);
		this.view = new AppViewModel(config.view);
	}

	async load(): Promise<void> {
		await this.view.load();

		this._loading = false;
	}
}

export interface AppDataModelConfig {}

export class AppDataModel implements AppDataModelConfig {
	constructor(config: AppDataModelConfig) {}
}

export interface AppViewModelConfig {
	current: PageViewModel | null;
}

export interface PageViewModel {
	title: string | null;
}

export class AppViewModel implements AppViewModelConfig {
	@observable
	loading = true;

	@observable
	current: PageViewModel | null = null;

	@observable
	profileOpen = false;

	get currentUser(): User | null {
		return this._securityService.currentUser;
	}

	private _dataService: DataService;
	private _securityService: SecurityService;

	constructor(config: AppViewModelConfig = {} as AppViewModel) {
		this._dataService = serviceLocator.get(ServiceName.Data);
		this._securityService = serviceLocator.get(ServiceName.Security);
	}

	@action.bound
	async load(): Promise<void> {
		this.loading = false;
	}

	@action.bound
	setCurrentView(view: PageViewModel): void {
		this.current = view;
	}

	@action.bound
	setProfileOpen(value: boolean): void {
		this.profileOpen = value;
	}
}