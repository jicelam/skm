import { User, UserInfo } from "./users";

export interface Forum {
	readonly description: string;
	readonly iconUrl: string;
	readonly id: string;
	readonly name: string;
	readonly pages: number;
}

export interface Thread {
	readonly createdBy: UserInfo;
	readonly createdOn: Date;
	readonly iconUrl: string;
	readonly id: string;
	readonly lastPostBy: UserInfo;
	readonly lastPostOn: Date;
	readonly name: string;
	readonly pages: number;
}

export interface Post {
	readonly content: string;
	readonly editedOn: Date | null;
	readonly id: string;
	readonly postedBy: User;
	readonly postedOn: Date;
}