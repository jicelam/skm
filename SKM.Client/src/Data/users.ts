export interface UserInfo {
	readonly avatarUrl: string | null;
	readonly name: string;
	readonly id: string;
}

export interface User extends UserInfo {
	readonly caption: string | null;
	readonly joinedOn: Date;
	readonly location: string | null;
	readonly signature: string | null;
}

export interface Reprimand {
	readonly assignedBy: UserInfo;
	readonly assignedOn: Date;
	readonly assignedTo: UserInfo;
	readonly expiresOn: Date | null;
	readonly forumId: string | null;
	readonly id: string;
	readonly reason: string;
	readonly type: ReprimandType;
}

export const enum ReprimandType {
	Ban = 0,
	Probation = 1
}