export interface Notice {
	id: string;
	name: string;
	publishedOn: Date;
	summary: string;
}