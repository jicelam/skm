import * as classnames from "classnames";
import * as moment from "moment";
import * as React from "react";

import { Strings } from "Assets";
import { ActionButton, Card, CardSection, CardFooter } from "Components";

import * as styles from "./styles.css";

export interface UserProfileProps {
	name: string;
	avatarUrl: string | null;
	caption: string | null;
	joinedOn: Date;
	location: string | null;
	signature: string | null;

	className?: string;
	style?: React.CSSProperties;

	onSendPMClick(): void;
}

export class UserProfile extends React.Component<UserProfileProps> {
	render(): JSX.Element {
		const { name, caption, joinedOn, location, signature, className, style, onSendPMClick } = this.props;

		return (
			<Card
				className={classnames(
					styles.userProfile,
					className
				)}
				style={style}
			>
				<CardSection className={styles.userProfileSummary}>
					<div className={styles.userProfileAvatar} />
					<div className={styles.userProfileName}>{name}</div>
					{caption && (
						<div className={styles.userProfileCaption}>{caption}</div>
					)}
					<div className={styles.userProfileJoinedOn}>{`${Strings.UserViewJoinedOn}: ${moment(joinedOn).format("ll")}`}</div>
					{location && (
						<div className={styles.userProfileLocation}>{location}</div>
					)}
				</CardSection>
				{true && (
					<CardSection className={styles.userProfileSignature}>
						{signature || Strings.UserViewNoSignatureMessage}
					</CardSection>
				)}
				<CardFooter className={styles.userProfileButtonRow}>
					<ActionButton
						className={styles.userProfileSendPmButton}
						onClick={onSendPMClick}
					>
						{Strings.UserViewSendPMMessage}
					</ActionButton>
				</CardFooter>
			</Card>
		);
	}
}