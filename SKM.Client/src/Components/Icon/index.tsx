import * as classnames from "classnames";
import * as React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "@fortawesome/free-solid-svg-icons";
import { IconProp } from "@fortawesome/fontawesome-svg-core";

import * as styles from "./styles.css";

export interface IconProps {
	icon: IconProp;
	spin?: boolean;
	pulse?: boolean;

	className?: string;
	style?: React.CSSProperties;
	title?: string;

	onClick?(event: React.MouseEvent<HTMLElement>): void;
}

export class Icon extends React.Component<IconProps> {
	render(): JSX.Element {
		const { icon, spin, pulse, className, style, title, onClick } = this.props;

		return (
			<span
				className={classnames(
					styles.icon,
					className
				)}
				style={style}
				title={title}
				onClick={onClick}
			>
				<FontAwesomeIcon
					icon={icon}
					spin={spin}
					pulse={pulse}
				/>
			</span>
		);
	}
}