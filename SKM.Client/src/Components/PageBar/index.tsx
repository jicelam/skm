import autobind from "autobind-decorator";
import * as classnames from "classnames";
import * as React from "react";

import { Strings } from "Assets";
import {
	ConfirmButton,
	CancelButton,
	Form,
	FormField,
	FormError,
	Input,
	Modal,
	ModalBody,
	ModalFooter
} from "Components";

import * as styles from "./styles.css";

export interface PageBarProps {
	selectedPageNumber: number;
	maxPageNumber?: number;

	className?: string;
	style?: React.CSSProperties;

	onPageSelect(pageNumber: number): void;
}

interface PageBarState {
	modalOpen: boolean;
	modalError: string | null;
}

export class PageBar extends React.Component<PageBarProps, PageBarState> {
	private _modal: Modal | null = null;
	private _goToPageInput: Input | null = null;

	constructor(props: PageBarProps, context?: {}) {
		super(props, context);

		this.state = {
			modalOpen: false,
			modalError: null
		};
	}

	render(): JSX.Element {
		const { selectedPageNumber, maxPageNumber, className, style, onPageSelect } = this.props;
		const { modalOpen, modalError } = this.state;

		const onFirstPage = selectedPageNumber <= 1;
		const onLastPage = maxPageNumber != null && (selectedPageNumber >= maxPageNumber);

		return (
			<div
				className={classnames(
					styles.pageBar,
					className
				)}
				style={style}
			>
				<button
					className={styles.pageBarBack}
					type="button"
					disabled={onFirstPage}
					onClick={() => onPageSelect(selectedPageNumber - 1)}
				>
					{Strings.PageBarBack}
				</button>
				<button
					className={styles.pageBarCurrent}
					type="button"
					onClick={this.onGoToPageClick}
				>
					{selectedPageNumber}
				</button>
				<button
					className={styles.pageBarNext}
					type="button"
					disabled={onLastPage}
					onClick={() => onPageSelect(selectedPageNumber + 1)}
				>
					{Strings.PageBarNext}
				</button>
				{modalOpen && (
					<Modal
						ref={elem => this._modal = elem}
						onCloseAction={this.onModalClose}
					>
						<ModalBody>
							<Form>
								<FormField title={Strings.PageBarGoTo}>
									<Input
										ref={elem => this._goToPageInput = elem}
										className={styles.pageBarInput}
										defaultValue={`${selectedPageNumber}`}
										min={1}
										max={maxPageNumber}
										placeholder={Strings.PageBarInputPlaceholder}
										type="number"
										onChange={this.validatePageNumber}
									/>
								</FormField>
								{modalError && (
									<FormError>{modalError}</FormError>
								)}
							</Form>
						</ModalBody>
						<ModalFooter>
							<CancelButton onClick={this.onModalClose}>
								{Strings.ButtonCancel}
							</CancelButton>
							<ConfirmButton 
								onClick={this.onModalSubmit}
								disabled={!!modalError}
							>
								{Strings.ButtonOk}
							</ConfirmButton>
						</ModalFooter>
					</Modal>
				)}
			</div>
		);
	}

	@autobind
	private onGoToPageClick(): void {
		this.setState({ modalOpen: true });
	}

	@autobind
	private async onModalSubmit(): Promise<void> {
		if (this.validatePageNumber()) {
			const pageNumber = +this._goToPageInput!.value;

			await this.onModalClose();

			this.props.onPageSelect(pageNumber);
		}
	}

	@autobind
	private async onModalClose(): Promise<void> {
		await this._modal!.startClose();

		this.setState({ modalOpen: false, modalError: null });

		this._modal = null;
		this._goToPageInput = null;
	}

	@autobind
	private validatePageNumber(): boolean {
		const { maxPageNumber } = this.props;
		const pageNumber = +this._goToPageInput!.value;

		if (Number.isNaN(pageNumber)) {
			this.setState({ modalError: Strings.PageBarErrorInvalidNumber });
		} else if (pageNumber <= 0) {
			this.setState({ modalError: Strings.PageBarErrorNumberTooLow.replace("${number}", `${1}`) });
		} else if (maxPageNumber != null && pageNumber > maxPageNumber) {
			this.setState({ modalError: Strings.PageBarErrorNumberTooHigh.replace("${number}", `${maxPageNumber}`) });
		} else {
			this.setState({ modalError: null });
			
			return true;
		}
		return false;
	}
}