import * as classnames from "classnames";
import * as React from "react";

import * as styles from "./styles.css";

export const enum TooltipHorizontalPosition { Left, Right }
export const enum TooltipVerticalPosition { Bottom, Top }

export interface TooltipProps extends React.HTMLAttributes<HTMLElement> {
	bodyClassName?: string;
	horizontalPosition?: TooltipHorizontalPosition;
	verticalPosition?: TooltipVerticalPosition;

	onOutsideClick?(): void;
}

export class Tooltip extends React.Component<TooltipProps> {
	static defaultProps: Partial<TooltipProps> = {
		horizontalPosition: TooltipHorizontalPosition.Left,
		verticalPosition: TooltipVerticalPosition.Bottom
	};
	
	render(): JSX.Element {
		const {
			bodyClassName,
			className,
			horizontalPosition,
			verticalPosition,
			children,
			...props
		} = this.props;

		return (
			<div {...props} className={classnames(
				styles.tooltip,
				horizontalPosition === TooltipHorizontalPosition.Left
					? styles.tooltipLeft
					: styles.tooltipRight,
				verticalPosition === TooltipVerticalPosition.Bottom
					? styles.tooltipBottom
					: styles.tooltipTop,
				className
			)}>
				<div className={classnames(
					styles.tooltipBody,
					bodyClassName
					)}>
					{children}
				</div>
				<div
					className={classnames(
						styles.tooltipArrow,
						horizontalPosition === TooltipHorizontalPosition.Left
							? styles.tooltipArrowLeft
							: styles.tooltipArrowRight,
						verticalPosition === TooltipVerticalPosition.Bottom
							? styles.tooltipArrowBottom
							: styles.tooltipArrowTop,
					)}
				/>
			</div>
		);
	}

	componentDidMount(): void {
		const { onOutsideClick } = this.props;

		onOutsideClick && document.body.addEventListener("click", onOutsideClick);
	}

	componentDidUpdate(): void {
		
	}

	componentWillUnmount(): void {
		const { onOutsideClick } = this.props;

		onOutsideClick && document.body.removeEventListener("click", onOutsideClick);
	}
}