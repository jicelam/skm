import * as classnames from "classnames";
import * as React from "react";

import { ForumListItemProps } from "./ForumListItem";
import * as styles from "./styles.css";

export { ForumListItem, ForumListItemProps } from "./ForumListItem";

export interface ForumListProps {
	className?: string;
	itemClassName?: string;
	style?: React.CSSProperties;

	children: React.ReactElement<ForumListItemProps>[];
}

export class ForumList extends React.Component<ForumListProps> {
	render(): JSX.Element {
		const { className, style } = this.props;

		return (
			<ul
				className={classnames(
					styles.forumList,
					className
				)}
				style={style}
			>
				{this.renderChildren()}
			</ul>
		);
	}

	private renderChildren(): JSX.Element[] {
		const { itemClassName, children } = this.props;

		return React.Children.map(
			children,
			child => React.cloneElement(child as React.ReactElement<ForumListItemProps>, {
				className: classnames(
					itemClassName,
					(child as React.ReactElement<ForumListItemProps>).props.className
				)
			})
		);
	}
}