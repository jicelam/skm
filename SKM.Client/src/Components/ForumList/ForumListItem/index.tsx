import * as classnames from "classnames";
import * as React from "react";

import { RouteLink } from "../../RouteLink";
import * as styles from "./styles.css";

export interface ForumListItemProps {
	name: string;
	description: string;
	iconUrl?: string;

	href?: string;

	className?: string;
	style?: React.CSSProperties;
}

export class ForumListItem extends React.Component<ForumListItemProps> {
	render(): JSX.Element {
		const { name, description, href, className, style } = this.props;

		return (
			<li
				className={classnames(
					styles.forumListItem,
					className
				)}
				style={style}
			>
				<RouteLink className={styles.forumListItemLink} to={href!}>
					<div className={styles.forumListItemHeader}>
						<div className={styles.forumListItemIcon} />
						<div className={styles.forumListItemName}>
							{name}
						</div>
					</div>
					<div className={styles.forumListItemContent}>
						{description}
					</div>
				</RouteLink>
			</li>
		);
	}
}