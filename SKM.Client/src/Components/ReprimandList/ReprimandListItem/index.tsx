import * as classnames from "classnames";
import * as moment from "moment";
import * as React from "react";

import { Strings } from "Assets";

import * as styles from "./styles.css";

export interface ReprimandListItemProps {
	reason: string;
	assignedOn: Date;
	expiresOn?: Date;

	className?: string;
	style?: React.CSSProperties;
}

export class ReprimandListItem extends React.Component<ReprimandListItemProps> {
	render(): JSX.Element {
		const { reason, assignedOn, expiresOn, className, style } = this.props;

		return (
			<li
				className={classnames(
					styles.reprimandListItem,
					className
				)}
				style={style}
			>
				<div className={styles.reprimandListItemReason}>{reason}</div>
				<div className={styles.reprimandListItemFooter}>
					<div className={styles.reprimandListItemAssignedOn}>
						{`${Strings.UserViewAssignedOn}: ${moment(assignedOn).format("ll")}`}
					</div>
					{expiresOn && (
						<div className={styles.reprimandListItemExpiresOn}>
							{`${Strings.UserViewExpiresOn}: ${moment(expiresOn).format("ll")}`}
						</div>
					)}
				</div>
			</li>
		);
	}
}