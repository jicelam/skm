import autobind from "autobind-decorator";
import * as classnames from "classnames";
import * as React from "react";

import { Icon, Loading } from "Components";

import { ReprimandListItemProps } from "./ReprimandListItem";
import * as styles from "./styles.css";

export { ReprimandListItem, ReprimandListItemProps } from "./ReprimandListItem";

export interface ReprimandListProps {
	title: string;
	emptyMessage: string;
	loading?: boolean;

	className?: string;
	itemClassName?: string;
	style?: React.CSSProperties;

	children?: React.ReactElement<ReprimandListItemProps>[];
}

interface ReprimandListState {
	expanded: boolean;
}

export class ReprimandList extends React.Component<ReprimandListProps, ReprimandListState> {
	constructor(props: ReprimandListProps, context?: {}) {
		super(props, context);

		this.state = { expanded: false };
	}

	render(): JSX.Element {
		const { title, loading, className, style } = this.props;
		const { expanded } = this.state;

		return (
			<div
				className={classnames(
					className,
					styles.reprimandList
				)}
				style={style}
			>
				{loading && (
					<Loading className={styles.reprimandListLoading} />
				)}
				<header className={styles.reprimandListHeader}>
					{!loading && (
						<div className={styles.reprimandListHeaderTitle}>
							{title}
						</div>
					)}
					<Icon
						className={classnames(
							styles.reprimandListHeaderIcon,
							expanded && styles.reprimandListHeaderIconExpanded
						)}
						icon="angle-down"
						onClick={this.onExpandToggle}
					/>
				</header>
				{this.renderBody()}
			</div>
		);
	}

	private renderBody(): JSX.Element {
		const { children, emptyMessage } = this.props;
		const { expanded } = this.state;

		return (
			<section
				className={classnames(
					styles.reprimandListBody,
					expanded && styles.reprimandListBodyExpanded
				)}
			>
				{React.Children.count(children) === 0
					? <div className={styles.reprimandListEmptyMessage}>{emptyMessage}</div>
					: this.renderList()
				}
			</section>
		);
	}

	private renderList(): JSX.Element {
		const { itemClassName, children } = this.props;

		return (
			<ul className={styles.reprimandListItems}>
				{React.Children.map(
					children,
					child => React.cloneElement(child as React.ReactElement<ReprimandListItemProps>, {
						className: classnames(
							itemClassName,
							(child as React.ReactElement<ReprimandListItemProps>).props.className
						)	
					})
				)}
			</ul>
		);
	}

	@autobind
	private onExpandToggle(): void {
		this.setState({ expanded: !this.state.expanded });
	}
}