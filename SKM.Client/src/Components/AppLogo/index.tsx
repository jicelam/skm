import * as classnames from "classnames";
import * as React from "react";

import * as styles from "./styles.css";

export interface AppLogoProps {
	className?: string;
	style?: React.CSSProperties;
}

export class AppLogo extends React.Component<AppLogoProps> {
	render(): JSX.Element {
		const { className, style } = this.props;

		return (
			<div
				className={classnames(
					styles.appLogo,
					className
				)}
				style={style}
			>
				S
			</div>
		);
	}
}