import * as classnames from "classnames";
import * as React from "react";

import * as styles from "./styles.css";

export interface CardProps extends React.HTMLAttributes<HTMLElement> {}

export const Card = ({ className, children, ...props }: CardProps) => (
	<div 
		{...props}
		className={classnames(styles.card, className)}
	>
		{children}
	</div>
);

export interface CardSectionProps extends React.HTMLAttributes<HTMLElement> {}

export const CardSection = ({ className, children, ...props }: CardSectionProps) => (
	<div 
		{...props}
		className={classnames(styles.cardSection, className)}
	>
		{children}
	</div>
);

export interface CardFooterProps extends React.HTMLAttributes<HTMLElement> {}

export const CardFooter = ({ className, children, ...props }: CardFooterProps) => (
	<div 
		{...props}
		className={classnames(styles.cardFooter, className)}
	>
		{children}
	</div>
);