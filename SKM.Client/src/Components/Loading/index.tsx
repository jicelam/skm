import * as classnames from "classnames";
import * as React from "react";

import { Icon } from "../Icon";
import * as styles from "./styles.css";

export interface LoadingProps {
	className?: string;
	iconClassName?: string;

	style?: React.CSSProperties;
	iconStyle?: React.CSSProperties;
}

export const Loading = (props: LoadingProps) => (
	<div
		className={classnames(
			styles.loading,
			props.className
		)}
		style={props.style}
	>
		<LoadingIcon
			className={props.iconClassName}
			style={props.iconStyle}
		/>
	</div>
);

export interface LoadingIconProps {
	className?: string;
	style?: React.CSSProperties;
}

export const LoadingIcon = (props: LoadingProps) => (
	<Icon
		{...props}
		className={classnames(
			styles.loadingIcon,
			props.className
		)}
		icon="spinner"
		spin
	/>
);