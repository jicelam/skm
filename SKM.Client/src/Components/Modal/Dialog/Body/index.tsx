import * as classnames from "classnames";
import * as React from "react";

import * as styles from "./styles.css";

export interface ModalBodyProps {
	className?: string;
	style?: React.CSSProperties;

	children?: React.ReactNode;
}

export const ModalBody = (props: ModalBodyProps) => (
	<div
		className={classnames(
			styles.modalBody,
			props.className
		)}
		style={props.style}
	>
		{props.children}
	</div>
);