import * as classnames from "classnames";
import * as React from "react";

import * as styles from "./styles.css";

export interface ModalDialogProps {
	className?: string;
	style?: React.CSSProperties;
}

export class ModalDialog extends React.Component<ModalDialogProps> {
	render(): JSX.Element {
		const { className, style, children } = this.props;

		return (
			<div
				className={classnames(
					styles.modalDialog,
					className
				)}
				style={style}
			>
				{children}
			</div>
		);
	}
}