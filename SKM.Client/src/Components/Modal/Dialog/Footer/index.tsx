import * as classnames from "classnames";
import * as React from "react";

import * as styles from "./styles.css";

export interface ModalFooterProps {
	className?: string;
	style?: React.CSSProperties;

	children?: React.ReactNode;
}

export const ModalFooter = (props: ModalFooterProps) => (
	<footer
		className={classnames(
			styles.modalFooter,
			props.className
		)}
		style={props.style}
	>
		{props.children}
	</footer>
);