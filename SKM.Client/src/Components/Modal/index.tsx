import autobind from "autobind-decorator";
import * as classnames from "classnames";
import * as React from "react";
import * as ReactDOM from "react-dom";
import { Transition } from "react-transition-group";

import { timeout, KeyCode } from "Util";

import { ModalDialog } from "./Dialog";
import * as styles from "./styles.css";

export * from "./Dialog/Body";
export * from "./Dialog/Footer";

export interface ModalProps {
	rootElementId?: string;

	className?: string;
	style?: React.CSSProperties;

	children: React.ReactNode;

	onCloseAction?(): void;
}

interface ModalState {
	open: boolean;
}

export class Modal extends React.Component<ModalProps, ModalState> {
	static rootElementId: string = "";
	static get defaultProps(): Partial<ModalProps> {
		return { rootElementId: Modal.rootElementId };
	}

	private _rootElement: HTMLElement;

	constructor(props: ModalProps, context?: {}) {
		super(props, context);

		const { rootElementId } = this.props;
		if (!rootElementId) {
			throw new Error("No root element has been set for modals");
		}

		const rootElement = document.getElementById(rootElementId);
		if (!rootElement) {
			throw new Error(`Root element with the id "${rootElementId}" was not found`);
		}

		this._rootElement = rootElement;

		this.state = {
			open: false
		};
	}

	render(): React.ReactPortal {
		const { className, style, children } = this.props;
		const { open } = this.state;

		return ReactDOM.createPortal(
			(
				<Transition appear in={open} timeout={+styles.modalAnimationDuration}>
					{animationState => (
						<div
							className={classnames(
								styles.modal,
								styles[`modal-${animationState}`],
								className
							)}
							style={style}
						>
							<ModalDialog className={styles.modalDialog}>
								{children}
							</ModalDialog>
						</div>
					)}
				</Transition>
			),
			this._rootElement
		);
	}

	async componentDidMount(): Promise<void> {
		document.body.addEventListener("keyup", this.onKeyPress);

		await timeout(0);

		this.setState({ open: true });
	}

	componentWillUnmount(): void {
		document.body.removeEventListener("keyup", this.onKeyPress);
	}

	async startClose(): Promise<void> {
		this.setState({ open: false });

		await timeout(+styles.modalAnimationDuration);
	}

	@autobind
	private onKeyPress(event: KeyboardEvent): void {
		if (event.keyCode !== KeyCode.Escape) {
			return;
		}

		const { onCloseAction } = this.props;

		onCloseAction && onCloseAction();
	}
}