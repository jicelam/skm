import * as classnames from "classnames";
import * as React from "react";

import * as styles from "./styles.css";

export interface InputProps extends React.InputHTMLAttributes<HTMLInputElement> {}

export class Input extends React.Component<InputProps> {
	private _elem: HTMLInputElement;

	get value(): string { return this._elem.value; }

	render(): JSX.Element {
		return (
			<input
				{...this.props}
				ref={elem => this._elem = elem!}
				className={classnames(
					styles.input,
					this.props.className
				)}
			/>
		);
	}
}