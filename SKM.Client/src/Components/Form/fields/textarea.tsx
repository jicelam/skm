import * as classnames from "classnames";
import * as React from "react";

import * as styles from "./styles.css";

export interface TextAreaProps extends React.InputHTMLAttributes<HTMLTextAreaElement> {
	elem?(elem: HTMLTextAreaElement | null): void;
}

export class TextArea extends React.Component<TextAreaProps> {
	private _elem: HTMLTextAreaElement;

	get value(): string { return this._elem.value; }

	render(): JSX.Element {
		const { className, elem, ...props } = this.props;

		return (
			<textarea
				{...props}
				ref={textAreaElem => {
					this._elem = textAreaElem!;
					elem && elem(textAreaElem);
				}}
				className={classnames(
					styles.textarea,
					className
				)}
			/>
		);
	}
}