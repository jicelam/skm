import * as classnames from "classnames";
import * as React from "react";

import * as styles from "./styles.css";

export interface FormErrorProps {
	className?: string;
	style?: React.CSSProperties;

	children?: React.ReactNode;
}

export const FormError = (props: FormErrorProps) => (
	<div
		className={classnames(
			styles.formError,
			props.className
		)}
		style={props.style}
	>
		{props.children}
	</div>
);