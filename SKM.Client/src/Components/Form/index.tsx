import * as classnames from "classnames";
import * as React from "react";

import * as styles from "./styles.css";

export * from "./error";
export * from "./field";
export * from "./fields/input";
export * from "./fields/textarea";

export interface FormProps extends React.FormHTMLAttributes<HTMLFormElement> {}

export class Form extends React.Component<FormProps> {
	render(): JSX.Element {
		const { className, children } = this.props;

		return (
			<form
				{...this.props}
				className={classnames(
					styles.form,
					className
				)}
			>
				{children}
			</form>
		);
	}
}