import * as classnames from "classnames";
import * as React from "react";

import * as styles from "./styles.css";

export interface FormFieldGroupProps {
	title?: string;

	className?: string;
	style?: React.CSSProperties;
}

export class FormField extends React.Component<FormFieldGroupProps> {
	render(): JSX.Element {
		const { title, className, style, children } = this.props;

		return (
			<div
				className={classnames(
					styles.formField,
					className
				)}
				style={style}
			>
				{title && (
					<label className={styles.formFieldLabel}>
						{title}
					</label>
				)}
				<div className={styles.formFieldControl}>
					{children}
				</div>
			</div>
		);
	}
}