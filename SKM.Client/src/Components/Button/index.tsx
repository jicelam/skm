import * as classnames from "classnames";
import * as React from "react";

import * as styles from "./styles.css";

export interface ButtonProps extends React.ButtonHTMLAttributes<HTMLButtonElement> {
	elem?(elem: HTMLButtonElement | null): void;
}

export class Button extends React.Component<ButtonProps> {
	render(): JSX.Element {
		const { className, children, disabled, elem, onClick, ...props } = this.props;

		return (
			<button
				{...props}
				ref={elem}
				className={classnames(
					styles.button,
					className
				)}
				disabled={disabled}
				type="button"
				onClick={!disabled ? onClick : undefined}
			>
				{children}
				{disabled && (
					<div className={styles.buttonDisabledMask} />
				)}
			</button>
		);
	}
}

export const ActionButton = (props: ButtonProps) => (
	<Button
		{...props}
		className={classnames(
			styles.actionButton,
			props.className
		)}
	/>
);

export const ConfirmButton = (props: ButtonProps) => (
	<Button
		{...props}
		className={classnames(
			styles.confirmButton,
			props.className
		)}
	/>
);

export const CancelButton = (props: ButtonProps) => (
	<Button
		{...props}
		className={classnames(
			styles.cancelButton,
			props.className
		)}
	/>
);

export const TooltipButton = (props: ButtonProps) => (
	<Button
		{...props}
		className={classnames(
			styles.tooltipButton,
			props.className
		)}
	/>
);