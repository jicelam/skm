import * as classnames from "classnames";
import * as moment from "moment";
import * as React from "react";

import { RouteLink } from "../../RouteLink";
import * as styles from "./styles.css";

export interface ThreadListItemProps {
	name: string;
	createdBy: string;
	createdOn: Date;
	lastPostBy: string;
	lastPostOn: Date;

	href?: string;

	className?: string;
	style?: React.CSSProperties;
}

export class ThreadListItem extends React.Component<ThreadListItemProps> {
	render(): JSX.Element {
		const { name, createdBy, lastPostBy, lastPostOn, href, className, style } = this.props;

		return (
			<li
				className={classnames(
					styles.threadListItem,
					className
				)}
				style={style}
			>
				<RouteLink className={styles.threadListItemLink} to={href!}>
					<div className={styles.threadListItemContent}>
						<div className={styles.threadListItemIcon} />
						<div className={styles.threadListItemSummary}>
							<div className={styles.threadListItemName}>
								{name}
							</div>
							<div className={styles.threadListItemAuthor}>
								{createdBy}
							</div>
						</div>
						<div className={styles.threadListItemLastPost}>
							<div className={styles.threadListItemLastPostBy}>
								{lastPostBy}
							</div>
							<div className={styles.threadListItemLastPostOn}>
								{moment(lastPostOn).format("HH:mm l")}
							</div>
						</div>
					</div>
				</RouteLink>
			</li>
		);
	}
}