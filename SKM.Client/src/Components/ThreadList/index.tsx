import * as classnames from "classnames";
import * as React from "react";

import { Strings } from "Assets";

import { ThreadListItem, ThreadListItemProps } from "./ThreadListItem";
import * as styles from "./styles.css";

export { ThreadListItem, ThreadListItemProps } from "./ThreadListItem";

export interface ThreadListProps {
	className?: string;
	itemClassName?: string;
	style?: React.CSSProperties;

	children?: React.ReactElement<ThreadListItem>[];
}

export class ThreadList extends React.Component<ThreadListProps> {
	render(): JSX.Element {
		const { className, style, children } = this.props;
		if (React.Children.count(children) === 0) {
			return (
				<div className={styles.threadListEmptyMessage}>
					{Strings.ThreadViewEmpty}
				</div>
			);
		}

		return (
			<ul
				className={classnames(
					styles.threadList,
					className
				)}
				style={style}
			>
				{this.renderChildren()}
			</ul>
		);
	}

	private renderChildren(): JSX.Element[] {
		const { itemClassName, children } = this.props;

		return React.Children.map(
			children,
			child => React.cloneElement(child as React.ReactElement<ThreadListItemProps>, {
				className: classnames(
					itemClassName,
					(child as React.ReactElement<ThreadListItemProps>).props.className
				)
			})
		);
	}
}