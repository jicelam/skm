import * as classnames from "classnames";
import * as React from "react";

import * as styles from "./styles.css";

export interface PageTabProps {
	selected?: boolean;

	className?: string;

	onClick?(): void;
}

export class PageTab extends React.Component<PageTabProps> {
	render(): JSX.Element {
		const { selected, className, onClick, children } = this.props;

		return (
			<div
				className={classnames(
					styles.pageTab,
					selected && styles.pageTabSelected,
					className
				)}
				onClick={onClick}
			>
				{children}
			</div>
		);
	}
}