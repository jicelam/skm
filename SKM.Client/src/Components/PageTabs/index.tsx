import * as classnames from "classnames";
import * as React from "react";

import { PageTabProps } from "./PageTab";
import * as styles from "./styles.css";

export { PageTab } from "./PageTab";

export interface PageTabProps {
	text: string;

	onClick(): void;
}

export interface PageTabsProps {
	selectedTabIndex?: number;

	className?: string;
	tabClassName?: string;
	style?: React.CSSProperties;

	children: React.ReactElement<PageTabProps>[];
}

export class PageTabs extends React.Component<PageTabsProps> {
	static defaultProps: Partial<PageTabsProps> = {
		selectedTabIndex: 0
	};

	render(): JSX.Element {
		const { selectedTabIndex, className, tabClassName, style } = this.props;

		return (
			<div
				className={classnames(
					styles.pageTabs,
					className
				)}
				style={style}
			>
				{this.renderChildren()}
			</div>
		);
	}

	private renderChildren(): JSX.Element[] {
		const { selectedTabIndex, tabClassName, children } = this.props;

		return React.Children.map(
			children,
			(child, index) => React.cloneElement(child as React.ReactElement<PageTabProps>, {
				selected: index === selectedTabIndex,
				className: classnames(
					tabClassName,
					(child as React.ReactElement<PageTabProps>).props.className
				)
			})
		);
	}
}