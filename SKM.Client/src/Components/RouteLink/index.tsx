import * as classnames from "classnames";
import { LocationDescriptor } from "history";
import * as React from "react";
import { Link, LinkProps } from "react-router-dom";

import * as styles from "./styles.css";

export interface RouteLinkProps extends React.AnchorHTMLAttributes<HTMLAnchorElement> {
	to?: LocationDescriptor;
	replace?: boolean;
	innerRef?: (node: HTMLAnchorElement | null) => void;
}

export const RouteLink = (props: RouteLinkProps) => (
	<Link
		{...props}
		className={classnames(styles.routeLink, props.className)}
		to={props.to || ""}
	>
		{props.children}
	</Link>
);