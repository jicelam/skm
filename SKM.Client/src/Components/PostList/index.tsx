import * as classnames from "classnames";
import * as React from "react";

import { Strings } from "Assets";

import { PostListItemProps } from "./PostListItem";
import * as styles from "./styles.css";

export { PostListItem, PostListItemProps } from "./PostListItem";

export interface PostListProps {
	className?: string;
	itemClassName?: string;
	style?: React.CSSProperties;
}

export class PostList extends React.Component<PostListProps> {
	render(): JSX.Element {
		const { className, style, children } = this.props;
		if (React.Children.count(children) === 0) {
			return (
				<div className={styles.postListEmptyMessage}>
					{Strings.PostViewEmpty}
				</div>
			);
		}

		return (
			<ul
				className={classnames(
					styles.postList,
					className
				)}
				style={style}
			>
				{this.renderChildren()}
			</ul>
		);
	}

	private renderChildren(): JSX.Element | JSX.Element[] {
		const { itemClassName, children } = this.props;

		return React.Children.map(
			children,
			child => React.cloneElement(child as React.ReactElement<PostListItemProps>, {
				className: classnames(
					itemClassName,
					(child as React.ReactElement<PostListItemProps>).props.className
				)
			})
		);
	}
}