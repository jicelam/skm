import * as classnames from "classnames";
import * as moment from "moment";
import * as React from "react";

import { Strings } from "Assets";

import { Icon } from "../../Icon";
import { RouteLink } from "../../RouteLink";
import * as styles from "./styles.css";

export interface PostListItemProps {
	postedBy: string;
	postedOn?: Date;
	editedOn?: Date;

	href?: string;

	className?: string;
	style?: React.CSSProperties;
}

export class PostListItem extends React.Component<PostListItemProps> {
	render(): JSX.Element {
		const { postedBy, postedOn, editedOn, href, className, style, children } = this.props;

		return (
			<li
				className={classnames(
					styles.postListItem,
					className
				)}
				style={style}
			>
				<div className={styles.postListItemInfo}>
					<div className={styles.postListItemIcon} />
					<div className={styles.postListItemAuthor}>
						<RouteLink to={href}>
							<span
								className={styles.postListItemAuthorName}
								title={Strings.PostViewPostedBy}
							>
								{postedBy}
							</span>
						</RouteLink>
					</div>
					<div className={styles.postListItemDates}>
						{postedOn && (
							<div className={styles.postListItemPostedOn} title={Strings.PostViewPostedOn}>
								<Icon className={styles.postListItemPostedOnIcon} icon="envelope" />
								{moment(postedOn).format("HH:mm ll")}
							</div>
						)}
						{editedOn && (
							<div className={styles.postListItemEditedOn} title={Strings.PostViewEditedOn}>
								<Icon className={styles.postListItemEditedOnIcon} icon="edit" />
								{moment(editedOn).format("HH:mm ll")}
							</div>
						)}
					</div>
				</div>
				<div className={styles.postListItemContent}>
					{children}
				</div>
			</li>
		);
	}
}