import { action, computed, observable, runInAction } from "mobx";

import { PageViewModel } from "App";
import { Strings } from "Assets";
import { Forum, Notice } from "Data";
import serviceLocator, {
	DataService,
	SecurityService,
	ServiceName
} from "Services";

export const enum HomeViewTab {
	Dashboard = 0,
	Find = 1
}

export module HomeViewTabs {
	export function getHash(tab: HomeViewTab): string {
		switch (tab) {
			case HomeViewTab.Find:
				return "find";
			case HomeViewTab.Dashboard:
				return "dashboard";
			default:
				throw new Error(`Unrecognized home tab value "${tab}"`);
		}
	}
}

export interface HomeViewModelConfig {
	selectedTab: HomeViewTab;
}

export class HomeViewModel implements PageViewModel {
	static get defaultConfig(): HomeViewModelConfig {
		return {
			selectedTab: HomeViewTab.Dashboard
		};
	}
	
	@observable
	selectedTab: HomeViewTab;
	
	@observable
	favoriteForums: Forum[] | null;

	@observable
	forums: Forum[] | null;

	@observable
	forumsTotal: number | null;

	@observable
	notices: Notice[] | null;

	@computed
	get title(): string | null {
		return Strings.HomeViewTitle;
	}

	private _dataService: DataService;
	private _securityService: SecurityService;

	constructor(config: HomeViewModelConfig = HomeViewModel.defaultConfig) {
		this.selectedTab = config.selectedTab;

		this.favoriteForums = null;
		this.forums = null;
		this.forumsTotal = null;
		this.notices = null;

		this._dataService = serviceLocator.get(ServiceName.Data);
		this._securityService = serviceLocator.get(ServiceName.Security);
	}

	@action.bound
	async fetchFavoriteForums(): Promise<void> {
		const currentUser = this._securityService.currentUser;
		if (!currentUser || this.favoriteForums) {
			return;
		}
		
		const forums = await this._dataService.getFavoriteForums();

		runInAction(() => {
			this.favoriteForums = forums;
		});
	}

	@action.bound
	async fetchForums(pageNumber?: number): Promise<void> {
		this.forums = null;

		const forums = await this._dataService.getForums(pageNumber);

		runInAction(() => {
			this.forums = forums.items;
			this.forumsTotal = forums.totalPages;
		});
	}

	@action.bound
	async fetchNotices(): Promise<void> {
		if (this.notices) {
			return;
		}

		const notices = await this._dataService.getNotices();

		runInAction(() => {
			this.notices = notices;
		});
	}

	@action.bound
	setSelectedTab(tab: HomeViewTab): void {
		this.selectedTab = tab;
	}
}