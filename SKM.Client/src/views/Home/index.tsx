import { inject, observer } from "mobx-react";
import * as React from "react";
import { RouteComponentProps } from "react-router";

import { AppContext, AppViewModel } from "App";
import { Strings } from "Assets";
import { PageTabs, PageTab, RouteLink } from "Components";

import { InjectedComponent } from "../base";
import { Dashboard } from "./Dashboard";
import { ForumFinder } from "./ForumFinder";
import { HomeViewModel, HomeViewModelConfig, HomeViewTab, HomeViewTabs } from "./model";
import * as styles from "./styles.css";

export interface HomeViewProps extends RouteComponentProps<{}> {
	config?: HomeViewModelConfig;
}

interface HomeViewInjected {
	app: AppViewModel;
}

@inject((context: AppContext) => ({
	app: context.model.view
}) as HomeViewInjected)
@observer
export class HomeView extends InjectedComponent<HomeViewProps, HomeViewInjected> {
	private _model: HomeViewModel;

	constructor(props: HomeViewProps, context?: {}) {
		super(props, context);

		const { app, config } = this.injected;

		this._model = new HomeViewModel(config);
		app.setCurrentView(this._model);

		this.updateModelFromHash();
	}

	render(): JSX.Element {
		return (
			<div className={styles.homeView}>
				<PageTabs
					tabClassName={styles.homeViewPageTab}
					selectedTabIndex={this._model.selectedTab}
				>
					<PageTab key={HomeViewTab.Dashboard}>
						<RouteLink to={{ hash: HomeViewTabs.getHash(HomeViewTab.Dashboard) }}>
							{Strings.HomeViewPageTabDashboard}
						</RouteLink>
					</PageTab>
					<PageTab key={HomeViewTab.Find}>
						<RouteLink to={{ hash: HomeViewTabs.getHash(HomeViewTab.Find) }}>
							{Strings.HomeViewPageTabFind}
						</RouteLink>
					</PageTab>
				</PageTabs>
				<div className={styles.homeViewBody}>
					{this.renderBody()}
				</div>
			</div>
		);
	}

	componentDidUpdate(): void {
		this.updateModelFromHash();
	}

	private updateModelFromHash(): void {
		const { location } = this.props;
		const targetTab = this.mapHashToTab(location.hash);
		if (targetTab !== this._model.selectedTab) {
			this._model.setSelectedTab(targetTab);
		}
	}

	private mapHashToTab(hash: string): HomeViewTab {
		hash = hash.toLowerCase();
		if (hash.match(/[#&]find&?/)) {
			return HomeViewTab.Find;
		}
		return HomeViewTab.Dashboard;
	}

	private renderBody(): JSX.Element {
		const { selectedTab } = this._model;

		switch (selectedTab) {
			case HomeViewTab.Dashboard:
				return (
					<Dashboard />
				);
			case HomeViewTab.Find:
				return (
					<ForumFinder {...this.props} />
				);
			default:
				throw new Error(`Invalid home page state "${selectedTab}"`);
		}
	}
}