import { inject, observer } from "mobx-react";
import * as moment from "moment";
import * as React from "react";

import { AppContext, AppViewModel } from "App";
import { Strings } from "Assets";
import {
	ConfirmButton,
	ForumList,
	ForumListItem,
	Loading,
	RouteLink
} from "Components";

import { InjectedComponent } from "../../base";
import { HomeViewModel, HomeViewTab, HomeViewTabs } from "../model";
import * as styles from "./styles.css";

export interface DashboardProps {}

interface DashboardInjected {
	app: AppViewModel;
	model: HomeViewModel;
}

@inject((context: AppContext) => ({
	app: context.model.view,
	model: context.model.view.current
} as DashboardInjected))
@observer
export class Dashboard extends InjectedComponent<DashboardProps, DashboardInjected> {
	render(): JSX.Element {
		const { app, model } = this.injected;

		return (
			<div className={styles.dashboard}>
				{!model.favoriteForums && !model.notices
					? this.renderLoading()
					: <>
						{app.currentUser && (
							this.renderFavoritesColumn()
						)}
						{this.renderContentColumn()}
					</>
				}
			</div>
		);
	}

	async componentDidMount(): Promise<void> {
		document.title = Strings.HomeViewDashboardRouteTitle;

		const { model } = this.injected;

		model.fetchFavoriteForums();
		model.fetchNotices();
	}

	private renderFavoritesColumn(): JSX.Element {
		const { model } = this.injected;

		return (
			<div className={styles.dashboardFavoritesColumn}>
				{!model.favoriteForums
					? this.renderLoading()
					: model.favoriteForums.length === 0
						? this.renderEmpty()
						: this.renderForums()
				}
			</div>
		);
	}

	private renderLoading(): JSX.Element {
		return (
			<Loading iconClassName={styles.dashboardLoading} />
		);
	}

	private renderEmpty(): JSX.Element {
		return (
			<div className={styles.dashboardFavoritesEmpty}>
				<p>{Strings.HomeViewDashboardEmptyMessage}</p>

				<div className={styles.dashboardFavoritesEmptyButtonRow}>
					<ConfirmButton className={styles.dashboardFavoritesEmptyButton}>
						<RouteLink to={{ hash: HomeViewTabs.getHash(HomeViewTab.Find) }}>
							{Strings.HomeViewDashboardEmptyButton}
						</RouteLink>
					</ConfirmButton>
				</div>
			</div>
		);
	}

	private renderForums(): JSX.Element {
		const { model } = this.injected;

		return (
			<section className={styles.dashboardFavorites}>
				<h4 className={styles.dashboardFavoritesHeader}>
					{Strings.HomeViewDashboardFavoritesTitle}
				</h4>
				<ForumList
					className={styles.dashboardFavoritesForumList}
					itemClassName={styles.dashboardFavoritesForumListItem}
				>
					{model.favoriteForums!.map(forum => (
						<ForumListItem
							key={forum.id}
							name={forum.name}
							description={forum.description}
							href={`/forums/${forum.id}`}
						/>
					))}
				</ForumList>
			</section>
		);
	}

	private renderContentColumn(): JSX.Element {
		const { model } = this.injected;

		return (
			<div className={styles.dashboardContentColumn}>
				<h3 className={styles.dashboardContentGreeting}>
					Welcome to SKM!
				</h3>
				{!model.notices
					? this.renderLoading()
					: <ul className={styles.dashboardNoticesList}>
						{model.notices.map(notice => (
							<li
								key={notice.id}
								className={styles.dashboardNoticesListItem}
							>
								<header className={styles.dashboardNoticesListItemHeader}>
									<h5
										className={styles.dashboardNoticesListItemHeaderText}
										title={notice.name}
									>
										{notice.name}
									</h5>
									<div className={styles.dashboardNoticesListItemHeaderDate}>
										{moment(notice.publishedOn).format("l")}
									</div>
								</header>
								<section
									className={styles.dashboardNoticesListItemBody}
									dangerouslySetInnerHTML={{ __html: notice.summary }}
								/>
							</li>
						))}
					</ul>
				}
			</div>
		);
	}
}