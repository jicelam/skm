import autobind from "autobind-decorator";
import { inject, observer } from "mobx-react";
import * as qs from "query-string";
import * as React from "react";
import { RouteComponentProps } from "react-router";

import { AppContext, AppViewModel } from "App";
import { Strings } from "Assets";
import { ForumList, ForumListItem, Loading, PageBar } from "Components";

import { InjectedComponent } from "../../base";
import { HomeViewModel } from "../model";
import * as styles from "./styles.css";

export interface ForumFinderProps extends RouteComponentProps<{}> {}

interface ForumFinderInjected {
	app: AppViewModel;
	model: HomeViewModel;
}

@inject((context: AppContext) => ({
	app: context.model.view,
	model: context.model.view.current
}) as ForumFinderInjected)
@observer
export class ForumFinder extends InjectedComponent<ForumFinderProps, ForumFinderInjected> {
	private _disposeObservers: (() => void)[] = [];

	private get _pageNumber(): number {
		const { location } = this.props;

		const queryParams = qs.parse(location.search);
		const pageNumber = queryParams.page;

		return pageNumber != null
			? +pageNumber
			: 1;
	}
	
	render(): JSX.Element {
		const { model } = this.injected;

		return (
			<div className={styles.forumFinder}>
				{!model.forums
					? this.renderLoading()
					: model.forums.length === 0
						? this.renderEmpty()
						: this.renderForums()
				}
			</div>
		);
	}

	componentDidMount(): void {
		document.title = Strings.HomeViewForumFinderRouteTitle;

		const { history, model } = this.injected;

		this._disposeObservers.push(history.listen(location => {
			const queryParams = qs.parse(location.search);

			model.fetchForums(queryParams.page
				? +queryParams.page
				: 1
			);
		}));
		
		model.fetchForums(this._pageNumber);
	}

	componentWillUnmount(): void {
		for (let dispose of this._disposeObservers) { dispose(); }
	}

	private renderLoading(): JSX.Element {
		return (
			<Loading iconClassName={styles.forumFinderLoading} />
		);
	}

	private renderEmpty(): JSX.Element {
		return (
			<div className={styles.forumFinderEmpty}>
				<p>{Strings.HomeViewForumFinderEmptyMessage}</p>
			</div>
		);
	}

	private renderForums(): JSX.Element {
		const { model } = this.injected;

		return (
			<>
				<div className={styles.forumFinderListSection}>
					<ForumList>
						{model.forums!.map(forum => (
							<ForumListItem
								key={forum.id}
								name={forum.name}
								description={forum.description}
								href={`/forums/${forum.id}`}
							/>
						))}
					</ForumList>
				</div>
				<footer className={styles.forumFinderFooter}>
					<PageBar
						selectedPageNumber={this._pageNumber}
						maxPageNumber={model.forumsTotal!}
						onPageSelect={this.onPageChange}
					/>
				</footer>
			</>
		);
	}

	@autobind
	private onPageChange(newPageNumber: number): void {
		const { history, location } = this.props;

		history.push({
			hash: location.hash,
			search: `page=${newPageNumber}`
		});
	}
}