import { action, computed, observable, runInAction } from "mobx";

import { PageViewModel } from "App";
import { Strings } from "Assets";
import { Post, Thread } from "Data";
import serviceLocator, { ServiceName, DataService } from "Services";

export interface NewPostViewModelConfig {
	threadId: string;
	
	content?: string;
}

export const enum NewPostSubmissionResult { Successful, Failed }

export class NewPostViewModel implements PageViewModel {
	@observable
	thread: Thread | null = null;

	@observable
	posts: Post[] | null = null;

	@observable
	content: string | null = null;

	@observable
	error: string | null = null;

	@observable
	contentError: string | null = null;

	@observable
	loading = true;

	@computed
	get title(): string | null {
		return this.thread
			? Strings.NewPostFormTitle(this.thread.name)
			: null;
	}

	@computed
	get validForm(): boolean {
		return !!this.content;
	}

	private _dataService: DataService;

	constructor(config: NewPostViewModelConfig) {
		this.content = config.content || null;

		this._dataService = serviceLocator.get(ServiceName.Data);

		this.fetchData(config.threadId);
	}

	@action.bound
	setContent(value: string): void {
		this.content = value;
		this.contentError = null;
	}

	@action.bound
	async submit(): Promise<NewPostSubmissionResult> {
		if (!this.thread || !this.validatePost()) {
			return NewPostSubmissionResult.Failed;
		}

		try {
			this.loading = true;

			await this._dataService.createPost({
				threadId: this.thread.id,
				content: this.content!
			});

			const updatedThread = await this._dataService.getThread(this.thread.id);

			runInAction(() => {
				this.thread = updatedThread;
				this.loading = false;
			});

			return NewPostSubmissionResult.Successful;
		} catch (error) {
			runInAction(() => {
				this.error = error.message;
			});

			return NewPostSubmissionResult.Failed;
		}
	}

	@action
	private async fetchData(threadId: string): Promise<void> {
		const [thread, posts] = await Promise.all([
			this._dataService.getThread(threadId),
			this._dataService.getPosts(threadId, "last")
		]);

		runInAction(() => {
			this.thread = thread;
			this.posts = posts.length === this._dataService.threadPageSize
				? []
				: posts;
			this.loading = false;
		});
	}

	@action
	private validatePost(): boolean {
		let postValid = true;
		this.error = null;
		this.contentError = null;

		if (!this.content) {
			this.contentError = Strings.NewPostNoContentError;
			postValid = false;
		}

		return postValid;
	}
}