import autobind from "autobind-decorator";
import * as classnames from "classnames";
import { observe, IValueDidChange as ValueDidChange } from "mobx";
import { inject, observer } from "mobx-react";
import * as React from "react";
import { RouteComponentProps } from "react-router";

import { AppContext, AppViewModel } from "App";
import { Strings } from "Assets";
import {
	ActionButton,
	Card,
	CardFooter,
	CardSection,
	Form,
	Loading,
	PostList,
	PostListItem,
	TextArea
} from "Components";
import { Post, Thread } from "Data";

import { InjectedComponent } from "../../base";
import { NewPostViewModel, NewPostSubmissionResult } from "./model";
import * as styles from "./styles.css";

export interface NewPostViewProps extends RouteComponentProps<{ forumId: string; threadId: string; }> {}

interface NewPostViewInjected {
	app: AppViewModel;
}

@inject((context: AppContext) => ({
	app: context.model.view
}) as NewPostViewInjected)
@observer
export class NewPostView extends InjectedComponent<NewPostViewProps, NewPostViewInjected> {
	private _model: NewPostViewModel;
	
	private _inputElem: HTMLTextAreaElement;
	private _buttonElem: HTMLButtonElement;

	private _disposeObservers: (() => void)[] = [];

	constructor(props: NewPostViewProps, context?: {}) {
		super(props, context);

		const { app } = this.injected;
		const { threadId } = props.match.params;

		this._model = new NewPostViewModel({ threadId });
		app.setCurrentView(this._model);

		this._disposeObservers.push(observe(this._model, "thread", change => {
			this._model.thread && this.setRouteTitle();
		}));
	}

	render(): JSX.Element {
		const { app } = this.injected;

		return (
			<div className={styles.newPostView}>
				{!app.currentUser
					? <div className={styles.newPostMessageContainer}>
						<Card className={styles.newPostLoginRequired}>
							{Strings.NewPostLoginRequired}
						</Card>
					</div>
					: !(this._model.thread && this._model.posts)
						? <Loading />
						: <>
							{this._model.posts.length > 0 && (
								<PostList className={styles.newPostList}>
									{this._model.posts.map(post => (
										<PostListItem
											key={post.id}
											className={styles.newPostListItem}
											postedBy={post.postedBy.name}
											postedOn={post.postedOn}
											editedOn={post.editedOn!}
											href={`/users/${post.postedBy.id}`}
										>
											{post.content}
										</PostListItem>
									))}
								</PostList>
							)}
							<Card className={styles.newPostFormContainer}>
								<Form className={styles.newPostForm}>
									<CardSection className={styles.newPostFormGroup}>
										<PostListItem
											className={styles.newPostListItem}
											postedBy={app.currentUser.name}
										>
											<TextArea
												elem={elem => this._inputElem = elem!}
												className={classnames(
													styles.newPostFormInput,
													this._model.contentError && styles.newPostFormErrorField
												)}
												placeholder={Strings.NewPostContentPlaceholder}
												onChange={this.onContentChange}
											/>
											{this._model.contentError && (
												<div className={styles.newPostFormErrorMessage}>
													{this._model.contentError}
												</div>
											)}
										</PostListItem>
									</CardSection>
									<CardFooter className={styles.newPostFormFooter}>
										<ActionButton
											elem={elem => this._buttonElem = elem!}
											className={styles.newPostButton}
											disabled={this._model.loading || !this._model.validForm}
											onClick={this.onSubmit}
										>
											{this._model.loading && (
												<Loading className={styles.newPostButtonLoading} />
											)}
											<span className={styles.newPostButtonText}>
												{Strings.NewPostCreateButton}
											</span>
										</ActionButton>
									</CardFooter>
								</Form>
							</Card>
						</>
				}
			</div>
		);
	}

	componentDidMount(): void {
		this.setRouteTitle();

		if (!this.injected.app.currentUser) {
			return;
		}

		this._disposeObservers.push(observe(this._model, "posts", this.modelPropertyChanged));
		this._disposeObservers.push(observe(this._model, "thread", this.modelPropertyChanged));
	}

	componentWillUnmount(): void {
		for (let disposeObserver of this._disposeObservers) { disposeObserver(); }
	}

	private setRouteTitle(): void {
		if (!this._model.thread) {
			return;
		}
		
		document.title = Strings.ThreadViewNewPostRouteTitle(this._model.thread.name);
	}

	@autobind
	private onContentChange(event: React.ChangeEvent<HTMLTextAreaElement>): void {
		this._model.setContent(event.target.value);
	}

	@autobind
	private async onSubmit(): Promise<void> {
		const result = await this._model.submit();
		if (result === NewPostSubmissionResult.Failed) {
			return;
		}

		const { match, history } = this.injected;

		history.push(`/threads/${match.params.threadId}?page=${this._model.thread!.pages}`);
	}

	@autobind
	private modelPropertyChanged(change: ValueDidChange<Thread | Post[] | null>): void {
		if (!change.oldValue && this._model.thread && this._model.posts) {
			setTimeout(() => {
				this._inputElem.focus();
				this._buttonElem.scrollIntoView({ behavior: "auto" });
			});
		}
	}
}