import autobind from "autobind-decorator";
import { observe } from "mobx";
import { inject, observer } from "mobx-react";
import * as React from "react";
import { RouteComponentProps } from "react-router";

import { AppContext, AppViewModel } from "App";
import { Strings } from "Assets";
import { Loading, ReprimandList, ReprimandListItem, UserProfile } from "Components";

import { InjectedComponent } from "../base";
import { UserViewModel, UserViewModelConfig } from "./model";
import * as styles from "./styles.css";

export interface UserViewProps extends RouteComponentProps<{ id: string; }> {
	config?: UserViewModelConfig;
}

interface UserViewInjected {
	app: AppViewModel;
}

@inject((context: AppContext) => ({
	app: context.model.view
}) as UserViewInjected)
@observer
export class UserView extends InjectedComponent<UserViewProps, UserViewInjected> {
	private _model: UserViewModel;

	private _disposeObservers: (() => void)[] = [];

	constructor(props: UserViewProps, context?: {}) {
		super(props, context);

		const { app, config } = this.injected;

		this._model = new UserViewModel(config);
		app.setCurrentView(this._model);

		this._disposeObservers.push(observe(this._model, "user", change => {
			this._model.user && this.setRouteTitle();
		}));
	}
	
	render(): JSX.Element {
		return (
			<div className={styles.userView}>
				{this._model.error
					? this.renderError()
					: !this._model.user
						? this.renderLoading()
						: this.renderBody()
				}
			</div>
		);
	}

	componentDidMount(): void {
		const { match } = this.props;

		this._model.fetchData(match.params.id);
	}

	componentWillUnmount(): void {
		for (let dispose of this._disposeObservers) { dispose(); }
	}

	private setRouteTitle(): void {
		if (!this._model.user) {
			return;
		}
		
		document.title = Strings.UserViewRouteTitle(this._model.user.name);
	}

	private renderError(): JSX.Element {
		return (
			<div className={styles.userViewError}>
				{this._model.error}
			</div>
		);
	}

	private renderLoading(): JSX.Element {
		return (
			<Loading />
		);
	}

	private renderBody(): JSX.Element {
		const user = this._model.user!;
		const { bans, probations } = this._model;

		return (
			<>
				<UserProfile
					className={styles.userViewProfileSection}
					name={user.name}
					avatarUrl={``}
					caption={user.caption}
					joinedOn={user.joinedOn}
					location={user.location}
					signature={user.signature}
					onSendPMClick={this.onSendPMClick}
				/>
				<ReprimandList
					className={styles.userViewProbationSection}
					title={Strings.UserViewProbationsTitle}
					emptyMessage={Strings.UserViewProbationsEmptyMessage}
					loading={!probations}
				>
					{(probations || []).map(probation => (
						<ReprimandListItem
							key={probation.id}
							reason={probation.reason}
							assignedOn={probation.assignedOn}
							expiresOn={probation.expiresOn || undefined}
						/>
					))}
				</ReprimandList>
				<ReprimandList
					className={styles.userViewBanSection}
					title={Strings.UserViewBansTitle}
					emptyMessage={Strings.UserViewBansEmptyMessage}
					loading={!bans}
				>
					{(bans || []).map(ban => (
						<ReprimandListItem
							key={ban.id}
							reason={ban.reason}
							assignedOn={ban.assignedOn}
						/>
					))}
				</ReprimandList>
			</>
		);
	}
	
	@autobind
	private onSendPMClick(): void {}
}