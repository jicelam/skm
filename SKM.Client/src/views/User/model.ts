import { action, computed, observable, runInAction } from "mobx";

import { PageViewModel } from "App";
import { Reprimand, ReprimandType, User } from "Data";
import serviceLocator, { ServiceName, DataService, SecurityService } from "Services";

export interface UserViewModelConfig {}

export class UserViewModel implements PageViewModel, UserViewModelConfig {
	static get defaultConfig(): UserViewModelConfig {
		return {};
	}

	@observable
	user: User | null;

	@observable
	bans: Reprimand[] | null;

	@observable
	probations: Reprimand[] | null;

	@observable
	error: string | null;

	@computed
	get title(): string | null {
		return this.user
			? this.user.name
			: null;
	}

	private _dataService: DataService;
	private _securityService: SecurityService;

	constructor(config: UserViewModelConfig = UserViewModel.defaultConfig) {
		this.user = null;
		this.bans = null;
		this.probations = null;
		this.error = null;

		this._dataService = serviceLocator.get(ServiceName.Data);
		this._securityService = serviceLocator.get(ServiceName.Security);
	}

	@action.bound
	async fetchData(userId: string): Promise<void> {
		try {
			if (userId === "me") {
				if (!this._securityService.currentUser) {
					throw new Error("There is no currently logged in user");
				}

				userId = this._securityService.currentUser.id;
			}

			await Promise.all([
				this.fetchUser(userId),
				this.fetchReprimands(userId)
			]);
		} catch (error) {
			this.error = error.message;
		}
	}

	@action
	private async fetchUser(id: string): Promise<void> {
		const user = await this._dataService.getUser(id);

		runInAction(() => {
			this.user = user;
		});
	}

	@action
	private async fetchReprimands(userId: string): Promise<void> {
		const reprimands = await this._dataService.getReprimands(userId);

		runInAction(() => {
			this.bans = reprimands.filter(reprimand => reprimand.type === ReprimandType.Ban);
			this.probations = reprimands.filter(reprimand => reprimand.type === ReprimandType.Probation);
		});
	}
}