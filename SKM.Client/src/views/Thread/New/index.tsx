import autobind from "autobind-decorator";
import * as classnames from "classnames";
import { observe } from "mobx";
import { inject, observer } from "mobx-react";
import * as React from "react";
import { RouteComponentProps } from "react-router";

import { AppContext, AppViewModel } from "App";
import { Strings } from "Assets";
import {
	ActionButton,
	Card,
	CardFooter,
	CardSection,
	Form,
	Input,
	Loading,
	PostListItem,
	TextArea,
} from "Components";

import { InjectedComponent } from "../../base";
import { NewThreadViewModel, NewThreadSubmissionResult } from "./model";
import * as styles from "./styles.css";

export interface NewThreadViewProps extends RouteComponentProps<{ forumId: string; }> {}

interface NewThreadViewInjected {
	app: AppViewModel;
}

@inject((context: AppContext) => ({
	app: context.model.view
}) as NewThreadViewInjected)
@observer
export class NewThreadView extends InjectedComponent<NewThreadViewProps, NewThreadViewInjected> {
	private _model: NewThreadViewModel;

	private _disposeObservers: (() => void)[] = [];

	constructor(props: NewThreadViewProps, context?: {}) {
		super(props, context);

		const { app } = this.injected;
		const { forumId } = props.match.params;

		this._model = new NewThreadViewModel({ forumId });
		app.setCurrentView(this._model);

		this._disposeObservers.push(observe(this._model, "forum", change => {
			this._model.forum && this.setRouteTitle();
		}));
	}

	render(): JSX.Element {
		const { app } = this.injected;

		return (
			<div className={styles.newThreadView}>
				{!app.currentUser
					? <Card className={styles.newThreadLoginRequired}>
						{Strings.NewThreadLoginRequired}
					</Card>
					: !this._model.forum
						? <Loading />
						: <Card className={styles.newThreadFormContainer}>
							<Form className={styles.newThreadForm}>
								<CardSection className={styles.newThreadFormGroup}>
									<label className={styles.newThreadFormLabel}>
										{Strings.NewThreadNameLabel}
									</label>
									<Input
										className={classnames(
											styles.newThreadFormInput,
											this._model.threadNameError && styles.newThreadFormErrorField
										)}
										placeholder={this._model.threadNameError || Strings.NewThreadNamePlaceholder}
										onChange={this.onThreadNameChange}
									/>
								</CardSection>
								<CardSection className={styles.newThreadPost}>
									<PostListItem
										className={styles.newThreadPostItem}
										postedBy={app.currentUser.name}
									>
										<TextArea
											className={classnames(
												styles.newThreadFormTextarea,
												this._model.postContentError && styles.newThreadFormErrorField
											)}
											placeholder={this._model.postContentError || Strings.NewThreadPostPlaceholder}
											onChange={this.onPostContentChange}
										/>
									</PostListItem>
								</CardSection>
								<CardFooter className={styles.newThreadFormFooter}>
									{this._model.error && (
										<div className={styles.newThreadErrorMessage}>
											{this._model.error}
										</div>
									)}
									<ActionButton
										className={styles.newThreadCreateButton}
										disabled={this._model.loading || !this._model.validForm}
										onClick={this.onSubmit}
									>
										{this._model.loading && (
											<Loading className={styles.newThreadCreateButtonLoading} />
										)}
										<span className={styles.newThreadCreateButtonText}>
											{Strings.NewThreadCreateButton}
										</span>
									</ActionButton>
								</CardFooter>
							</Form>
						</Card>
				}
			</div>
		);
	}

	componentDidMount(): void {
		this.setRouteTitle();
	}

	componentWillUnmount(): void {
		for (let dispose of this._disposeObservers) { dispose(); }
	}

	private setRouteTitle(): void {
		if (!this._model.forum) {
			return;
		}
		
		document.title = Strings.ForumViewNewThreadRouteTitle(this._model.forum.name);
	}

	@autobind
	private onThreadNameChange(event: React.ChangeEvent<HTMLInputElement>): void {
		this._model.setThreadName(event.target.value);
	}

	@autobind
	private onPostContentChange(event: React.ChangeEvent<HTMLTextAreaElement>): void {
		this._model.setPostContent(event.target.value);
	}

	@autobind
	private async onSubmit(): Promise<void> {
		const result = await this._model.submit();
		if (result === NewThreadSubmissionResult.Failed) {
			return;
		}

		const { history } = this.injected;

		history.push(`/forums/${this._model.forum!.id}`);
	}
}