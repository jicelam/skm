import { action, computed, observable, runInAction } from "mobx";

import { PageViewModel } from "App";
import { Strings } from "Assets";
import { Forum } from "Data";
import serviceLocator, { ServiceName, DataService } from "Services";

export interface NewThreadViewModelConfig {
	forumId: string;

	threadName?: string;
	postContent?: string; 
}

export class NewThreadViewModel implements PageViewModel {
	@observable
	forum: Forum | null = null;

	@observable
	threadName: string | null = null;

	@observable
	postContent: string | null = null;

	@observable
	error: string | null = null;

	@observable
	threadNameError: string | null = null;

	@observable
	postContentError: string | null = null;

	@observable
	loading = true;

	@computed
	get title(): string | null {
		return !this.forum
			? null
			: this.threadName 
				? Strings.NewThreadFormTitle(this.threadName, this.forum.name)
				: Strings.NewThreadFormTitleThreadNameEmpty(this.forum.name);
	}

	@computed
	get validForm(): boolean {
		return !!this.threadName && !!this.postContent;
	}

	private _dataService: DataService;

	constructor(config: NewThreadViewModelConfig) {
		this.threadName = config.threadName || null;
		this.postContent = config.postContent || null;

		this._dataService = serviceLocator.get(ServiceName.Data);

		this.fetchThread(config.forumId);
	}

	@action.bound
	setThreadName(value: string): void {
		this.threadName = value;
		this.threadNameError = null;
	}

	@action.bound
	setPostContent(value: string): void {
		this.postContent = value;
		this.postContentError = null;
	}

	@action.bound
	async submit(): Promise<NewThreadSubmissionResult> {
		if (!this.forum || !this.validateThread()) {
			return NewThreadSubmissionResult.Failed;
		}

		try {
			this.loading = true;

			await this._dataService.createThread({ 
				forumId: this.forum.id,
				iconId: "",
				name: this.threadName!,
				postContent: this.postContent!
			});

			return NewThreadSubmissionResult.Successful;
		} catch (error) {
			runInAction(() => {
				this.error = error.message;
			});

			return NewThreadSubmissionResult.Failed;
		} finally {
			runInAction(() => {
				this.loading = false;
			});
		}
	}

	@action
	private async fetchThread(id: string): Promise<void> {
		this.loading = true;

		const forum = await this._dataService.getForum(id);

		runInAction(() => {
			this.forum = forum;
			this.loading = false;
		});
	}

	@action
	private validateThread(): boolean {
		let threadValid = true;
		this.error = null;
		this.threadNameError = null;
		this.postContentError = null;
		
		if (!this.threadName) {
			this.threadNameError = Strings.NewThreadNoNameError;
			threadValid = false;
		}

		if (!this.postContent) {
			this.postContentError = Strings.NewThreadNoPostContentError;			
			threadValid = false;
		}

		return threadValid;
	}
}

export const enum NewThreadSubmissionResult { Successful, Failed }