import autobind from "autobind-decorator";
import { observe } from "mobx";
import { inject, observer } from "mobx-react";
import * as qs from "query-string";
import * as React from "react";
import { RouteComponentProps } from "react-router";

import { AppContext, AppViewModel } from "App";
import { Strings } from "Assets";
import {
	ActionButton,
	Loading,
	PageBar,
	PostList,
	PostListItem,
	RouteLink
} from "Components";

import { InjectedComponent } from "../base";
import { ThreadViewModel, ThreadViewModelConfig } from "./model";
import * as styles from "./styles.css";

export interface ThreadViewProps extends RouteComponentProps<{ forumId: string; threadId: string; }> {
	config?: ThreadViewModelConfig;
}

interface ThreadViewInjected {
	app: AppViewModel;
}

@inject((context: AppContext) => ({
	app: context.model.view
}) as ThreadViewInjected)
@observer
export class ThreadView extends InjectedComponent<ThreadViewProps, ThreadViewInjected> {
	private _model: ThreadViewModel;

	private _disposeObservers: (() => void)[] = [];

	private get _pageNumber(): number | "last" {
		const { location } = this.props;

		const queryParams = qs.parse(location.search);
		const pageNumber = queryParams.page;

		return pageNumber === "last"
			? pageNumber
			: pageNumber != null
				? +pageNumber
				: 1;
	}

	constructor(props: ThreadViewProps, context?: {}) {
		super(props, context);

		const { app, config } = this.injected;

		this._model = new ThreadViewModel(config);
		app.setCurrentView(this._model);

		this._disposeObservers.push(observe(this._model, "thread", change => {
			this._model.thread && this.setRouteTitle();
		}));
	}

	render(): JSX.Element {
		return (
			<div className={styles.threadView}>
				{this._model.error
					? this.renderError()
					: !(this._model.thread && this._model.posts)
						? this.renderLoading()
						: this.renderBody()
				}
			</div>
		);
	}

	componentDidMount(): void {
		this.setRouteTitle();

		const { history, match } = this.injected;
		const { threadId } = match.params;
		
		this._disposeObservers.push(history.listen(location => {
			const queryParams = qs.parse(location.search);

			this._model.clearPosts();
			this._model.fetchPosts(threadId, !queryParams.page
				? 1
				: queryParams.page === "last"
					? queryParams.page 
					: +queryParams.page
			);
		}));

		this._model.fetchThread(threadId);
		this._model.fetchPosts(threadId, this._pageNumber);
	}

	componentDidUpdate(): void {
		this.setRouteTitle();
	}

	componentWillUnmount(): void {
		for (let dispose of this._disposeObservers) { dispose(); }
	}

	private setRouteTitle(): void {
		if (!this._model.thread) {
			return;
		}

		const pageNumber = this._pageNumber;

		document.title = Strings.ForumViewRouteTitle(
			this._model.thread.name, 
			typeof pageNumber === "number"
				? pageNumber
				: undefined
		);
	}

	private renderError(): JSX.Element {
		return (
			<div className={styles.threadViewError}>
				{this._model.error}
			</div>
		);
	}

	private renderLoading(): JSX.Element {
		return (
			<Loading />
		);
	}

	private renderBody(): JSX.Element {
		const { match } = this.props;
		const { app } = this.injected;
		const pageNumber = this._pageNumber;

		return (
			<>
				<div className={styles.threadViewPostsSection}>
					<PostList
						className={styles.threadViewPostList}
						itemClassName={styles.threadViewPostListItem}
					>
						{this._model.posts!.map(post => (
							<PostListItem
								key={post.id}
								postedBy={post.postedBy.name}
								postedOn={post.postedOn}
								editedOn={post.editedOn!}
								href={`/users/${post.postedBy.id}`}
							>
								{post.content}
							</PostListItem>
						))}
					</PostList>
				</div>
				<footer className={styles.threadViewFooter}>
					<div className={styles.threadViewFooterColumnLeft} />
					<div className={styles.threadViewFooterColumnMiddle}>
					{this._model.posts!.length > 0 && (
						<PageBar
							selectedPageNumber={pageNumber === "last" 
								? this._model.thread!.pages 
								: pageNumber
							}
							maxPageNumber={this._model.thread!.pages}
							onPageSelect={this.onPageChange}
						/>
					)}
					</div>
					<div className={styles.threadViewFooterColumnRight}>
						<ActionButton className={styles.threadViewNewPostButton}
							disabled={!app.currentUser}>
							<RouteLink
								className={styles.threadViewNewPostLink}
								to={`/threads/${this._model.thread!.id}/posts/new`}
							>
								{Strings.ThreadViewNewPost}
							</RouteLink>
						</ActionButton>
					</div>
				</footer>
			</>
		);
	}
	
	@autobind
	private onPageChange(newPageNumber: number): void {
		const { history } = this.props;

		history.push({ search: `page=${newPageNumber}` });
	}
}