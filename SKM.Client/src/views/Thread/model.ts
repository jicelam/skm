import { action, computed, observable, runInAction } from "mobx";

import { PageViewModel } from "App";
import { Post, Thread } from "Data";
import serviceLocator, { ServiceName, DataService } from "Services";

export interface ThreadViewModelConfig {}

export class ThreadViewModel implements PageViewModel {
	static get defaultConfig(): ThreadViewModelConfig {
		return {};
	}
	
	@observable
	thread: Thread | null;
	
	@observable
	posts: Post[] | null;
	
	@observable
	error: string | null;

	@computed
	get title(): string | null {
		return this.thread
			? this.thread.name
			: null;
	}

	private _dataService: DataService;

	constructor(config: ThreadViewModelConfig = ThreadViewModel.defaultConfig) {
		this.thread = null;
		this.posts = null;
		this.error = null;

		this._dataService = serviceLocator.get(ServiceName.Data);
	}

	@action.bound
	async fetchThread(threadId: string): Promise<void> {
		try {
			const thread = await this._dataService.getThread(threadId);

			runInAction(() => {
				this.thread = thread;
			});
		} catch (error) {
			runInAction(() => {
				this.error = error.message;
			});
		}
	}

	@action.bound
	async fetchPosts(threadId: string, pageNumber: number | "last"): Promise<void> {
		try {
			const posts = await this._dataService.getPosts(threadId, pageNumber);

			runInAction(() => {
				this.posts = posts;
			});
		} catch (error) {
			runInAction(() => {
				this.error = error.message;
			});
		}
	}

	@action.bound
	clearPosts(): void {
		this.posts = null;
	}
}