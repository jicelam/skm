import * as React from "react";

import { Icon } from "Components";

import * as styles from "./styles.css";

export interface ErrorViewProps {
	message?: string;
}

export class ErrorView extends React.Component<ErrorViewProps> {
	render(): JSX.Element {
		const { message } = this.props;
		
		return (
			<div className={styles.errorView}>
				<section className={styles.errorDisplay}>
					<Icon 
						className={styles.errorIcon}
						icon="exclamation-circle"
					/>
					<p className={styles.errorMessage}>
						{message || "Unknown Error"}
					</p>
				</section>
			</div>
		);
	}
}