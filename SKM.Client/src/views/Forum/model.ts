import { action, computed, observable, runInAction } from "mobx";

import { PageViewModel } from "App";
import { Forum, Thread } from "Data";
import serviceLocator, { ServiceName, DataService } from "Services";

export interface ForumViewModelConfig {}

export class ForumViewModel implements PageViewModel, ForumViewModelConfig {
	static get defaultConfig(): ForumViewModelConfig {
		return {};
	}
	
	@observable
	page: number;
	
	@observable
	forum: Forum | null;
	
	@observable
	threads: Thread[] | null;
	
	@observable
	error: string | null;

	@computed
	get title(): string | null {
		return this.forum
			? this.forum.name
			: null;
	}

	private _dataService: DataService;

	constructor(config: ForumViewModelConfig = ForumViewModel.defaultConfig) {
		this.page = 1;
		this.forum = null;
		this.threads = null;
		this.error = null;

		this._dataService = serviceLocator.get(ServiceName.Data);
	}

	@action.bound
	async fetchForum(id: string): Promise<void> {
		try {
			const forum = await this._dataService.getForum(id);

			runInAction(() => {
				this.forum = forum;
			});
		} catch (error) {
			runInAction(() => {
				this.error = error.message;
			});
		}
	}

	@action.bound
	async fetchThreads(forumId: string, pageNumber: number): Promise<void> {
		try {
			const threads = await this._dataService.getThreads(forumId, pageNumber);

			runInAction(() => {
				this.threads = threads;
			});
		} catch (error) {
			runInAction(() => {
				this.error = error.message;
			});
		}
	}

	@action.bound
	clearThreads(): void {
		this.threads = null;
	}
}