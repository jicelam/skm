import autobind from "autobind-decorator";
import { observe } from "mobx";
import { inject, observer } from "mobx-react";
import * as qs from "query-string";
import * as React from "react";
import { RouteComponentProps } from "react-router";

import { AppContext, AppViewModel } from "App";
import { Strings } from "Assets";
import {
	ActionButton,
	Loading,
	PageBar,
	RouteLink,
	ThreadList,
	ThreadListItem
} from "Components";

import { InjectedComponent } from "../base";
import { ForumViewModel, ForumViewModelConfig } from "./model";
import * as styles from "./styles.css";

export interface ForumViewProps extends RouteComponentProps<{ id: string }> {
	config?: ForumViewModelConfig;
}

interface ForumViewInjected {
	app: AppViewModel;
}

@inject((context: AppContext) => ({
	app: context.model.view
}) as ForumViewInjected)
@observer
export class ForumView extends InjectedComponent<ForumViewProps, ForumViewInjected> {
	private _model: ForumViewModel;

	private _disposeObservers: (() => void)[] = [];

	private get _pageNumber(): number {
		const { location } = this.props;

		const queryParams = qs.parse(location.search);

		return queryParams.page != null
			? +queryParams.page
			: 1;
	}

	constructor(props: ForumViewProps, context?: {}) {
		super(props, context);

		const { app, config } = this.injected;

		this._model = new ForumViewModel(config);
		app.setCurrentView(this._model);

		this._disposeObservers.push(observe(this._model, "forum", change => {
			this._model.forum && this.setRouteTitle();
		}));
	}

	render(): JSX.Element {
		return (
			<div className={styles.forumView}>
				{this._model.error
					? this.renderError()
					: !(this._model.forum && this._model.threads)
						? this.renderLoading()
						: this.renderBody()
				}
			</div>
		);
	}

	componentDidMount(): void {
		this.setRouteTitle();

		const { history, match } = this.props;
		const { id: forumId } = match.params;
		
		this._disposeObservers.push(history.listen(location => {
			const queryParams = qs.parse(location.search);

			this._model.clearThreads();
			this._model.fetchThreads(forumId, queryParams.page
				? +queryParams.page
				: 1
			);
		}));

		this._model.fetchForum(forumId);
		this._model.fetchThreads(forumId, this._pageNumber);
	}

	componentDidUpdate(): void {
		this.setRouteTitle();
	}

	componentWillUnmount(): void {
		for (let dispose of this._disposeObservers) { dispose(); }
	}
	
	private setRouteTitle(): void {
		if (!this._model.forum) {
			return;
		}
		
		document.title = Strings.ForumViewRouteTitle(this._model.forum.name, this._pageNumber);
	}

	private renderError(): JSX.Element {
		return (
			<div className={styles.forumViewError}>
				{this._model.error}
			</div>
		);
	}

	private renderLoading(): JSX.Element {
		return (
			<Loading />
		);
	}

	private renderBody(): JSX.Element {
		const { app } = this.injected;

		return (
			<>
				<div className={styles.forumViewThreadsSection}>
					<ThreadList className={styles.forumViewThreads}>
						{this._model.threads!.map(thread => (
							<ThreadListItem
								key={thread.id}
								className={styles.forumViewThread}
								name={thread.name}
								createdBy={thread.createdBy.name}
								createdOn={thread.createdOn}
								lastPostBy={thread.lastPostBy.name}
								lastPostOn={thread.lastPostOn}
								href={`/threads/${thread.id}`}
							/>
						))}
					</ThreadList>
				</div>
				<footer className={styles.forumViewFooter}>
					<div className={styles.forumViewFooterColumnLeft} />
					<div className={styles.forumViewFooterColumnMiddle}>
						{this._model.threads!.length > 0 && (
							<PageBar
								selectedPageNumber={this._pageNumber}
								maxPageNumber={this._model.forum!.pages}
								onPageSelect={this.onPageChange}
							/>
						)}
					</div>
					<div className={styles.forumViewFooterColumnRight}>
						<ActionButton className={styles.forumViewNewThreadButton}
							disabled={!app.currentUser}>
							<RouteLink
								className={styles.forumViewNewThreadLink}
								to={`/forums/${this._model.forum!.id}/threads/new`}
							>
								{Strings.ForumViewNewThread}
							</RouteLink>
						</ActionButton>
					</div>
				</footer>
			</>
		);
	}

	@autobind
	private onPageChange(newPageNumber: number): void {
		const { history } = this.props;

		history.push({ search: `page=${newPageNumber}` });
	}
}