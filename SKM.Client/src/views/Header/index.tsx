import autobind from "autobind-decorator";
import * as classnames from "classnames";
import { inject, observer } from "mobx-react";
import * as React from "react";

import { AppContext, AppViewModel } from "App";
import { Strings } from "Assets";
import {
	AppLogo,
	Icon,
	RouteLink,
	Tooltip,
	TooltipButton,
	TooltipHorizontalPosition
} from "Components";

import { InjectedComponent } from "../base";
import * as styles from "./styles.css";

export interface HeaderViewProps {}

export interface HeaderViewInjected {
	model: AppViewModel;
}

@inject((context: AppContext) => ({
	model: context.model.view
}) as HeaderViewInjected)
@observer
export class HeaderView extends InjectedComponent<HeaderViewProps, HeaderViewInjected> {
	private get _loginRoute(): string { return "/login"; }
	private get _logoutRoute(): string { return "/logout"; }
	private get _myProfileRoute(): string { return "/users/me"; }
	
	render(): JSX.Element {
		const { model } = this.injected;

		const headerText = (model.current && model.current.title) || "";

		return (
			<header className={styles.headerView}>
				<div className={styles.headerViewLeftColumn}>
					<RouteLink to="/">
						<AppLogo className={styles.headerViewLogo} />
					</RouteLink>
				</div>
				<div className={styles.headerViewMiddleColumn}>
					<div className={styles.headerViewBanner} title={headerText}>
						{headerText}
					</div>
				</div>
				<div className={styles.headerViewRightColumn}>
					{model.loading
						? null
						: !model.currentUser
							? <RouteLink className={styles.headerViewButton}
								to={this._loginRoute}
								onClick={this.redirectToLogin}>
								<Icon
									className={styles.headerViewIcon}
									icon="sign-in-alt"
									title={Strings.HeaderLogin}
								/>
							</RouteLink>
							: <div className={styles.headerViewButton}>
								<Icon
									className={styles.headerViewIcon}
									icon="user-circle"
									title={Strings.HeaderProfile} 
									onClick={this.toggleProfile}
								/>
								{model.profileOpen && (
									<Tooltip
										bodyClassName={styles.userProfileTooltip}
										horizontalPosition={TooltipHorizontalPosition.Right}
									>
										<RouteLink
											to={this._myProfileRoute}
											onClick={this.toggleProfile}
										>
											<TooltipButton>
												My Profile
											</TooltipButton>
										</RouteLink>
										<RouteLink
											to={this._logoutRoute}
											onClick={this.redirectToLogout}>
											<TooltipButton>
												Log Out
											</TooltipButton>
										</RouteLink>
									</Tooltip>
								)}
							</div>
					}
					<Icon
						className={classnames(
							styles.headerViewButton,
							styles.headerViewIcon
						)}
						icon="cog"
						title={Strings.HeaderSettings}
					/>
				</div>
			</header>
		);
	}

	@autobind
	private toggleProfile(): void {
		const { model } = this.injected;

		model.setProfileOpen(!model.profileOpen);
	}

	@autobind
	private redirectToLogin(event: React.MouseEvent<HTMLElement>): void {
		event.preventDefault();
		event.stopPropagation();

		const { location } = window;

		location.assign(`${location.protocol}//${location.host}${this._loginRoute}`);
	}

	@autobind
	private async redirectToLogout(event: React.MouseEvent<HTMLElement>): Promise<void> {
		event.preventDefault();
		event.stopPropagation();

		const { location } = window;

		location.assign(`${location.protocol}//${location.host}${this._logoutRoute}`);
	}
}