import * as React from "react";

export abstract class InjectedComponent<PropsType = {}, InjectedType = {}, StateType = {}>
	extends React.Component<PropsType, StateType> {
	get injected(): PropsType & InjectedType {
		return this.props as PropsType & InjectedType;
	}
}