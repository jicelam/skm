import { computed } from "mobx";

import { PageViewModel } from "App";
import { Strings } from "Assets";
import serviceLocator, { ServiceName, SecurityService } from "Services";

export interface LogoutViewModelConfig {}

export class LogoutViewModel implements PageViewModel {
	static get defaultConfig(): LogoutViewModelConfig {
		return {};
	}

	@computed
	get title(): string | null {
		return Strings.LogoutViewTitle;
	}

	private _securityService: SecurityService;

	constructor(config: LogoutViewModelConfig = LogoutViewModel.defaultConfig) {
		this._securityService = serviceLocator.get(ServiceName.Security);
	}

	async logout(): Promise<void> {
		await this._securityService.logout();
	}
}