import { inject, observer } from "mobx-react";
import * as React from "react";

import { AppContext, AppViewModel } from "App";

import { InjectedComponent } from "../base";
import { LogoutViewModel, LogoutViewModelConfig } from "./model";
import * as styles from "./styles.css";

export interface LogoutViewProps {
	config?: LogoutViewModelConfig;
}

interface LogoutViewInjected {
	app: AppViewModel;
}

@inject((context: AppContext) => ({
	app: context.model.view
}) as LogoutViewInjected)
@observer
export class LogoutView extends InjectedComponent<LogoutViewProps, LogoutViewInjected> {
	private _model: LogoutViewModel;

	constructor(props: LogoutViewProps, context?: {}) {
		super(props, context);

		const { app, config } = this.injected;

		this._model = new LogoutViewModel(config);
		app.setCurrentView(this._model);
	}

	render(): JSX.Element {
		return (
			<div className={styles.logoutMessage}>
				Logging out...
			</div>
		);
	}

	async componentDidMount(): Promise<void> {
		await this._model.logout();

		const { location } = window;
		location.replace(`${location.protocol}//${location.host}`);
	}
}