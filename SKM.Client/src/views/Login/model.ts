import { computed, observable, action } from "mobx";

import { PageViewModel } from "App";
import { Strings } from "Assets";
import serviceLocator, { ServiceName, SecurityService } from "Services";

export interface LoginViewModelConfig {}

export class LoginViewModel implements PageViewModel, LoginViewModelConfig {
	static get defaultConfig(): LoginViewModelConfig {
		return {};
	}

	@observable
	userName: string | null = null;
	
	@observable
	password: string | null = null;

	@observable
	userNameError: string | null = null;

	@observable
	passwordError: string | null = null;

	@computed
	get title(): string | null {
		return Strings.LoginViewTitle;
	}

	private _securityService: SecurityService;

	constructor(config: LoginViewModelConfig = LoginViewModel.defaultConfig) {
		this._securityService = serviceLocator.get(ServiceName.Security);
	}

	@action.bound
	setUserName(value: string): void {
		this.userName = value;
		this.userNameError = null;
	}

	@action.bound
	setPassword(value: string): void {
		this.password = value;
		this.passwordError = null;
	}

	@action.bound
	async submit(): Promise<LoginSubmissionResult> {
		if (!this.validateLogin()) {
			return LoginSubmissionResult.Failed;
		}

		await this._securityService.login({
			password: this.password!,
			userName: this.userName!
		});

		return LoginSubmissionResult.Successful;
	}

	@action
	private validateLogin(): boolean {
		let loginValid = true;
		if (!this.userName) {
			this.userNameError = Strings.LoginNoUserNameError;
			loginValid = false;
		}

		if (!this.password) {
			this.passwordError = Strings.LoginNoPasswordError;
			loginValid = false;
		}

		return loginValid;
	}
}

export const enum LoginSubmissionResult { Successful, Failed }