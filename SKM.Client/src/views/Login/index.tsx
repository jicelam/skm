import autobind from "autobind-decorator";
import { inject, observer } from "mobx-react";
import * as React from "react";
import { RouteComponentProps } from "react-router";

import { AppContext, AppViewModel } from "App";
import { Strings } from "Assets";
import { ActionButton, Card, CardSection, CardFooter, Form, Input } from "Components";

import { InjectedComponent } from "../base";
import { LoginViewModel, LoginViewModelConfig, LoginSubmissionResult } from "./model";
import * as styles from "./styles.css";

export interface LoginViewProps extends RouteComponentProps {
	config?: LoginViewModelConfig;
}

interface LoginViewInjected {
	app: AppViewModel;
}

@inject((context: AppContext) => ({
	app: context.model.view
}) as LoginViewInjected)
@observer
export class LoginView extends InjectedComponent<LoginViewProps, LoginViewInjected> {
	private _model: LoginViewModel;

	constructor(props: LoginViewProps, context?: {}) {
		super(props, context);

		const { app, config } = this.injected;

		this._model = new LoginViewModel(config);
		app.setCurrentView(this._model);
	}

	render(): JSX.Element {
		return (
			<div className={styles.loginView}>
				<Card className={styles.loginFormContainer}>
					<Form className={styles.loginForm}>
						<CardSection className={styles.loginFormGroup}>
							<label className={styles.loginFormLabel}>
								{Strings.LoginUserNameLabel}
							</label>
							<Input
								value={this._model.userName || ""}
								onChange={this.onUserNameChange}
							/>
						</CardSection>
						<CardSection className={styles.loginFormGroup}>
							<label className={styles.loginFormLabel}>
								{Strings.LoginPasswordLabel}
							</label>
							<Input
								type="password"
								value={this._model.password || ""}
								onChange={this.onPasswordChange}
							/>
						</CardSection>
						<CardFooter className={styles.loginFormFooter}>
							<ActionButton className={styles.loginButton}
								onClick={this.onSubmit}>
								{Strings.LoginButton}
							</ActionButton>
						</CardFooter>
					</Form>
				</Card>
			</div>
		);
	}

	componentDidMount(): void {
		document.title = Strings.LoginViewRouteTitle;
	}

	@autobind
	private onUserNameChange(event: React.ChangeEvent<HTMLInputElement>): void {
		this._model.setUserName(event.target.value);
	}

	@autobind
	private onPasswordChange(event: React.ChangeEvent<HTMLInputElement>): void {
		this._model.setPassword(event.target.value);
	}

	@autobind
	private async onSubmit(): Promise<void> {
		const result = await this._model.submit();
		if (result === LoginSubmissionResult.Failed) {
			return;
		}

		const { location } = window;
		location.replace(`${location.protocol}//${location.host}`);
	}
}