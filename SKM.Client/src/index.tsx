import * as mobx from "mobx";
import * as React from "react";
import * as ReactDOM from "react-dom";

import { App } from "App";
import { ServiceLocator, ServiceName } from "Services";
import { HttpDataService } from "Services/Data/Http";
import { HttpSecurityService } from "Services/Security/Http";

mobx.configure({
	enforceActions: "observed"
});

const securityService = new HttpSecurityService();
const dataService = new HttpDataService(securityService);
ServiceLocator.register({
	[ServiceName.Data]: dataService,
	[ServiceName.Security]: securityService
});

function renderApp(AppComponent: typeof App): void {
	ReactDOM.render(
		<App onLoad={loadServices}/>,
		document.getElementById("app-root")
	);
}

async function loadServices(): Promise<void> {
	await Promise.all([
		dataService.load(),
		securityService.load()
	]);
}

if (module.hot) {
	module.hot.accept("App", () => {
		const UpdatedApp = require("App");

		renderApp(UpdatedApp);
	});
}

renderApp(App);