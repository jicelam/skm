import {
	Forum,
	Notice,
	Post,
	Reprimand,
	ReprimandType,
	Thread,
	User,
	UserInfo
} from "Data";

export interface PagedDataResult<ItemType> {
	items: ItemType[];
	nextToken: string | null;
	totalCount: number;
}

export interface FavoritedItemData {
	id: string;
	type: FavoritedItemType;
}

export interface ForumData {
	createdOn: Date;
	description: string;
	iconUrl: string;
	id: string;
	name: string;
	threadCount: number;
}

export interface NoticeData {
	content: string;
	createdOn: Date;
	id: string;
	name: string;
}

export interface PostData {
	content: string;
	createdBy: UserSummaryData;
	createdOn: Date;
	id: string;
	updatedOn: Date;
}

export interface ReprimandData {
	assignedBy: UserSummaryData;
	assignedOn: Date;
	assignedTo: UserSummaryData;
	expiresOn: Date | null;
	forumId: string | null;
	id: string;
	reason: string;
	type: ReprimandType;
}

export interface ThreadData {
	createdBy: UserSummaryData;
	createdOn: Date;
	iconUrl: string;
	id: string;
	lastPostBy: UserSummaryData;
	lastPostOn: Date;
	name: string;
	postCount: number;
}

export interface UserSummaryData {
	avatarUrl: string;
	id: string;
	name: string;
}

export interface UserData extends UserSummaryData {
	caption: string;
	joinedOn: Date;
	location: string;
	signature: string;
}

export const enum FavoritedItemType {
	Forum = 0,
	Thread = 1
}

export class DataConverter {
	private _forumPageSize: number;
	private _threadPageSize: number;

	constructor(
		forumPageSize: number,
		threadPageSize: number
	) {
		this._forumPageSize = forumPageSize;
		this._threadPageSize = threadPageSize;
	}

	fromForumData(data: ForumData): Forum {
		return {
			description: data.description,
			iconUrl: data.iconUrl,
			id: data.id,
			name: data.name,
			pages: Math.ceil(data.threadCount / this._forumPageSize)
		};
	}

	fromNoticeData(data: NoticeData): Notice {
		return {
			id: data.id,
			name: data.name,
			publishedOn: data.createdOn,
			summary: data.content
		};
	}

	fromPostData(data: PostData): Post {
		return {
			content: data.content,
			editedOn: data.updatedOn,
			id: data.id,
			postedBy: this.fromUserSummaryData(data.createdBy) as User,
			postedOn: data.createdOn,
		};
	}

	fromReprimandData(data: ReprimandData): Reprimand {
		return {
			assignedBy: this.fromUserSummaryData(data.assignedBy),
			assignedOn: data.assignedOn,
			assignedTo: this.fromUserSummaryData(data.assignedTo),	
			expiresOn: data.expiresOn,
			forumId: data.forumId,
			id: data.id,
			reason: data.reason,
			type: data.type
		};
	}

	fromThreadData(data: ThreadData): Thread {
		return {
			createdBy: this.fromUserSummaryData(data.createdBy),
			createdOn: data.createdOn,
			iconUrl: data.iconUrl,
			id: data.id,
			lastPostBy: this.fromUserSummaryData(data.lastPostBy),
			lastPostOn: data.lastPostOn,
			name: data.name,
			pages: Math.ceil(data.postCount / this._threadPageSize)
		};
	}

	fromUserData(data: UserData): User {
		return {
			avatarUrl: data.avatarUrl,
			caption: data.caption,
			id: data.id,
			joinedOn: data.joinedOn,
			location: data.location,
			name: data.name,
			signature: data.signature
		};
	}

	fromUserSummaryData(data: UserSummaryData): UserInfo {
		return {
			avatarUrl: data.avatarUrl,
			id: data.id,
			name: data.name
		};
	}
}