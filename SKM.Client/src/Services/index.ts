import { DataService } from "./Data";
import { SecurityService } from "./Security";

export { DataService, SecurityService };

export const enum ServiceName {
	Data = "Data",
	Security = "Security"
}

export interface ServiceTypes {
	[ServiceName.Data]: DataService;
	[ServiceName.Security]: SecurityService;
} 

export type Service = {};

export class ServiceLocator {
	static register(services: { [serviceName: string]: ServiceTypes[ServiceName]; }): void {
		serviceLocator.register(services);
	}
	
	private _registry: { [serviceName: string]: ServiceTypes[ServiceName]; } = {};
	
	get<ServiceType extends ServiceName>(name: ServiceType): ServiceTypes[ServiceType] {
		return (this._registry[name] || null) as ServiceTypes[ServiceType];
	}

	private register(services: { [serviceName: string]: ServiceTypes[ServiceName]; }): void {
		for (let serviceName of Object.getOwnPropertyNames(services)) {
			if (this._registry[serviceName]) {
				throw new Error(`The service "${serviceName}" has already been registered`);
			}
			
			this._registry[serviceName] = services[serviceName];
		}
	}
}

const serviceLocator = new ServiceLocator();

export default serviceLocator;