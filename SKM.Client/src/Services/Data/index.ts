import {
	Forum,
	Notice,
	Post,
	Reprimand,
	Thread,
	User
} from "Data";

export interface DataService {
	readonly forumPageSize: number;
	readonly threadPageSize: number;

	getNotices(pageNumber?: number): Promise<Notice[]>;

	getFavoriteForums(pageNumber?: number): Promise<Forum[]>;

	getForum(id: string): Promise<Forum>;
	getForums(pageNumber?: number): Promise<PagedResult<Forum>>;
	getThread(id: string): Promise<Thread>;
	getThreads(forumId: string, pageNumber?: number): Promise<Thread[]>;
	getPost(id: string): Promise<Post>;
	getPosts(threadId: string, pageNumber?: number | "last"): Promise<Post[]>;

	createThread(params: ThreadCreationParams): Promise<Thread>;
	createPost(params: PostCreationParams): Promise<Post>;

	getUser(id: string): Promise<User>;
	getReprimands(userId: string, pageNumber?: number): Promise<Reprimand[]>;
}

export interface ThreadCreationParams {
	forumId: string;
	iconId: string;
	name: string;
	postContent: string;
}

export interface PostCreationParams {
	content: string;
	threadId: string;
}

export interface PagedResult<ResultType> {
	items: ResultType[];
	pageSize: number;
	totalPages: number;
}