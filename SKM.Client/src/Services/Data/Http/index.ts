import {
	Forum,
	Notice,
	Post,
	Reprimand,
	Thread,
	User
} from "Data";

import { SecurityService } from "../..";
import {
	DataConverter,
	FavoritedItemData,
	ForumData,
	NoticeData,
	PagedDataResult,
	PostData,
	ReprimandData,
	ThreadData,
	UserData
} from "../../converters";
import {
	DataService,
	PagedResult,
	PostCreationParams,
	ThreadCreationParams
} from "../";

export class HttpDataService implements DataService {
	readonly defaultPageSize = 10;
	readonly forumListPageSize = this.defaultPageSize;
	readonly forumPageSize = this.defaultPageSize;
	readonly threadPageSize = this.defaultPageSize;

	private _dataConverter = new DataConverter(
		this.forumPageSize,
		this.threadPageSize
	);
	private _securityService: SecurityService;

	private get _baseEndpoint(): string { return `${window.location.protocol}//${window.location.host}/api`; }
	private get _forumsEndpoint(): string { return `${this._baseEndpoint}/forums`; }
	private get _postsEndpoint(): string { return `${this._baseEndpoint}/posts`; }
	private get _threadsEndpoint(): string { return `${this._baseEndpoint}/threads`; }
	private get _usersEndpoint(): string { return `${this._baseEndpoint}/users`; }

	constructor(securityService: SecurityService) {
		this._securityService = securityService;
	}

	async load(): Promise<void> {}

	async getNotices(pageNumber?: number): Promise<Notice[]> {
		const response = await fetch(`${this._baseEndpoint}/notices${this.getQueryOptions(this.defaultPageSize, pageNumber)}`);

		const results = await response.json() as PagedDataResult<NoticeData>;

		return results.items.map(notice => this._dataConverter.fromNoticeData(notice));
	}

	async getFavoriteForums(pageNumber?: number): Promise<Forum[]> {
		const currentUser = this._securityService.currentUser;
		if (!currentUser) {
			throw new Error("No user is currently logged in");
		}

		const response = await fetch(`${this._usersEndpoint}/${currentUser.id}/favorites${this.getQueryOptions(this.defaultPageSize, pageNumber)}`);
		
		await this.checkForErrors(response, {
			notFound: "The current user was not found"
		});

		// TODO
		const results = await response.json() as PagedDataResult<FavoritedItemData>;

		return [];
	}
	
	async getForum(id: string): Promise<Forum> {
		const response = await fetch(`${this._forumsEndpoint}/${id}`);

		await this.checkForErrors(response, {
			notFound: `No forum was found with the id "${id}"`
		});

		const result = await response.json() as ForumData;

		return this._dataConverter.fromForumData(result);
	}

	async getForums(pageNumber?: number): Promise<PagedResult<Forum>> {
		const response = await fetch(`${this._forumsEndpoint}${this.getQueryOptions(this.forumListPageSize, pageNumber)}`);

		await this.checkForErrors(response);

		const results = await response.json() as PagedDataResult<ForumData>;

		return {
			items: results.items.map(forum => this._dataConverter.fromForumData(forum)),
			pageSize: this.forumListPageSize,
			totalPages: Math.ceil(results.totalCount / this.forumListPageSize)
		};
	}

	async getThread(id: string): Promise<Thread> {
		const response = await fetch(`${this._threadsEndpoint}/${id}`);

		await this.checkForErrors(response, {
			notFound: `No thread was found with the id "${id}"`
		});

		const result = await response.json() as ThreadData;

		return this._dataConverter.fromThreadData(result);
	}

	async getThreads(forumId: string, pageNumber?: number): Promise<Thread[]> {
		const response = await fetch(`${this._forumsEndpoint}/${forumId}/threads${this.getQueryOptions(this.forumPageSize, pageNumber)}`);

		await this.checkForErrors(response, {
			notFound: `No forum was found with the id "${forumId}"`
		});

		const results = await response.json() as PagedDataResult<ThreadData>;

		return results.items.map(thread => this._dataConverter.fromThreadData(thread));
	}

	async getPost(id: string): Promise<Post> {
		const response = await fetch(`${this._postsEndpoint}/${id}`);

		await this.checkForErrors(response, {
			notFound: `No post was found with the id "${id}"`
		});

		const result = await response.json() as PostData;

		return this._dataConverter.fromPostData(result);
	}

	async getPosts(threadId: string, pageNumber?: number | "last"): Promise<Post[]> {
		const response = await fetch(`${this._threadsEndpoint}/${threadId}/posts${this.getQueryOptions(this.threadPageSize, pageNumber)}`);

		await this.checkForErrors(response, {
			notFound: `No thread was found with the id "${threadId}"`
		});

		const results = await response.json() as PagedDataResult<PostData>;

		let posts = results.items.map(post => this._dataConverter.fromPostData(post));
		if (pageNumber === "last") {
			posts = posts.reverse();
		}

		return posts;
	}

	async createThread({ forumId, iconId, name, postContent }: ThreadCreationParams): Promise<Thread> {
		const formData = new FormData();
		formData.set("content", postContent);
		formData.set("iconUrl", iconId);
		formData.set("name", name);

		const response = await fetch(`${this._forumsEndpoint}/${forumId}/threads/new`, {
			method: "POST",
			body: formData,
			credentials: "include"
		});

		await this.checkForErrors(response);

		const result = await response.json() as ThreadData;

		return this._dataConverter.fromThreadData(result);
	}

	async createPost({ threadId, content }: PostCreationParams): Promise<Post> {
		const formData = new FormData();
		formData.set("content", content);

		const response = await fetch(`${this._threadsEndpoint}/${threadId}/posts/new`, {
			method: "POST",
			body: formData,
			credentials: "include"
		});
		
		await this.checkForErrors(response);

		const result = await response.json() as PostData;

		return this._dataConverter.fromPostData(result);
	}

	async getUser(id: string): Promise<User> {
		const response = await fetch(`${this._usersEndpoint}/${id}`);
		
		await this.checkForErrors(response, {
			notFound: `No user was found with the id "${id}"`
		});

		const result = await response.json() as UserData;

		return this._dataConverter.fromUserData(result);
	}

	async getReprimands(userId: string, pageNumber?: number): Promise<Reprimand[]> {
		const response = await fetch(`${this._usersEndpoint}/${userId}/reprimands${this.getQueryOptions(this.defaultPageSize, pageNumber)}`);

		await this.checkForErrors(response, {
			notFound: `No user was found with the id "${userId}"`
		});

		const results = await response.json() as PagedDataResult<ReprimandData>;

		return results.items.map(reprimand => this._dataConverter.fromReprimandData(reprimand));
	}

	private getQueryOptions(pageSize: number, pageNumber?: number | "last"): string {
		let query = `?take=${pageSize}`;
		if (pageNumber === "last") {
			query = `${query}&reverse=true`;

			return query;
		}

		if (pageNumber == null) {
			return query;
		}

		if (isNaN(pageNumber)) {
			throw new Error("The page number parameter cannot be NaN");
		}

		if (pageNumber <= 0) {
			throw new Error("The page number parameter cannot be zero or negative");
		}

		return `${query}&skip=${(pageNumber - 1) * pageSize}`;
	}

	private async checkForErrors(response: Response, errorMessages?: { notFound?: string; unauthorized?: string; }): Promise<void> {
		const { status } = response;
		if (200 <= status && status < 300) {
			return;
		}

		if (status === 403) {
			throw new Error(errorMessages && errorMessages.unauthorized
				? errorMessages.unauthorized 
				: "You are not authorized to access this data"
			);
		}

		if (status === 404) {
			throw new Error(errorMessages && errorMessages.notFound
				? errorMessages.notFound
				: "No data was found with that id"
			);
		}

		const text = await response.text();

		throw new Error(text);
	}
}