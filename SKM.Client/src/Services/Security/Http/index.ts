import { User } from "Data";

import { DataConverter, UserData } from "../../converters";
import {
	SecurityService,
	SecurityOperationResult,
	LoginParams,
	RegistrationParams
} from "..";

export class HttpSecurityService implements SecurityService {
	private _currentUser: User | null = null;
	get currentUser(): User | null {
		return this._currentUser
			? JSON.parse(JSON.stringify(this._currentUser))
			: null;
	}

	private _dataConverter = new DataConverter(1, 1);

	private get _baseEndpoint(): string { return `${window.location.protocol}//${window.location.host}/api`; }
	private get _usersEndpoint(): string { return `${this._baseEndpoint}/users`; }

	async load(): Promise<void> {
		await this.loadCurrentUser();
	}

	async login(params: LoginParams): Promise<SecurityOperationResult> {
		const formData = new FormData();
		formData.set("name", params.userName);
		formData.set("password", params.password);
		
		const response = await fetch(`${this._usersEndpoint}/login`, {
			method: "POST",
			body: formData,
			credentials: "include"
		});

		return (200 <= response.status || response.status < 300)
			? SecurityOperationResult.Successful
			: SecurityOperationResult.Failed;
	}

	async logout(): Promise<SecurityOperationResult> {
		const response = await fetch(`${this._usersEndpoint}/logout`, {
			method: "POST",
			credentials: "include"
		});

		return (200 <= response.status || response.status < 300)
			? SecurityOperationResult.Successful
			: SecurityOperationResult.Failed;
	}

	async register(params: RegistrationParams): Promise<SecurityOperationResult> {
		const formData = new FormData();
		formData.set("email", params.email);
		formData.set("name", params.userName);
		formData.set("password", params.password);

		params.caption && formData.set("caption", params.caption);
		params.signature && formData.set("signature", params.signature);
		
		const response = await fetch(`${this._usersEndpoint}/register`, {
			method: "POST",
			body: formData,
			credentials: "include"
		});

		return (200 <= response.status || response.status < 300)
			? SecurityOperationResult.Successful
			: SecurityOperationResult.Failed;
	}

	private async loadCurrentUser(): Promise<void> {
		const response = await fetch(`${this._baseEndpoint}/users/me`, {
			credentials: "include"
		});
		if (response.status < 200 || 300 <= response.status) {
			this._currentUser = null;

			if (response.status !== 404) {
				const statusText = await response.text();

				throw new Error(statusText);
			}
			
			return;
		}

		const currentUser = await response.json() as UserData;

		this._currentUser = this._dataConverter.fromUserData(currentUser);
	}
}