import { User } from "Data";

export interface SecurityService {
	readonly currentUser: User | null;
	
	login(params: LoginParams): Promise<SecurityOperationResult>;
	logout(): Promise<SecurityOperationResult>;
	register(params: RegistrationParams): Promise<SecurityOperationResult>;
}

export interface LoginParams {
	password: string;
	userName: string;
}

export interface RegistrationParams {
	caption?: string;
	email: string;
	password: string;
	signature?: string;
	userName: string;
}

export const enum SecurityOperationResult { Successful, Failed }