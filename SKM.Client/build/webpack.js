const webpack = require("webpack");
const postCssHexToRgba = require("postcss-hexrgba");
const postCssImport = require("postcss-import");
const postCssPresetEnv = require("postcss-preset-env");
const postCssReporter = require("postcss-reporter");
const stylelint = require("stylelint");

const HtmlWebpackPlugin = require("html-webpack-plugin");

const { paths } = require("./config");

module.exports = {
	entry: {
		index: "./src/index.tsx"
	},
	output: {
		path: paths.out,
		publicPath: paths.app,
		filename: "[name].js",
		chunkFilename: "[name].bundle.js"
	},
	resolve: {
		alias: {
			"App": `${paths.source}/App`,
			"Assets": `${paths.source}/Assets`,
			"Components": `${paths.source}/Components`,
			"Data": `${paths.source}/Data`,
			"Services": `${paths.source}/services`,
			"Util": `${paths.source}/Util`,
			"Views": `${paths.source}/views`
		},
		extensions: [".js", ".ts", ".tsx"],
	},
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				loader: "tslint-loader",
				enforce: "pre",
				options: {
					emitErrors: true
				}
			},
			{
				test: /\.tsx?$/,
				use: "ts-loader"
			},
			{
				test: /\.css$/,
				use: [
					{ loader: "style-loader" },
					{
						loader: "css-loader",
						options: {
							camelCase: true,
							localIdentName: "[local].[hash:8]",
							modules: true
						}
					},
					{
						loader: "postcss-loader",
						options: {
							ident: "postcss",
							plugins: () => [
								stylelint(),
								postCssImport({ addDependencyTo: webpack }),
								postCssPresetEnv({
									stage: 4,
									features: {
										'custom-properties': {
											preserve: false
										},
										'nesting-rules': true
									}
								}),
								postCssHexToRgba(),
								postCssReporter({ throwError: true })
							]
						}
					}
				]
			},
			{
				test: [
					/\.eot$/,
					/\.svg$/,
					/\.ttf$/,
					/\.woff2?$/
				],
				use: {
					loader: "url-loader",
					options: {
						limit: Infinity
					}
				}
			}
		]
	},
	plugins: [
		new HtmlWebpackPlugin({ template: `${paths.public}/index.html` })
	]
};