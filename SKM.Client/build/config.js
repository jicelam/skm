const path = require("path");

const appHttpPort = 63334;
const appHttpsPort = 44382;
const devServerPort = 3150;
module.exports = {
	hosts: {
		appHttp: `localhost:${appHttpPort}`,
		appHttps: `localhost:${appHttpsPort}`,
		devServer: `localhost:${devServerPort}`
	},
	paths: {
		app: "/",
		out: path.resolve(__dirname, "../../SKM.Api/wwwroot"),
		public: path.resolve(__dirname, "../public"),
		root: path.resolve(__dirname, ".."),
		source: path.resolve(__dirname, "../src")
	},
	ports: {
		appHttp: appHttpPort,
		appHttps: appHttpsPort,
		devServer: devServerPort
	}
};