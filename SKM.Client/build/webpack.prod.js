const webpackMerge = require("webpack-merge");

const config = require("./webpack");

module.exports = webpackMerge(config, {
	mode: "production",
	optimization: {
		minimize: true
	}
});