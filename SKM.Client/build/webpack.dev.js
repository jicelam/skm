const webpackMerge = require("webpack-merge");

const config = require("./webpack");

module.exports = webpackMerge(config, {
	mode: "development",
	devtool: "inline-source-map"
});