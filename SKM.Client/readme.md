# SKM.Client

SKM is a test web app meant to demonstrate knowledge of/play around with various full-stack web technologies. The app is a simple forum system that allows for basic forums, threads and posts. It is written to be an SPA, and so the SKM.Client project appropriately contains all of the front-end presentation code. This is ultimately more of a toy project, so there's not been much attention paid to SEO or other typical SPA issues you would try to address in a production-ready app.

## Building and Running the Projects

SKM.Client uses Yarn for dependency management (and thus the latest version of Node as well). Once you've installed that on your machine, building the project is as simple as running

```
yarn install
```

and

```
yarn build
```

The project uses webpack as its sole bundling system, and is configured to push its JS output to the sibling SKM.Api project's wwwroot directory. The web app can be accessed to running the above build command, starting the api project and then navigating to https://localhost:44382 in the browser of your choice (note: all of development has happened in Chrome so far however, so no guarantees are made at this point that everything will look correct with anything else). Consult the api project's readme file for information about setting up the database and seeding it with test data.

When making changes/additions to the front end, the following couple of options are available

1. Start the api project and run webpack in watch mode with `yarn watch`

2. Start the api project, run the webpack dev server with `yarn start` and navigate to http://localhost:3150. The project doesn't fully function with hot-reloading as of yet, so any changes made will force the browser to refresh.

There is also an NPM script called `build:prod` for building the app in webpack's production mode.

## Technologies

The SKM.Client project is written entirely in TypeScript, uses React for its UI, MobX for state management and React Router for routing. Styles are written in Post-CSS with a number of plugins meant roughly to correspond with future CSS syntax. Linters have been set up for both TypeScript and CSS, and are meant to error out the build when they detect any problems.

## Project Structure

All of the project source code lives in the src directory (appropriately enough). The project aims to maintain the distinction betwen smart and dumb components that is common to a lot of SPAs, so care should be taken to observe that convention when writing any new UI. There are no current plans to port this app as a mobile app, so UI code doesn't take any special pains to avoid direct calls to the DOM. The project's top level folders consist of the following:

- __App__: Top-level application and initialization code such as the root app component and route definitions.
- __Assets__: Reusable media assets and display text which might eventually need to be localized
- __Components__: Dumb, reusable components not aware of the state store
- __Data__: Data interface definitions
- __Services__: Service api interface definitions and implementations
- __Styles__: App-wide CSS variable and style definitions
- __Util__: Miscellaneous utility functions and classes
- __Views__: Smart, state-aware components with sub-folders that approximately match the different route definitions

## Development Environment

SKM.Client has mostly been developed from the integrated terminal in Visual Studio Code. As such, development should be possible from any text editor or IDE of your choosing. If you opt to stick with Visual Studio Code however, it is recommended to utilize the following plugins for the smoothest experience:

- [CSS Modules](https://marketplace.visualstudio.com/items?itemName=clinyong.vscode-css-modules)
- [PostCSS Syntax](https://marketplace.visualstudio.com/items?itemName=ricard.PostCSS)
- [Stylelint](https://marketplace.visualstudio.com/items?itemName=shinnn.stylelint)
- [TSLint](https://marketplace.visualstudio.com/items?itemName=ms-vscode.vscode-typescript-tslint-plugin)