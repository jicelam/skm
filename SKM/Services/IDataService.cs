﻿using SKM.Models.Data;
using SKM.Models.Data.Create;
using SKM.Models.Data.Update;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SKM.Services {
	public interface IDataService {
		Task<FavoritedItemData> GetFavorite(Guid favoriteId);
		Task<QueryResult<FavoritedItemData>> GetFavorites(Guid userId, QueryOptions<FavoritedItemData> options = null);
		Task<ForumData> GetForum(Guid forumId);
		Task<QueryResult<ForumData>> GetForums(QueryOptions<ForumData> options = null);
		Task<NoticeData> GetNotice(Guid noticeId);
		Task<QueryResult<NoticeData>> GetNotices(QueryOptions<NoticeData> options = null);
		Task<QueryResult<NoticeData>> GetNotices(Guid? forumId, QueryOptions<NoticeData> options = null);
		Task<PostData> GetPost(Guid postId);
		Task<QueryResult<PostData>> GetPosts(Guid threadId, QueryOptions<PostData> options = null);
		Task<QueryResult<ReprimandData>> GetReprimands(Guid userId, QueryOptions<ReprimandData> options = null);
		Task<ThreadData> GetThread(Guid threadId);
		Task<QueryResult<ThreadData>> GetThreads(Guid forumId, QueryOptions<ThreadData> options = null);
		Task<UserData> GetUser(Guid userId);
		Task<QueryResult<UserData>> GetUsers(QueryOptions<UserData> options = null);

		Task<ForumData> CreateForum(ForumCreateData data);
		Task<NoticeData> CreateNotice(NoticeCreateData data);
		Task<PostData> CreatePost(PostCreateData data);
		Task<ThreadData> CreateThread(ThreadCreateData data);
		Task<ReprimandData> ReprimandUser(ReprimandCreateData data);

		Task<ForumData> UpdateForum(ForumUpdateData data);
		Task<NoticeData> UpdateNotice(NoticeUpdateData data);
		Task<PostData> UpdatePost(PostUpdateData data);
		Task<ThreadData> UpdateThread(ThreadUpdateData data);

		Task DeleteForum(Guid forumId);
		Task DeleteNotice(Guid noticeId);
		Task DeleteThread(Guid threadId);
		Task DeletePost(Guid postId);
	}

	public class QueryOptions<ResultType> {
		public Func<IQueryable<ResultType>, IQueryable<ResultType>> Extension { get; set; }
		public int? Skip { get; set; }
		public int? Take { get; set; }
	}

	public class QueryResult<ResultType> {
		public IEnumerable<ResultType> Items { get; private set; }
		public int SkipAmount { get; private set; }
		public int TotalCount { get; private set; }

		public QueryResult(
			IEnumerable<ResultType> items,
			int skipAmount,
			int totalCount
		) {
			Items = items;
			SkipAmount = skipAmount;
			TotalCount = totalCount;
		}
	}
}
