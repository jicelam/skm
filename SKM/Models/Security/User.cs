﻿using System;

namespace SKM.Models.Security {
	public class User {
		public Guid Id { get; set; }
		public string Email { get; set; }
	}
}
