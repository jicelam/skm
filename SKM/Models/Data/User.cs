﻿using System;

namespace SKM.Models.Data {
	public class UserSummaryData {
		public string AvatarUrl { get; set; }
		public Guid Id { get; set; }
		public string Name { get; set; }
	}

	public class UserData : UserSummaryData {
		public string Caption { get; set; }
		public DateTime JoinedOn { get; set; }
		public string Location { get; set; }
		public string Signature { get; set; }
	}
}
