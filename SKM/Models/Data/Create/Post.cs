﻿using System;

namespace SKM.Models.Data.Create {
	public class PostCreateData {
		public string Content { get; set; }
		public Guid CreatedById { get; set; }
		public Guid ThreadId { get; set; }

		public PostCreateData(
			Guid threadId,
			Guid createdById,
			string content
		) {
			Content = content;
			CreatedById = createdById;
			ThreadId = threadId;
		}
	}
}
