﻿using System;

namespace SKM.Models.Data.Create {
	public class ForumCreateData {
		public Guid CreatedById { get; set; }
		public string Description { get; set; }
		public string IconUrl { get; set; }
		public string Name { get; set; }

		public ForumCreateData(
			string name,
			Guid createdById,
			string description,
			string iconUrl
		) {
			CreatedById = createdById;
			Description = description;
			IconUrl = iconUrl;
			Name = name;
		}
	}
}
