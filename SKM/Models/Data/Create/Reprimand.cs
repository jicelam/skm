﻿using System;

namespace SKM.Models.Data.Create {
	public class ReprimandCreateData {
		public Guid AssignedById { get; set; }
		public Guid AssignedToId { get; set; }
		public DateTime? ExpiresOn { get; set; }
		public Guid ForumId { get; set; }
		public string Reason { get; set; }
		public ReprimandType Type { get; set; }

		public ReprimandCreateData(
			Guid assignedById,
			Guid assignedToId,
			DateTime? expiresOn,
			Guid forumId,
			string reason,
			ReprimandType type
		) {
			AssignedById = assignedById;
			AssignedToId = assignedToId;
			ExpiresOn = expiresOn;
			ForumId = forumId;
			Reason = reason;
			Type = type;
		}
	}
}
