﻿using System;

namespace SKM.Models.Data.Create {
	public class NoticeCreateData {
		public string Content { get; set; }
		public Guid CreatedById { get; set; }
		public Guid? ForumId { get; set; }
		public string IconUrl { get; set; }
		public string Name { get; set; }

		public NoticeCreateData(
			string content,
			Guid createdById,
			Guid? forumId,
			string iconUrl,
			string name
		) {
			Content = content;
			CreatedById = createdById;
			ForumId = forumId;
			IconUrl = iconUrl;
			Name = name;
		}
	}
}
