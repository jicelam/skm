﻿using System;

namespace SKM.Models.Data.Create {
	public class ThreadCreateData {
		public string Content { get; set; }
		public Guid CreatedById { get; set; }
		public Guid ForumId { get; set; }
		public string IconUrl { get; set; }
		public string Name { get; set; }

		public ThreadCreateData(
			Guid forumId,
			string content,
			Guid createdById,
			string iconUrl,
			string name
		) {
			Content = content;
			CreatedById = createdById;
			ForumId = forumId;
			IconUrl = iconUrl;
			Name = name;
		}
	}
}
