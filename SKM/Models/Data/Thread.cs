﻿using System;

namespace SKM.Models.Data {
	public class ThreadData {
		public UserSummaryData CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public Guid ForumId { get; set; }
		public string IconUrl { get; set; }
		public Guid Id { get; set; }
		public UserSummaryData LastPostBy { get; set; }
		public DateTime LastPostOn { get; set; }
		public string Name { get; set; }
		public int PostCount { get; set; }
		public DateTime? UpdatedOn { get; set; }
	}
}
