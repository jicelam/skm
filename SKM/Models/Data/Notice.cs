﻿using System;

namespace SKM.Models.Data {
	public class NoticeData {
		public string Content { get; set; }
		public UserSummaryData CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public Guid? ForumId { get; set; }
		public string IconUrl { get; set; }
		public Guid Id { get; set; }
		public string Name { get; set; }
		public DateTime? UpdatedOn { get; set; }
	}
}
