﻿using System;

namespace SKM.Models.Data {
	public class FavoritedItemData {
		public Guid Id { get; set; }
		public FavoritedItemType Type { get; set; }
		public Guid UserId { get; set; }
	}

	public enum FavoritedItemType {
		Forum = 0,
		Thread = 1
	}
}
