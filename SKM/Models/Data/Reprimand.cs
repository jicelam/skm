﻿using System;

namespace SKM.Models.Data {
	public class ReprimandData {
		public UserSummaryData AssignedBy { get; set; }
		public DateTime AssignedOn { get; set; }
		public UserSummaryData AssignedTo { get; set; }
		public DateTime? ExpiresOn { get; set; }
		public Guid ForumId { get; set; }
		public Guid Id { get; set; }
		public string Reason { get; set; }
		public ReprimandType Type { get; set; }
	}

	public enum ReprimandType {
		Ban = 0,
		Probation = 1
	}
}
