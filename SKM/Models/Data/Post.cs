﻿using System;

namespace SKM.Models.Data {
	public class PostData {
		public string Content { get; set; }
		public UserSummaryData CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public Guid Id { get; set; }
		public Guid ThreadId { get; set; }
		public DateTime? UpdatedOn { get; set; }
	}
}
