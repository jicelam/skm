﻿using System;

namespace SKM.Models.Data.Update {
	public class PostUpdateData {
		public Guid Id { get; set; }

		public string Content { get; set; }

		public PostUpdateData(
			Guid id,
			string content
		) {
			Id = id;
			Content = content;
		}
	}
}
