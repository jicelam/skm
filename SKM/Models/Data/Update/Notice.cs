﻿using System;

namespace SKM.Models.Data.Update {
	public class NoticeUpdateData {
		public Guid Id { get; set; }

		public string Content { get; set; }
		public string Name { get; set; }

		public NoticeUpdateData(
			Guid id,
			string content,
			string name
		) {
			Id = id;
			Content = content;
			Name = name;
		}
	}
}
