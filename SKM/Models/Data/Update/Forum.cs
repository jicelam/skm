﻿using System;

namespace SKM.Models.Data.Update {
	public class ForumUpdateData {
		public Guid Id { get; set; }

		public string Description { get; set; }
		public string IconUrl { get; set; }
		public string Name { get; set; }

		public ForumUpdateData(
			Guid id,
			string description,
			string iconUrl,
			string name
		) {
			Id = id;
			Description = description;
			IconUrl = iconUrl;
			Name = name;
		}
	}
}
