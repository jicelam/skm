﻿using System;

namespace SKM.Models.Data.Update {
	public class ThreadUpdateData {
		public Guid Id { get; set; }

		public string IconUrl { get; set; }
		public string Name { get; set; }

		public ThreadUpdateData(
			Guid id,
			string iconUrl,
			string name
		) {
			Id = id;
			IconUrl = iconUrl;
			Name = name;
		}
	}
}
