﻿using System;

namespace SKM.Models.Data {
	public class ForumData {
		public UserSummaryData CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public string Description { get; set; }
		public string IconUrl { get; set; }
		public Guid Id { get; set; }
		public string Name { get; set; }
		public int ThreadCount { get; set; }
		public DateTime? UpdatedOn { get; set; }
	}
}
