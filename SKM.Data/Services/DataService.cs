﻿using Microsoft.EntityFrameworkCore;
using SKM.Data.Models;
using SKM.Models.Data;
using SKM.Models.Data.Create;
using SKM.Models.Data.Update;
using SKM.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SKM.Data.Services {
	public class EntityDataService : IDataService, IDisposable {
		private readonly SKMContext _context;

		public EntityDataService(SKMContext context) {
			_context = context;
		}

		public void Dispose() {
			_context?.Dispose();
		}

		/* ----------------------------------- Read ----------------------------------- */
		public async Task<FavoritedItemData> GetFavorite(Guid favoriteId) {
			var favoriteQuery = _context.FavoritedItems
				.Where(fi => fi.Uuid == favoriteId)
				.ProjectTo<FavoritedItemData>();

			return await favoriteQuery.FirstOrDefaultAsync();
		}

		public async Task<QueryResult<FavoritedItemData>> GetFavorites(Guid userId, QueryOptions<FavoritedItemData> options = null) {
			var totalFavoritesQuery = _context.FavoritedItems
				.Where(fi => fi.UserId == userId)
				.ProjectTo<FavoritedItemData>();

			var pagedFavoritesQuery = GetPagedQuery(totalFavoritesQuery, options);

			return new QueryResult<FavoritedItemData>(
				await pagedFavoritesQuery.ToListAsync(),
				options?.Skip ?? 0,
				await totalFavoritesQuery.CountAsync()
			);
		}

		public async Task<ForumData> GetForum(Guid forumId) {
			var forumQuery = (
				from f in _context.Forums
				where f.Uuid == forumId
				select new ForumModelAggregate {
					Forum = f,
					ThreadCount = f.Threads.Count()
				}
			).ProjectTo<ForumData>();

			return await forumQuery.FirstOrDefaultAsync();
		}

		public async Task<QueryResult<ForumData>> GetForums(QueryOptions<ForumData> options = null) {
			var totalForumsQuery = (
				from f in _context.Forums
				select new ForumModelAggregate {
					Forum = f,
					ThreadCount = f.Threads.Count()
				}
			).ProjectTo<ForumData>();

			var pagedForumsQuery = GetPagedQuery(totalForumsQuery, options);

			return new QueryResult<ForumData>(
				await pagedForumsQuery.ToListAsync(),
				options?.Skip ?? 0,
				await totalForumsQuery.CountAsync()
			);
		}

		public async Task<NoticeData> GetNotice(Guid noticeId) {
			var noticeQuery = _context.Notices
				.Where(n => n.Uuid == noticeId)
				.ProjectTo<NoticeData>();

			return await noticeQuery.FirstOrDefaultAsync();
		}

		public async Task<QueryResult<NoticeData>> GetNotices(QueryOptions<NoticeData> options = null) {
			return await GetNotices(null, options);
		}

		public async Task<QueryResult<NoticeData>> GetNotices(Guid? forumId, QueryOptions<NoticeData> options = null) {
			var totalNoticesQuery = _context.Notices
				.Where(n => n.ForumId == forumId)
				.ProjectTo<NoticeData>();

			var pagedNoticesQuery = GetPagedQuery(totalNoticesQuery, options);

			return new QueryResult<NoticeData>(
				await pagedNoticesQuery.ToListAsync(),
				options?.Skip ?? 0,
				await totalNoticesQuery.CountAsync()
			);
		}

		public async Task<PostData> GetPost(Guid postId) {
			var postQuery = _context.Posts
				.Where(p => p.Uuid == postId)
				.ProjectTo<PostData>();

			return await postQuery.FirstOrDefaultAsync();
		}

		public async Task<QueryResult<PostData>> GetPosts(Guid threadId, QueryOptions<PostData> options = null) {
			var totalPostsQuery = (
				from p in _context.Posts
				join u in _context.Users
					on p.CreatedById equals u.Uuid
				where p.ThreadId == threadId
				select p
			).ProjectTo<PostData>();

			var pagedPostsQuery = GetPagedQuery(totalPostsQuery, options);

			return new QueryResult<PostData>(
				await pagedPostsQuery.ToListAsync(),
				options?.Skip ?? 0,
				await totalPostsQuery.CountAsync()
			);
		}

		public async Task<ReprimandData> GetReprimand(Guid reprimandId) {
			var reprimandQuery = _context.Reprimands
				.Where(r => r.Uuid == reprimandId)
				.ProjectTo<ReprimandData>();

			return await reprimandQuery.FirstOrDefaultAsync();
		}

		public async Task<QueryResult<ReprimandData>> GetReprimands(Guid userId, QueryOptions<ReprimandData> options = null) {
			var totalProbationsQuery = _context.Reprimands
				.Where(r => r.AssignedToId == userId)
				.ProjectTo<ReprimandData>();

			var pagedProbationsQuery = GetPagedQuery(totalProbationsQuery, options);

			return new QueryResult<ReprimandData>(
				await pagedProbationsQuery.ToListAsync(),
				options?.Skip ?? 0,
				await totalProbationsQuery.CountAsync()
			);
		}

		public async Task<ThreadData> GetThread(Guid threadId) {
			var threadQuery = (
				from t in _context.Threads
				join u in _context.Users
					on t.CreatedById equals u.Uuid
				join lp in GetLastPost(threadId)
					on t.Uuid equals lp.ThreadId
				where t.Uuid == threadId
				select new ThreadModelAggregate {
					Thread = t,
					LastPost = lp,
					PostCount = t.Posts.Count()
				}
			).ProjectTo<ThreadData>();

			return await threadQuery.FirstOrDefaultAsync();
		}

		public async Task<QueryResult<ThreadData>> GetThreads(Guid forumId, QueryOptions<ThreadData> options = null) {
			var lastPosts = await (
				from p in _context.Posts
				join t in _context.Threads
					on p.ThreadId equals t.Uuid
				where t.ForumId == forumId
				group p by p.ThreadId into pg1
				from pg2 in (
					from spg in pg1
					group spg by spg.CreatedOn into spgf
					orderby spgf.Key descending
					select spgf
				)
				group pg2 by pg1.Key into r1
				select r1.FirstOrDefault() into r2
				select r2.FirstOrDefault()
			).ToListAsync();

			/* TODO: Figure out how to condense this down into one query */
			var totalThreadsQuery = (
				from t in _context.Threads
				join p in lastPosts
					on t.Uuid equals p.ThreadId
				select new ThreadModelAggregate {
					Thread = t,
					LastPost = p,
					PostCount = t.Posts.Count()
				}
			).ProjectTo<ThreadData>();

			var pagedThreadsQuery = GetPagedQuery(totalThreadsQuery, options);

			return new QueryResult<ThreadData>(
				await pagedThreadsQuery.ToListAsync(),
				options?.Skip ?? 0,
				await pagedThreadsQuery.CountAsync()
			);
		}

		public async Task<UserData> GetUser(Guid userId) {
			var userQuery = _context.Users
				.Where(u => u.Uuid == userId)
				.ProjectTo<UserData>();

			return await userQuery.FirstOrDefaultAsync();
		}

		public async Task<QueryResult<UserData>> GetUsers(QueryOptions<UserData> options = null) {
			var totalUsersQuery = _context.Users
				.ProjectTo<UserData>();

			var pagedUsersQuery = GetPagedQuery(totalUsersQuery, options);

			return new QueryResult<UserData>(
				await pagedUsersQuery.ToListAsync(),
				options?.Skip ?? 0,
				await pagedUsersQuery.CountAsync()
			);
		}

		/* ----------------------------------- Create ----------------------------------- */
		public async Task<ForumData> CreateForum(ForumCreateData data) {
			var newForumEntry = _context.Forums.Add(new ForumModel {
				CreatedOn = DateTime.Now,
				Description = data.Description,
				IconUrl = data.IconUrl,
				Name = data.Name,
				CreatedById = data.CreatedById
			});
			await _context.SaveChangesAsync();

			return await GetForum(newForumEntry.Entity.Uuid);
		}

		public async Task<NoticeData> CreateNotice(NoticeCreateData data) {
			ForumModel forum = null;
			if (data.ForumId != null) {
				forum = await _context.Forums
					.Where(f => f.Uuid == data.ForumId)
					.FirstOrDefaultAsync();

				if (forum == null)
					throw new ArgumentException($"No forum found with the id \"{data.ForumId}\"");
			}

			var newNoticeEntry = _context.Notices.Add(new NoticeModel {
				Content = data.Content,
				CreatedOn = DateTime.Now,
				IconUrl = data.IconUrl,
				Name = data.Name,
				CreatedById = data.CreatedById,
				ForumId = forum?.Uuid
			});
			await _context.SaveChangesAsync();

			return await GetNotice(newNoticeEntry.Entity.Uuid);
		}

		public async Task<PostData> CreatePost(PostCreateData data) {
			var thread = await _context.Threads
				.Where(t => t.Uuid == data.ThreadId)
				.FirstOrDefaultAsync();
			if (thread == null)
				throw new ArgumentException($"No thread found with the id \"{data.ThreadId}\"");

			var newPostEntry = _context.Posts.Add(new PostModel {
				Content = data.Content,
				CreatedOn = DateTime.Now,
				CreatedById = data.CreatedById,
				ThreadId = thread.Uuid
			});
			await _context.SaveChangesAsync();

			return await GetPost(newPostEntry.Entity.Uuid);
		}

		public async Task<ThreadData> CreateThread(ThreadCreateData data) {
			var forum = await _context.Forums
				.Where(f => f.Uuid == data.ForumId)
				.FirstOrDefaultAsync();
			if (forum == null)
				throw new ArgumentException($"No forum found with the id \"{data.ForumId}\"");

			var newThreadEntry = _context.Threads.Add(new ThreadModel {
				CreatedOn = DateTime.Now,
				IconUrl = data.IconUrl,
				Name = data.Name,
				CreatedById = data.CreatedById,
				ForumId = forum.Uuid
			});
			await _context.SaveChangesAsync();

			_context.Posts.Add(new PostModel {
				Content = data.Content,
				CreatedOn = DateTime.Now,
				CreatedById = data.CreatedById,
				ThreadId = newThreadEntry.Entity.Uuid
			});
			await _context.SaveChangesAsync();

			return await GetThread(newThreadEntry.Entity.Uuid);
		}

		public async Task<ReprimandData> ReprimandUser(ReprimandCreateData data) {
			if (data.Type == ReprimandType.Probation && data.ExpiresOn == null)
				throw new ArgumentNullException("Probations must have an expiration date");

			var assignedOn = DateTime.Now;
			if (data.ExpiresOn != null && data.ExpiresOn >= assignedOn)
				throw new ArgumentException("Probation expiration dates must have a value greater than now");

			var targetUser = await _context.Users
				.Where(u => u.Uuid == data.AssignedToId)
				.FirstOrDefaultAsync();
			if (targetUser == null)
				throw new ArgumentException($"No user found with the id \"{data.AssignedToId}\"");

			ForumModel forum = null;
			if (data.ForumId != null) {
				await (
					from f in _context.Forums
					where f.Uuid == data.ForumId
					select f
				).FirstOrDefaultAsync();

				if (forum == null)
					throw new ArgumentException("No forum found with the id \"{data.AssignedToId}\"");
			}

			var newReprimandEntry = _context.Reprimands.Add(new ReprimandModel {
				AssignedOn = assignedOn,
				ExpiresOn = data.ExpiresOn,
				Reason = data.Reason,
				Type = data.Type,
				AssignedById = data.AssignedById,
				AssignedToId = targetUser.Uuid,
				ForumId = forum?.Uuid
			});
			await _context.SaveChangesAsync();

			return await GetReprimand(newReprimandEntry.Entity.Uuid);
		}

		/* ----------------------------------- Update ----------------------------------- */
		public async Task<ForumData> UpdateForum(ForumUpdateData data) {
			var forum = await _context.Forums
				.Where(f => f.Uuid == data.Id)
				.FirstOrDefaultAsync();
			if (forum == null)
				throw new ArgumentException($"No forum found with the id \"{data.Id}\"");

			var forumUpdated = false;
			if (!string.IsNullOrWhiteSpace(data.Name)) {
				forum.Name = data.Name;
				forumUpdated = true;
			}

			if (!string.IsNullOrWhiteSpace(data.Description)) {
				forum.Description = data.Description;
				forumUpdated = true;
			}

			if (!string.IsNullOrWhiteSpace(data.IconUrl)) {
				forum.IconUrl = data.IconUrl;
				forumUpdated = true;
			}

			if (forumUpdated)
				forum.UpdatedOn = DateTime.Now;

			_context.Forums.Update(forum);
			await _context.SaveChangesAsync();

			return Mapper.Instance.Map<ForumData>(forum);
		}

		public async Task<NoticeData> UpdateNotice(NoticeUpdateData data) {
			var notice = await _context.Notices
				.Where(n => n.Uuid == data.Id)
				.FirstOrDefaultAsync();
			if (notice == null)
				throw new ArgumentException($"No notice found with the id \"{data.Id}\"");

			var noticeUpdated = false;
			if (!string.IsNullOrWhiteSpace(data.Content)) {
				notice.Content = data.Content;
				noticeUpdated = true;
			}

			if (!string.IsNullOrWhiteSpace(data.Name)) {
				notice.Name = data.Name;
				noticeUpdated = true;
			}

			if (noticeUpdated)
				notice.UpdatedOn = DateTime.Now;

			_context.Notices.Update(notice);
			await _context.SaveChangesAsync();

			return Mapper.Instance.Map<NoticeData>(notice);
		}

		public async Task<PostData> UpdatePost(PostUpdateData data) {
			var post = await _context.Posts
				.Where(p => p.Uuid == data.Id)
				.FirstOrDefaultAsync();
			if (post == null)
				throw new ArgumentException($"No post found with the id \"{data.Id}\"");

			var postUpdated = false;
			if (!string.IsNullOrWhiteSpace(data.Content)) {
				post.Content = data.Content;
				postUpdated = true;
			}

			if (postUpdated)
				post.UpdatedOn = DateTime.Now;

			_context.Posts.Update(post);
			await _context.SaveChangesAsync();

			return Mapper.Instance.Map<PostData>(post);
		}

		public async Task<ThreadData> UpdateThread(ThreadUpdateData data) {
			var thread = await _context.Threads
				.Where(t => t.Uuid == data.Id)
				.FirstOrDefaultAsync();
			if (thread == null)
				throw new ArgumentException($"No thread found with the id \"{data.Id}\"");

			var threadUpdated = false;
			if (!string.IsNullOrWhiteSpace(data.IconUrl)) {
				thread.IconUrl = data.IconUrl;
				threadUpdated = true;
			}

			if (!string.IsNullOrWhiteSpace(data.Name)) {
				thread.Name = data.Name;
				threadUpdated = true;
			}

			if (threadUpdated)
				thread.UpdatedOn = DateTime.Now;

			_context.Threads.Update(thread);
			await _context.SaveChangesAsync();

			return Mapper.Instance.Map<ThreadData>(thread);
		}

		/* ----------------------------------- Delete ----------------------------------- */
		public async Task DeleteForum(Guid forumId) {
			var forum = await _context.Forums
				.Where(f => f.Uuid == forumId)
				.FirstOrDefaultAsync();
			if (forum == null)
				throw new ArgumentException($"No forum found with the id \"{forumId}\"");

			forum.Delete();
			_context.Forums.Update(forum);

			var notices = await _context.Notices
				.Where(n => n.ForumId == forumId)
				.ToListAsync();
			foreach (var notice in notices) {
				notice.Delete();
				_context.Notices.Update(notice);
			}

			var threads = await _context.Threads
				.Where(t => t.ForumId == forumId)
				.ToListAsync();
			foreach (var thread in threads) {
				thread.Delete();
				_context.Threads.Update(thread);
			}

			var posts = await (
				from p in _context.Posts
				join t in _context.Threads
					on p.ThreadId equals t.Uuid
				join f in _context.Forums
					on t.ForumId equals f.Uuid
				where f.Uuid == forumId
				select p
			).ToListAsync();
			foreach (var post in posts) {
				post.Delete();
				_context.Posts.Update(post);
			}

			var favorites = await _context.FavoritedItems
				.Where(fi => (
					fi.Type == FavoritedItemType.Forum
					&& fi.TargetId == forumId
				))
				.ToListAsync();
			foreach (var favorite in favorites) {
				favorite.Delete();
				_context.FavoritedItems.Update(favorite);
			}

			await _context.SaveChangesAsync();
		}

		public async Task DeleteNotice(Guid noticeId) {
			var notice = await _context.Notices
				.Where(n => n.Uuid == noticeId)
				.FirstOrDefaultAsync();
			if (notice == null)
				throw new ArgumentException($"No notice found with the id \"{noticeId}\"");

			notice.Delete();
			_context.Notices.Update(notice);
			await _context.SaveChangesAsync();
		}

		public async Task DeletePost(Guid postId) {
			var post = await _context.Posts
				.Where(p => p.Uuid == postId)
				.FirstOrDefaultAsync();
			if (post == null)
				throw new ArgumentException($"No post found with the id \"{postId}\"");

			post.Delete();
			_context.Posts.Update(post);
			await _context.SaveChangesAsync();
		}

		public async Task DeleteThread(Guid threadId) {
			var thread = await _context.Threads
				.Where(t => t.Uuid == threadId)
				.FirstOrDefaultAsync();
			if (thread == null)
				throw new ArgumentException($"No thread found with the id \"{threadId}\"");

			thread.Delete();
			_context.Threads.Update(thread);

			var posts = await _context.Posts
				.Where(p => p.ThreadId == threadId)
				.ToListAsync();
			foreach (var post in posts) {
				post.Delete();
				_context.Posts.Update(post);
			}

			await _context.SaveChangesAsync();
		}

		/* ------------------------------- Common Queries ------------------------------- */
		private IQueryable<PostModel> GetLastPost(Guid threadId) => (
			from p in _context.Posts
			join t in _context.Threads
				on p.ThreadId equals t.Uuid
			join u in _context.Users
				on p.CreatedById equals u.Uuid
			where t.Uuid == threadId
			group p by p.CreatedOn into pg
			orderby pg.Key descending
			select pg.FirstOrDefault()
		);

		/* ----------------------------------- Utilities ----------------------------------- */
		private IQueryable<ResultType> GetPagedQuery<ResultType>(IQueryable<ResultType> queryable, QueryOptions<ResultType> options) {
			var pagedQuery = queryable;
			if (options != null) {
				if (options.Extension != null) {
					pagedQuery = options.Extension(pagedQuery);
				}

				var skipAmount = options?.Skip;
				if (skipAmount != null) {
					pagedQuery = pagedQuery.Skip((int) skipAmount);
				}

				var takeAmount = options?.Take;
				if (takeAmount != null) {
					pagedQuery = pagedQuery.Take((int) takeAmount);
				}
			}

			return pagedQuery;
		}
	}

	internal class ForumModelAggregate {
		public ForumModel Forum { get; set; }
		public int ThreadCount { get; set; }
	}

	internal class ThreadModelAggregate {
		public ThreadModel Thread { get; set; }
		public PostModel LastPost { get; set; }
		public int PostCount { get; set; }
	}
}
