﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SKM.Data.Models;

namespace SKM.Data {
	public class SKMContext : IdentityDbContext<ApplicationUser, IdentityRole<int>, int> {
		internal virtual DbSet<FavoritedItemModel> FavoritedItems { get; set; }
		internal virtual DbSet<ForumModel> Forums { get; set; }
		internal virtual DbSet<NoticeModel> Notices { get; set; }
		internal virtual DbSet<PostModel> Posts { get; set; }
		internal virtual DbSet<ReprimandModel> Reprimands { get; set; }
		internal virtual DbSet<ThreadModel> Threads { get; set; }

		public SKMContext(DbContextOptions<SKMContext> options) : base(options) { }

		protected override void OnModelCreating(ModelBuilder modelBuilder) {
			base.OnModelCreating(modelBuilder);

			ApplyIdentityOverrides(modelBuilder);
			modelBuilder.Entity<ApplicationUser>(ApplicationUser.ApplyAttributes);

			modelBuilder.Entity<FavoritedItemModel>(FavoritedItemModel.ApplyAttributes);
			modelBuilder.Entity<ForumModel>(ForumModel.ApplyAttributes);
			modelBuilder.Entity<NoticeModel>(NoticeModel.ApplyAttributes);
			modelBuilder.Entity<PostModel>(PostModel.ApplyAttributes);
			modelBuilder.Entity<ReprimandModel>(ReprimandModel.ApplyAttributes);
			modelBuilder.Entity<ThreadModel>(ThreadModel.ApplyAttributes);
		}

		private void ApplyIdentityOverrides(ModelBuilder modelBuilder) {
			modelBuilder.Entity<ApplicationUser>(e => e.ToTable("Users"));
			modelBuilder.Entity<IdentityRole<int>>(e => e.ToTable("SiteRoles"));
			modelBuilder.Entity<IdentityRoleClaim<int>>(e => e.ToTable("SiteRoleClaims"));
			modelBuilder.Entity<IdentityUserClaim<int>>(e => e.ToTable("UserClaims"));
			modelBuilder.Entity<IdentityUserLogin<int>>(e => e.ToTable("UserLogins"));
			modelBuilder.Entity<IdentityUserRole<int>>(e => e.ToTable("UserRoles"));
			modelBuilder.Entity<IdentityUserToken<int>>(e => e.ToTable("UserTokens"));
		}
	}
}
