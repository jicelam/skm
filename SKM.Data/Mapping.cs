﻿using AutoMapper;
using SKM.Data.Models;
using SKM.Data.Services;
using SKM.Models.Data;
using SKM.Models.Data.Create;
using SKM.Models.Data.Update;
using SKM.Models.Security;
using System.Linq;

namespace SKM.Data {
	public class Mapper {
		private static Mapper _instance;
		public static Mapper Instance {
			get {
				if (_instance == null)
					_instance = new Mapper();

				return _instance;
			}
		}

		private readonly IMapper _mapper;

		private Mapper() {
			var config = new MapperConfiguration(configOptions => {
				/* Data Mappings */
				configOptions.CreateMap<FavoritedItemModel, FavoritedItemData>()
					.ForMember(e => e.Id, o => o.MapFrom(d => d.Uuid));

				configOptions.CreateMap<ForumModel, ForumData>()
					.ForMember(e => e.Id, o => o.MapFrom(d => d.Uuid));
				configOptions.CreateMap<ForumModelAggregate, ForumData>()
					.IncludeMembers(d => d.Forum);

				configOptions.CreateMap<NoticeModel, NoticeData>()
					.ForMember(e => e.Id, o => o.MapFrom(d => d.Uuid));

				configOptions.CreateMap<PostModel, PostData>()
					.ForMember(e => e.Id, o => o.MapFrom(d => d.Uuid));

				configOptions.CreateMap<ReprimandModel, ReprimandData>()
					.ForMember(e => e.Id, o => o.MapFrom(d => d.Uuid));

				configOptions.CreateMap<ThreadModel, ThreadData>()
					.ForMember(e => e.Id, o => o.MapFrom(d => d.Uuid));
				configOptions.CreateMap<ThreadModelAggregate, ThreadData>()
					.IncludeMembers(d => d.Thread)
					.ForMember(e => e.LastPostBy, o => o.MapFrom(d => d.LastPost.CreatedBy))
					.ForMember(e => e.LastPostOn, o => o.MapFrom(d => d.LastPost.CreatedOn));

				configOptions.CreateMap<ApplicationUser, UserData>()
					.ForMember(e => e.Id, o => o.MapFrom(d => d.Uuid))
					.ForMember(e => e.Name, o => o.MapFrom(d => d.UserName));
				configOptions.CreateMap<ApplicationUser, UserSummaryData>()
					.ForMember(e => e.Id, o => o.MapFrom(d => d.Uuid))
					.ForMember(e => e.Name, o => o.MapFrom(d => d.UserName));

				configOptions.CreateMap<ForumCreateData, ForumModel>();
				configOptions.CreateMap<PostCreateData, PostModel>();
				configOptions.CreateMap<ReprimandCreateData, ReprimandModel>();
				configOptions.CreateMap<ThreadCreateData, ThreadModel>();

				configOptions.CreateMap<ForumUpdateData, ForumModel>()
					.ForMember(e => e.Uuid, o => o.MapFrom(d => d.Id));
				configOptions.CreateMap<PostUpdateData, PostModel>()
					.ForMember(e => e.Uuid, o => o.MapFrom(d => d.Id));
				configOptions.CreateMap<ThreadUpdateData, ThreadModel>()
					.ForMember(e => e.Uuid, o => o.MapFrom(d => d.Id));

				/* Security Mappings */
				configOptions.CreateMap<ApplicationUser, User>()
					.ForMember(e => e.Id, o => o.MapFrom(d => d.Uuid));
			});

			_mapper = config.CreateMapper();
		}

		public DestType Map<DestType>(object source) {
			return _mapper.Map<DestType>(source);
		}

		public IQueryable<DestType> ProjectTo<DestType>(IQueryable queryable) {
			return _mapper.ProjectTo<DestType>(queryable);
		}
	}

	internal static class MappingQueryExtensions {
		public static IQueryable<DestType> ProjectTo<DestType>(this IQueryable queryable) {
			return Mapper.Instance.ProjectTo<DestType>(queryable);
		}
	}
}
