﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using Microsoft.Extensions.DependencyInjection;
using SKM.Data.Models;
using SKM.Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SKM.Data {
	public class DataSeedService : IDisposable {
		public UserData CurrentUser { get => _context.Users.ProjectTo<UserData>().FirstOrDefault(); }

		public IReadOnlyList<FavoritedItemData> FavoritedItems { get => _context.FavoritedItems.ProjectTo<FavoritedItemData>().ToList(); }
		public IReadOnlyList<ForumData> Forums { get => _context.Forums.ProjectTo<ForumData>().ToList(); }
		public IReadOnlyList<NoticeData> Notices { get => _context.Notices.ProjectTo<NoticeData>().ToList(); }
		public IReadOnlyList<PostData> Posts { get => _context.Posts.ProjectTo<PostData>().ToList(); }
		public IReadOnlyList<ReprimandData> Reprimands { get => _context.Reprimands.ProjectTo<ReprimandData>().ToList(); }
		public IReadOnlyList<ThreadData> Threads { get => _context.Threads.ProjectTo<ThreadData>().ToList(); }
		public IReadOnlyList<UserData> Users { get => _context.Users.ProjectTo<UserData>().ToList(); }

		public bool Testing { get; set; }

		private readonly SKMContext _context;
		private readonly UserManager<ApplicationUser> _userManager;
		private readonly MockText _mockText;

		public DataSeedService(
			SKMContext context,
			UserManager<ApplicationUser> userManager
		) {
			_context = context;
			_userManager = userManager;
			_mockText = new MockText();
		}

		public void Dispose() {
			_context?.Dispose();
		}

		public async Task Seed() {
			if (!Testing) {
				var databaseExists = (_context.Database.GetService<IDatabaseCreator>() as RelationalDatabaseCreator).Exists();
				await _context.Database.MigrateAsync();
				if (databaseExists)
					return;
			}

			await CreateUsers();
			await CreateForums();
			await CreateNotices();
			await CreateThreads();
			await CreatePosts();
			await CreateReprimands();
			await CreateFavoritedItems();
		}

		private async Task CreateUsers() {
			var testUsers = Enumerable.Repeat(0, 8).Select((_, index) => new ApplicationUser {
				AvatarUrl = null,
				Caption = "Test User",
				Email = $"test.user.{index}@fake.email.com",
				JoinedOn = DateTime.Now,
				Location = null,
				Signature = null,
				UserName = $"User {index}"
			});

			if (Testing) {
				_context.Users.AddRange(testUsers);

				await _context.SaveChangesAsync();

				return;
			}

			foreach (var testUser in testUsers) {
				var result = await _userManager.CreateAsync(testUser, "T3st_p@ssword");

				if (result != IdentityResult.Success) {
					throw new Exception($"Unable to create the user \"{testUser.UserName}\"");
				}
			}
		}

		private async Task CreateForums() {
			var users = await _context.Users.ToListAsync();

			_context.Forums.AddRange(
				new List<ForumModel> {
					new ForumModel {
						CreatedOn = DateTime.Now,
						Description = _mockText.GetString(1),
						IconUrl = null,
						Name = "Forum 1",
						CreatedById = users[0].Uuid
					},
					new ForumModel {
						CreatedOn = DateTime.Now,
						Description = _mockText.GetString(1),
						IconUrl = null,
						Name = "Forum 2",
						CreatedById = users[1].Uuid
					},
					new ForumModel {
						CreatedOn = DateTime.Now,
						Description = _mockText.GetString(1),
						IconUrl = null,
						Name = "Forum 3",
						CreatedById = users[2].Uuid
					},
					new ForumModel {
						CreatedOn = DateTime.Now,
						Description = _mockText.GetString(1),
						IconUrl = null,
						Name = "Forum 4",
						CreatedById = users[1].Uuid
					},
					new ForumModel {
						CreatedOn = DateTime.Now,
						Description = _mockText.GetString(1),
						IconUrl = null,
						Name = "Forum 5",
						CreatedById = users[1].Uuid
					}
				}
			);

			await _context.SaveChangesAsync();
		}

		private async Task CreateNotices() {
			var forums = await _context.Forums.ToListAsync();
			var users = await _context.Users.ToListAsync();

			_context.Notices.AddRange(
				new List<NoticeModel> {
					new NoticeModel {
						Content = _mockText.GetString(),
						CreatedOn = DateTime.Now,
						IconUrl = null,
						Name = _mockText.GetString(1),
						CreatedById = users[0].Uuid,
						ForumId = null
					},
					new NoticeModel {
						Content = _mockText.GetString(),
						CreatedOn = DateTime.Now,
						IconUrl = null,
						Name = _mockText.GetString(1),
						CreatedById = users[1].Uuid,
						ForumId = null
					}
				}
			);

			var userIndexes = new[] { 0, 1, 0, 1, 1 };
			var noticeCount = new[] { 0, 1, 1, 1, 2 };
			for (var i = 0; i < forums.Count; i++) {
				var forum = forums[i];

				_context.Notices.AddRange(
					Enumerable.Repeat(0, noticeCount[i % noticeCount.Length])
						.Select((_, j) => new NoticeModel {
							Content = _mockText.GetString(),
							CreatedOn = DateTime.Now,
							IconUrl = null,
							Name = _mockText.GetString(1),
							CreatedById = users[userIndexes[j % userIndexes.Length]].Uuid,
							ForumId = forum.Uuid
						})
				);
			}

			await _context.SaveChangesAsync();
		}

		private async Task CreateThreads() {
			var forums = await _context.Forums.ToListAsync();
			var users = await _context.Users.ToListAsync();

			var threadCounts = new[] { 13, 9, 8, 16, 4, 7 };
			var userIndexes = new[] { 5, 1, 3, 5, 0, 1 };
			for (var i = 0; i < forums.Count; i++) {
				var forum = forums[i];

				_context.Threads.AddRange(
					Enumerable.Repeat(0, threadCounts[i % threadCounts.Length])
						.Select((_, j) => new ThreadModel {
							CreatedOn = DateTime.Now,
							IconUrl = null,
							Name = _mockText.GetString(1),
							CreatedById = users[userIndexes[j % userIndexes.Length]].Uuid,
							ForumId = forum.Uuid
						})
				);
			}

			await _context.SaveChangesAsync();
		}

		private async Task CreatePosts() {
			var threads = await _context.Threads.ToListAsync();
			var users = await _context.Users.ToListAsync();

			var postCounts = new[] { 10, 8, 3, 13, 21, 18, 5, 15, 7 };
			var userIndexes = new[] { 3, 0, 5, 1, 1, 2, 4 };
			for (var i = 0; i < threads.Count; i++) {
				var thread = threads[i];

				_context.Posts.AddRange(
					Enumerable.Repeat(0, postCounts[i % postCounts.Length])
						.Select((_, j) => new PostModel {
							Content = _mockText.GetString(),
							CreatedOn = DateTime.Now,
							UpdatedOn = null,
							CreatedById = users[userIndexes[j % userIndexes.Length]].Uuid,
							ThreadId = thread.Uuid
						})
				);
			}

			await _context.SaveChangesAsync();
		}

		private async Task CreateReprimands() {
			var forums = await _context.Forums.ToListAsync();
			var users = await _context.Users.ToListAsync();

			_context.Reprimands.AddRange(
				new List<ReprimandModel> {
					new ReprimandModel {
						AssignedOn = DateTime.Now,
						Reason = _mockText.GetString(),
						Type = ReprimandType.Ban,
						AssignedById = users[0].Uuid,
						AssignedToId = users[6].Uuid,
						ForumId = forums[1].Uuid
					},
					new ReprimandModel {
						AssignedOn = DateTime.Now,
						ExpiresOn = DateTime.Now.AddDays(1),
						Reason = _mockText.GetString(),
						Type = ReprimandType.Probation,
						AssignedById = users[0].Uuid,
						AssignedToId = users[7].Uuid,
						ForumId = forums[2].Uuid
					},
					new ReprimandModel {
						AssignedOn = DateTime.Now,
						ExpiresOn = DateTime.Now.AddDays(1),
						Reason = _mockText.GetString(),
						Type = ReprimandType.Probation,
						AssignedById = users[1].Uuid,
						AssignedToId = users[5].Uuid,
						ForumId = forums[2].Uuid
					}
				}
			);

			await _context.SaveChangesAsync();
		}

		private async Task CreateFavoritedItems() {
			var forums = await _context.Forums.ToListAsync();
			var threads = await _context.Threads.ToListAsync();
			var users = await _context.Users.ToListAsync();

			_context.FavoritedItems.AddRange(
				new List<FavoritedItemModel> {
					new FavoritedItemModel {
						TargetId = forums[3].Uuid,
						Type = FavoritedItemType.Forum,
						UserId = users[1].Uuid
					},
					new FavoritedItemModel {
						TargetId = threads[6].Uuid,
						Type = FavoritedItemType.Thread,
						UserId = users[3].Uuid
					}
				}
			);

			await _context.SaveChangesAsync();
		}

		private class MockText {
			private readonly string[] _textSample = @"
				Lorem ipsum dolor sit amet, saepe cetero in ius, cum no quod tantas populo. His et saepe bonorum cotidieque,
				has numquam cotidieque te, ea quas lucilius posidonium pri. Facer mucius vim ne, eos reque commodo adipisci no,
				tota antiopam mandamus ex eos. Dicam consequat constituam ex has, at vel dicat quando deserunt. Pro dicam putent
				nominati et, per torquatos deseruisse an. Ea alia nulla persequeris pri. Mea ad primis consulatu, justo indoctum
				instructior usu no. Sed inani dissentiet voluptatibus ad, in graece possit verterem mea. Qui in numquam vocibus
				ponderum, cum dolor vivendo complectitur et. Eum ad quis mutat corpora. Mel te quis posidonium, duo natum democritum
				ex, an quo oratio patrioque. Partem epicurei interesset eum te. Te eam harum democritum, erant cotidieque sea id, sit
				nibh albucius ut. Viderer pericula consectetuer in vis. Ea mel utamur ornatus lobortis. Eum natum putant ex, alii minim
				audire ut vix. Nam quas iuvaret intellegam ex, duo agam propriae adversarium eu, commodo fastidii et qui. His at
				platonem patrioque appellantur. Cu solet aliquam dissentiet vel, ad soleat imperdiet definiebas cum. Tempor numquam
				percipit his ne. Qui aperiam discere liberavisse an, cu vulputate conceptam eum. Usu harum labitur comprehensam et,
				eam at nusquam aliquando, et vix legere sadipscing reprehendunt. Ad dicit partem oportere vim. Tota torquatos cu mei,
				vis sint omnium ut. Id mea tibique electram adversarium
			".Replace("\t", " ")
			.Replace("\n", "")
			.Replace("\r", "")
			.Split(new[] { ". " }, StringSplitOptions.RemoveEmptyEntries);

			private int _startingIndex = 0;
			private int _sentenceCountIndex = 0;
			private readonly int[] _sentenceTotals = new[] { 3, 2, 4, 1, 4, 2 };

			public string GetString(int? sentenceCount = null) {
				if (sentenceCount == null) {
					sentenceCount = _sentenceTotals[_sentenceCountIndex % _sentenceTotals.Length];
					_sentenceCountIndex += 1;
				}

				var textBuilder = new StringBuilder();
				for (var i = 0; i < sentenceCount; i++) {
					var textCoda = i < sentenceCount - 1
						? ". "
						: "";

					textBuilder.Append($"{_textSample[(i + _startingIndex) % _textSample.Length]}{textCoda}");
				}

				_startingIndex += (int) sentenceCount;

				return textBuilder.ToString();
			}
		}
	}
}
