﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SKM.Models.Data;
using System;

namespace SKM.Data.Models {
	internal class FavoritedItemModel : ModelBase {
		public static void ApplyAttributes(EntityTypeBuilder<FavoritedItemModel> builder) {
			ModelBase.ApplyAttributes(builder);

			builder.HasIndex(e => e.TargetId);
			builder.HasIndex(e => e.Type);

			builder.HasOne(e => e.User)
				.WithMany(o => o.Favorites)
				.HasForeignKey(e => e.UserId)
				.HasPrincipalKey(o => o.Uuid)
				.OnDelete(DeleteBehavior.Restrict);
		}

		public Guid TargetId { get; set; }
		public FavoritedItemType Type { get; set; }

		public Guid UserId { get; set; }
		public ApplicationUser User { get; set; }
	}
}
