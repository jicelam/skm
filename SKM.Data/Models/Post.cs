﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.ComponentModel.DataAnnotations;

namespace SKM.Data.Models {
	internal class PostModel : ModelBase {
		public static void ApplyAttributes(EntityTypeBuilder<PostModel> builder) {
			ModelBase.ApplyAttributes(builder);

			builder.HasIndex(e => e.CreatedOn);

			builder.HasOne(e => e.CreatedBy)
				.WithMany(o => o.PostsCreated)
				.HasForeignKey(e => e.CreatedById)
				.HasPrincipalKey(o => o.Uuid)
				.OnDelete(DeleteBehavior.Restrict);

			builder.HasOne(e => e.UpdatedBy)
				.WithMany(o => o.PostsUpdated)
				.HasForeignKey(e => e.UpdatedById)
				.HasPrincipalKey(o => o.Uuid)
				.OnDelete(DeleteBehavior.Restrict);

			builder.HasOne(e => e.Thread)
				.WithMany(o => o.Posts)
				.HasForeignKey(e => e.ThreadId)
				.HasPrincipalKey(o => o.Uuid)
				.OnDelete(DeleteBehavior.Restrict);
		}

		[DataType(DataType.MultilineText)]
		[MinLength(1)]
		[Required]
		public string Content { get; set; }

		[DataType(DataType.DateTime)]
		public DateTime CreatedOn { get; set; }

		[DataType(DataType.DateTime)]
		public DateTime? UpdatedOn { get; set; }

		public Guid CreatedById { get; set; }
		public ApplicationUser CreatedBy { get; set; }

		public Guid? UpdatedById { get; set; }
		public ApplicationUser UpdatedBy { get; set; }

		public Guid ThreadId { get; set; }
		public ThreadModel Thread { get; set; }
	}
}
