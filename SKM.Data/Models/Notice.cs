﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.ComponentModel.DataAnnotations;

namespace SKM.Data.Models {
	internal class NoticeModel : ModelBase {
		public static void ApplyAttributes(EntityTypeBuilder<NoticeModel> builder) {
			ModelBase.ApplyAttributes(builder);

			builder.HasIndex(e => e.CreatedOn);

			builder.HasOne(e => e.CreatedBy)
				.WithMany(o => o.NoticesCreated)
				.HasForeignKey(e => e.CreatedById)
				.HasPrincipalKey(o => o.Uuid)
				.OnDelete(DeleteBehavior.Restrict);

			builder.HasOne(e => e.Forum)
				.WithMany(o => o.Notices)
				.HasForeignKey(e => e.ForumId)
				.HasPrincipalKey(o => o.Uuid)
				.OnDelete(DeleteBehavior.Restrict);
		}

		[DataType(DataType.MultilineText)]
		[MinLength(1)]
		[Required]
		public string Content { get; set; }

		[DataType(DataType.DateTime)]
		public DateTime CreatedOn { get; set; }

		[DataType(DataType.ImageUrl)]
		[StringLength(1_000)]
		public string IconUrl { get; set; }

		[Required]
		[StringLength(250, MinimumLength = 1)]
		public string Name { get; set; }

		[DataType(DataType.DateTime)]
		public DateTime? UpdatedOn { get; set; }

		public Guid CreatedById { get; set; }
		public ApplicationUser CreatedBy { get; set; }

		public Guid? ForumId { get; set; }
		public ForumModel Forum { get; set; }
	}
}
