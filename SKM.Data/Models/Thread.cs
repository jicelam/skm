﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SKM.Data.Models {
	internal class ThreadModel : ModelBase {
		public static void ApplyAttributes(EntityTypeBuilder<ThreadModel> builder) {
			ModelBase.ApplyAttributes(builder);

			builder.HasIndex(e => e.CreatedOn);

			builder.HasOne(e => e.CreatedBy)
				.WithMany(o => o.ThreadsCreated)
				.HasForeignKey(e => e.CreatedById)
				.HasPrincipalKey(o => o.Uuid)
				.OnDelete(DeleteBehavior.Restrict);

			builder.HasOne(e => e.Forum)
				.WithMany(o => o.Threads)
				.HasForeignKey(e => e.ForumId)
				.HasPrincipalKey(o => o.Uuid)
				.OnDelete(DeleteBehavior.Restrict);
		}

		[DataType(DataType.DateTime)]
		public DateTime CreatedOn { get; set; }

		[DataType(DataType.ImageUrl)]
		[StringLength(1_000)]
		public string IconUrl { get; set; }

		[Required]
		[StringLength(250, MinimumLength = 1)]
		public string Name { get; set; }

		[DataType(DataType.DateTime)]
		public DateTime? UpdatedOn { get; set; }

		public Guid CreatedById { get; set; }
		public ApplicationUser CreatedBy { get; set; }

		public Guid ForumId { get; set; }
		public ForumModel Forum { get; set; }

		public ICollection<PostModel> Posts { get; set; }
	}
}
