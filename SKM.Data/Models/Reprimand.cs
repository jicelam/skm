﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SKM.Models.Data;
using System;
using System.ComponentModel.DataAnnotations;

namespace SKM.Data.Models {
	internal class ReprimandModel : ModelBase {
		public static void ApplyAttributes(EntityTypeBuilder<ReprimandModel> builder) {
			ModelBase.ApplyAttributes(builder);

			builder.HasIndex(e => e.AssignedOn);
			builder.HasIndex(e => e.ExpiresOn);
			builder.HasIndex(e => e.Type);

			builder.HasOne(e => e.AssignedBy)
				.WithMany(o => o.ReprimandsCreated)
				.HasForeignKey(e => e.AssignedById)
				.HasPrincipalKey(o => o.Uuid)
				.OnDelete(DeleteBehavior.Restrict);

			builder.HasOne(e => e.AssignedTo)
				.WithMany(o => o.ReprimandsReceived)
				.HasForeignKey(e => e.AssignedToId)
				.HasPrincipalKey(o => o.Uuid)
				.OnDelete(DeleteBehavior.Restrict);

			builder.HasOne(e => e.Forum)
				.WithMany(o => o.Reprimands)
				.HasForeignKey(e => e.ForumId)
				.HasPrincipalKey(o => o.Uuid)
				.OnDelete(DeleteBehavior.Restrict);
		}

		[DataType(DataType.DateTime)]
		public DateTime AssignedOn { get; set; }

		[DataType(DataType.DateTime)]
		public DateTime? ExpiresOn { get; set; }

		[StringLength(2_000, MinimumLength = 1)]
		public string Reason { get; set; }

		public ReprimandType Type { get; set; }

		public Guid AssignedById { get; set; }
		public ApplicationUser AssignedBy { get; set; }

		public Guid AssignedToId { get; set; }
		public ApplicationUser AssignedTo { get; set; }

		public Guid? ForumId { get; set; }
		public ForumModel Forum { get; set; }
	}
}
