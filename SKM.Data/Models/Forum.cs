﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SKM.Data.Models {
	internal class ForumModel : ModelBase {
		public static void ApplyAttributes(EntityTypeBuilder<ForumModel> builder) {
			ModelBase.ApplyAttributes(builder);

			builder.HasIndex(e => e.CreatedOn);
			builder.HasIndex(e => e.Name);

			builder.HasOne(e => e.CreatedBy)
				.WithMany(o => o.ForumsCreated)
				.HasForeignKey(e => e.CreatedById)
				.HasPrincipalKey(o => o.Uuid)
				.OnDelete(DeleteBehavior.Restrict);
		}

		[DataType(DataType.DateTime)]
		public DateTime CreatedOn { get; set; }

		[StringLength(500, MinimumLength = 1)]
		public string Description { get; set; }

		[DataType(DataType.ImageUrl)]
		[StringLength(1_000)]
		public string IconUrl { get; set; }

		[Required]
		[StringLength(100, MinimumLength = 1)]
		public string Name { get; set; }

		[DataType(DataType.DateTime)]
		public DateTime? UpdatedOn { get; set; }

		public Guid CreatedById { get; set; }
		public ApplicationUser CreatedBy { get; set; }

		public ICollection<NoticeModel> Notices { get; set; }
		public ICollection<ReprimandModel> Reprimands { get; set; }
		public ICollection<ThreadModel> Threads { get; set; }
	}
}
