﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.ComponentModel.DataAnnotations;

namespace SKM.Data.Models {
	internal abstract class ModelBase {
		public static void ApplyAttributes<Type>(EntityTypeBuilder<Type> builder)
			where Type : ModelBase {
			builder.HasIndex(e => e.Uuid)
				.IsUnique();
			builder.HasIndex(e => e.DeletedOn);

			builder.HasQueryFilter(e => e.DeletedOn == null);
		}

		public int Id { get; set; }

		public Guid Uuid { get; set; } = Guid.NewGuid();

		[DataType(DataType.DateTime)]
		public DateTime? DeletedOn { get; set; }

		public void Delete() {
			if (DeletedOn == null)
				DeletedOn = DateTime.Now;
		}
	}
}
