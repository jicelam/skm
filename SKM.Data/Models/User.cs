﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SKM.Data.Models {
	public class ApplicationUser : IdentityUser<int> {
		public static void ApplyAttributes(EntityTypeBuilder<ApplicationUser> builder) {
			builder.HasIndex(e => e.Uuid)
				.IsUnique();
			builder.HasIndex(e => e.JoinedOn);
			builder.HasIndex(e => e.Location);
		}

		public Guid Uuid { get; set; } = Guid.NewGuid();

		[DataType(DataType.ImageUrl)]
		[StringLength(1_000)]
		public string AvatarUrl { get; set; }

		[StringLength(250)]
		public string Caption { get; set; }

		[DataType(DataType.DateTime)]
		public DateTime JoinedOn { get; set; }

		[StringLength(250)]
		public string Location { get; set; }

		[StringLength(2_000)]
		public string Signature { get; set; }

		internal ICollection<FavoritedItemModel> Favorites { get; set; }
		internal ICollection<ForumModel> ForumsCreated { get; set; }
		internal ICollection<NoticeModel> NoticesCreated { get; set; }
		internal ICollection<PostModel> PostsCreated { get; set; }
		internal ICollection<PostModel> PostsUpdated { get; set; }
		internal ICollection<ReprimandModel> ReprimandsCreated { get; set; }
		internal ICollection<ReprimandModel> ReprimandsReceived { get; set; }
		internal ICollection<ThreadModel> ThreadsCreated { get; set; }
	}
}
